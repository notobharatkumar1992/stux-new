package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.AppDelegate;
import com.stux.Models.JobsModel;
import com.stux.R;
import com.stux.Utils.DateUtils;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class JobsInternshipRecyclerViewAdapter extends RecyclerView.Adapter<JobsInternshipViewHolders> {

    private ArrayList<JobsModel> jobsArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public JobsInternshipRecyclerViewAdapter(Context mContext, ArrayList<JobsModel> jobsArray, OnListItemClickListener itemClickListener) {
        this.jobsArray = jobsArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public JobsInternshipViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.jobs_internship_list_item, null);
        JobsInternshipViewHolders rcv = new JobsInternshipViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final JobsInternshipViewHolders holder, final int position) {
        try {
            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.jobs, position);
                    }
                }
            });

            holder.txt_c_jobs_name.setText(jobsArray.get(position).title);
            holder.txt_c_company_name.setText(jobsArray.get(position).company_name);
            holder.txt_c_address.setText(jobsArray.get(position).company_address);

            Calendar calendar = Calendar.getInstance();
            String time = "";
            try {
                calendar.setTimeInMillis(Long.parseLong(jobsArray.get(position).expiry_date));
                time = new SimpleDateFormat("dd MMM yyyy '('hh:mm aa')'").format(calendar.getTime());
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            holder.txt_c_time.setText("Posted : " + time);

            if (DateUtils.isAfterDay(calendar.getTime(), Calendar.getInstance().getTime()) || DateUtils.isSameDay(calendar.getTime(), Calendar.getInstance().getTime())) {
                holder.txt_c_condition.setText("NEW");
                holder.txt_c_condition.setSelected(true);
            } else {
                holder.txt_c_condition.setText("CLOSE");
                holder.txt_c_condition.setSelected(false);
            }

            holder.img_loading.setVisibility(View.VISIBLE);
            if (holder.img_loading != null) {
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
            }

            if (AppDelegate.isValidString(jobsArray.get(position).logo_image_thumb)) {
                Picasso.with(mContext).load(jobsArray.get(position).logo_image_thumb).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        holder.cimg_user.setImageBitmap(bitmap);
                        holder.img_loading.setVisibility(View.GONE);
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        holder.img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
            } else {
                holder.img_loading.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.jobsArray.size();
    }
}
