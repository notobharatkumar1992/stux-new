package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.DealModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class MyDealRecyclerViewAdapter extends RecyclerView.Adapter<DealViewHolders> {

    private ArrayList<DealModel> dealArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public MyDealRecyclerViewAdapter(Context mContext, ArrayList<DealModel> dealArray, OnListItemClickListener itemClickListener) {
        this.dealArray = dealArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public DealViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.deals_list_item, null);
        DealViewHolders rcv = new DealViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final DealViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(dealArray.get(position).title);
            holder.txt_c_description.setText(dealArray.get(position).details);
            holder.txt_c_price_old.setText("N" + dealArray.get(position).price + "");
            holder.txt_c_price_old.setPaintFlags(holder.txt_c_price_old.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            String price = dealArray.get(position).price;
            price = price.replaceAll(",", "");

            String discount_price = dealArray.get(position).discount_price;
            discount_price = discount_price.replaceAll(",", "");

            int calculatedPrice = Integer.parseInt(price) - Integer.parseInt(discount_price);
            holder.txt_c_price.setText("N" + AppDelegate.getPriceFormatted(calculatedPrice));

            holder.txt_c_percent.setText(dealArray.get(position).discount + "%\nOff");

            String time = dealArray.get(position).created.substring(0, dealArray.get(position).created.lastIndexOf("+"));
//        2016-06-27T06:32
            try {
//            AppDelegate.LogT("time before = " + time);
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.txt_c_time.setText(time);

            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            Picasso.with(mContext).load(dealArray.get(position).image_1_thumb).into(holder.img_deals, new Callback() {
                @Override
                public void onSuccess() {
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.deal, position);
                    }
                }
            });

            holder.ll_c_main.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.LONG_CLICK, position);
                    }
                    return false;
                }
            });

            startTimer(holder, dealArray.get(position).expiry_date);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void startTimer(final DealViewHolders holder, String expiry_date) {
        Calendar calendar = Calendar.getInstance();
        expiry_date = expiry_date + "000";
        long remaining = Long.parseLong(expiry_date) - calendar.getTimeInMillis();
        if (remaining > 0) {
            calendar.setTimeInMillis(remaining);
            if (holder.countDownTimer == null) {
                holder.countDownTimer = new CountDownTimer(remaining, 1000) { // adjust the milli seconds here
                    public void onTick(long millisUntilFinished) {
                        long seconds = millisUntilFinished / 1000;
                        long minutes = seconds / 60;
                        long hours = minutes / 60;
                        long days = hours / 24;
                        String time = days + "D " + hours % 24 + "H " + minutes % 60 + "M " + seconds % 60 + "S ";

                        holder.txt_c_timer.setText(time + "");
                    }

                    public void onFinish() {
                        holder.txt_c_timer.setText("Coupon expired");
                    }
                }.start();
            }
        } else {
            AppDelegate.LogE("Deal expired : remaining => " + remaining);
            holder.txt_c_timer.setText("Coupon expired");
        }
    }

    @Override
    public int getItemCount() {
        return this.dealArray.size();
    }
}
