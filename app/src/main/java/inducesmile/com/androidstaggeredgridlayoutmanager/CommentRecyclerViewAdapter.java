package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.CommentModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentViewHolders> {

    private ArrayList<CommentModel> dealArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public CommentRecyclerViewAdapter(Context mContext, ArrayList<CommentModel> dealArray, OnListItemClickListener itemClickListener) {
        this.dealArray = dealArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public CommentViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, null);
        CommentViewHolders rcv = new CommentViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final CommentViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(dealArray.get(position).user_first_name + " " + dealArray.get(position).user_last_name);
            holder.txt_c_description.setText(dealArray.get(position).comment);

            String time = dealArray.get(position).created.substring(0, dealArray.get(position).created.lastIndexOf("+"));
//        2016-06-27T06:32
            try {
//            AppDelegate.LogT("time before = " + time);
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.txt_c_time.setText(time);
//        Picasso.with(mContext).load(dealArray.get(position).image_1).into(holder.img_product);

            if (AppDelegate.isValidString(dealArray.get(position).user_image)) {
                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();

                Picasso.with(mContext).load(dealArray.get(position).user_image).into(holder.cimg_user, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
            } else {
                holder.img_loading.setVisibility(View.GONE);
            }

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.comment, position);
                    }
                }
            });
            holder.cimg_user.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.user, position);
                    }
                }
            });

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.dealArray.size();
    }
}
