package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stux.AppDelegate;
import com.stux.Models.ReasonModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

public class ReasonRecyclerViewAdapter extends RecyclerView.Adapter<ReasonViewHolders> {

    private ArrayList<ReasonModel> productArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public ReasonRecyclerViewAdapter(Context mContext, ArrayList<ReasonModel> productArray, OnListItemClickListener itemClickListener) {
        this.productArray = productArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ReasonViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.reason_list_item, null);
        ReasonViewHolders rcv = new ReasonViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final ReasonViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(productArray.get(position).name);
            holder.img_reason.setImageResource(productArray.get(position).icon);
            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.reason_id, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.productArray.size();
    }
}
