package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.NotificationModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.TextView;

public class NotificationRecyclerViewAdapter extends RecyclerView.Adapter<NotificationViewHolders> {

    private ArrayList<NotificationModel> notificationArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public NotificationRecyclerViewAdapter(Context mContext, ArrayList<NotificationModel> notificationArray, OnListItemClickListener itemClickListener) {
        this.notificationArray = notificationArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public NotificationViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, null);
        NotificationViewHolders rcv = new NotificationViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final NotificationViewHolders holder, final int position) {
        try {
            String time = notificationArray.get(position).created;
            if (time.contains("+0000"))
                time = notificationArray.get(position).created.substring(0, notificationArray.get(position).created.lastIndexOf("+"));
            if (!time.contains("T"))
                time = notificationArray.get(position).created.replaceAll(" ", "T");
//        2016-06-27T06:32   .. 2016-08-13 06:02:57
            try {
//            AppDelegate.LogT("time before = " + time);
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            holder.txt_c_time.setText(time);
            AppDelegate.LogT("Type => " + notificationArray.get(position).notification_type);
            if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.follow)) {
                holder.cimg_user.setVisibility(View.VISIBLE);
                holder.cimg_user_ph.setVisibility(View.VISIBLE);
                holder.img_product.setVisibility(View.GONE);

                String title = notificationArray.get(position).user_first_name + " " + notificationArray.get(position).user_last_name + " started following you.";
                holder.txt_c_name.setText(title);
                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                if (AppDelegate.isValidString(notificationArray.get(position).user_image))
                    Picasso.with(mContext).load(notificationArray.get(position).user_image).into(holder.cimg_user, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.product)) {
                holder.img_product.setVisibility(View.VISIBLE);
                holder.cimg_user.setVisibility(View.GONE);
                holder.cimg_user_ph.setVisibility(View.GONE);

                String title = "Product " + notificationArray.get(position).product_name + " uploaded by " + notificationArray.get(position).user_first_name + " " + notificationArray.get(position).user_last_name + ".";
                holder.txt_c_name.setText(title);
                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                if (AppDelegate.isValidString(notificationArray.get(position).product_image))
                    Picasso.with(mContext).load(notificationArray.get(position).product_image).into(holder.img_product, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.CHAT)) {

                holder.img_product.setVisibility(View.VISIBLE);
                holder.cimg_user.setVisibility(View.GONE);
                holder.cimg_user_ph.setVisibility(View.GONE);

                String title = "You have a chat from " + notificationArray.get(position).user_first_name + " " + notificationArray.get(position).user_last_name + ".";
                holder.txt_c_name.setText(title);
                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                if (AppDelegate.isValidString(notificationArray.get(position).user_image))
                    Picasso.with(mContext).load(notificationArray.get(position).user_image).into(holder.img_product, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.deal)) {
                holder.img_product.setVisibility(View.VISIBLE);
                holder.cimg_user.setVisibility(View.GONE);
                holder.cimg_user_ph.setVisibility(View.GONE);

                String title = notificationArray.get(position).deal_title + " created in your campus.";
                holder.txt_c_name.setText(title);
                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                if (AppDelegate.isValidString(notificationArray.get(position).deal_image))
                    Picasso.with(mContext).load(notificationArray.get(position).deal_image).into(holder.img_product, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.Makeoffer)) {
                holder.img_product.setVisibility(View.GONE);
                holder.cimg_user.setVisibility(View.VISIBLE);
                holder.cimg_user_ph.setVisibility(View.VISIBLE);


//                String title = notificationArray.get(position).user_first_name + " " + notificationArray.get(position).user_last_name + " make offer on your product " + notificationArray.get(position).product_name + " of price " + notificationArray.get(position).offer_price + ".";
                String title = notificationArray.get(position).user_first_name + " " + notificationArray.get(position).user_last_name + " make an offer of  <font color='blue'>N" + notificationArray.get(position).offer_price + "</font> for " + notificationArray.get(position).product_name + " listed.";
//                sumant make an offer of N2,500 for iphone 6s listed
                holder.txt_c_name.setText(Html.fromHtml(title), TextView.BufferType.SPANNABLE);
//                holder.txt_c_name.setText(title);
                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                if (AppDelegate.isValidString(notificationArray.get(position).user_image))
                    Picasso.with(mContext).load(notificationArray.get(position).user_image).into(holder.cimg_user, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });

            } else if (notificationArray.get(position).notification_type.equalsIgnoreCase(Tags.CHAT)) {
                holder.img_product.setVisibility(View.GONE);
                holder.cimg_user.setVisibility(View.VISIBLE);
                holder.cimg_user_ph.setVisibility(View.VISIBLE);

                holder.txt_c_name.setText("you have a chat from " + notificationArray.get(position).user_first_name + ".");

                holder.img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                frameAnimation.setCallback(holder.img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();
                if (AppDelegate.isValidString(notificationArray.get(position).user_image))
                    Picasso.with(mContext).load(notificationArray.get(position).user_image).into(holder.cimg_user, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
            }

            holder.rl_c_following.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.notification, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.notificationArray.size();
    }

}
