package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.Message;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MessageListViewAdapter extends RecyclerView.Adapter<MessageListViewHolders> {

    private ArrayList<Message> messageArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public MessageListViewAdapter(Context mContext, ArrayList<Message> messageArray, OnListItemClickListener itemClickListener) {
        this.messageArray = messageArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public MessageListViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_list_item, null);
        MessageListViewHolders rcv = new MessageListViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final MessageListViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(messageArray.get(position).mUsername);
            holder.txt_c_description.setText(messageArray.get(position).product_name);
            holder.txt_c_message.setText(messageArray.get(position).mMessage);

            if (messageArray.get(position).newMessageCount > 0)
                holder.txt_c_message_count.setText(messageArray.get(position).newMessageCount);
            else holder.txt_c_message_count.setVisibility(View.GONE);

            String time = messageArray.get(position).created;
//        2016-06-27T06:32
            try {
                time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            holder.txt_c_time.setText(time);

            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            Picasso.with(mContext).load(messageArray.get(position).dataModel.image).into(holder.cimg_user, new Callback() {
                @Override
                public void onSuccess() {
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.user, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.messageArray.size();
    }
}
