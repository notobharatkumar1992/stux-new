package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.Message;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ChatListViewAdapter extends RecyclerView.Adapter<ChatListViewHolders> {

    private ArrayList<Message> messageArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public ChatListViewAdapter(Context mContext, ArrayList<Message> messageArray, OnListItemClickListener itemClickListener) {
        this.messageArray = messageArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ChatListViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item, null);
        ChatListViewHolders rcv = new ChatListViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final ChatListViewHolders holder, final int position) {
        try {
            if (messageArray.get(position).user_type == Message.USER_SENDER) {
                holder.rl_c_sender.setVisibility(View.VISIBLE);
                holder.rl_c_receiver.setVisibility(View.GONE);

                holder.etxt_name_sender.setText(messageArray.get(position).mMessage);
                //  2016-06-27T06:32
                try {
                    String time = messageArray.get(position).created;
                    if (time.contains(".")) {
                        time = messageArray.get(position).created.substring(0, messageArray.get(position).created.indexOf("."));
                    }
                    if (time.contains("+")) {
                        time = messageArray.get(position).created.substring(0, messageArray.get(position).created.indexOf("+"));
                    }
                    AppDelegate.LogT("time = > " + time);
                    time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                    holder.txt_c_time_sender.setText(time);
                } catch (ParseException e) {
                    AppDelegate.LogE(e);
                    holder.txt_c_time_sender.setText(messageArray.get(position).created);
                }

                holder.img_loading_sender.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading_sender.getDrawable();
                frameAnimation.setCallback(holder.img_loading_sender);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();

                if (AppDelegate.isValidString(messageArray.get(position).dataModel.image))
                    Picasso.with(mContext).load(messageArray.get(position).dataModel.image).into(holder.cimg_user_sender, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading_sender.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });

                holder.rl_c_sender.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (itemClickListener != null) {
                            itemClickListener.setOnListItemClickListener(Tags.user, position);
                        }
                    }
                });
            } else {

                holder.rl_c_sender.setVisibility(View.GONE);
                holder.rl_c_receiver.setVisibility(View.VISIBLE);

                holder.etxt_name_receiver.setText(messageArray.get(position).mMessage);
                String time = messageArray.get(position).created;
//        2016-06-27T06:32
                try {
                    time = new SimpleDateFormat("dd MMM, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                } catch (ParseException e) {
                } catch (Exception e) {
                }
                holder.txt_c_time_receiver.setText(time);

                holder.img_loading_receiver.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading_receiver.getDrawable();
                frameAnimation.setCallback(holder.img_loading_receiver);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();

                if (AppDelegate.isValidString(messageArray.get(position).dataModel.image))
                    Picasso.with(mContext).load(messageArray.get(position).dataModel.image).into(holder.cimg_user_receiver, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.img_loading_receiver.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });

                holder.rl_c_receiver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (itemClickListener != null) {
                            itemClickListener.setOnListItemClickListener(Tags.user, position);
                        }
                    }
                });
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.messageArray.size();
    }
}
