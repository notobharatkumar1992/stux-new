package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.ItemFoundModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class LostFoundItemRecyclerViewAdapter extends RecyclerView.Adapter<LostFoundItemViewHolders> {

    private ArrayList<ItemFoundModel> itemFoundArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;
    private String tagValue = "";

    public LostFoundItemRecyclerViewAdapter(Context mContext, ArrayList<ItemFoundModel> itemFoundArray, OnListItemClickListener itemClickListener) {
        this.itemFoundArray = itemFoundArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    public LostFoundItemRecyclerViewAdapter(Context mContext, ArrayList<ItemFoundModel> itemFoundArray, OnListItemClickListener itemClickListener, String tagValue) {
        this.itemFoundArray = itemFoundArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
        this.tagValue = tagValue;
    }

    @Override
    public LostFoundItemViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lost_found_list_item, null);
        LostFoundItemViewHolders rcv = new LostFoundItemViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final LostFoundItemViewHolders holder, final int position) {
        try {
            String time = itemFoundArray.get(position).created.substring(0, itemFoundArray.get(position).created.lastIndexOf("+"));
            try {
                time = new SimpleDateFormat("MMM dd yyyy, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
            holder.txt_c_time.setText(time);
            holder.txt_c_name.setText(itemFoundArray.get(position).item_name);
            holder.txt_c_description.setText(itemFoundArray.get(position).item_description);
            holder.txt_c_location.setText(AppDelegate.isValidString(itemFoundArray.get(position).location) ? itemFoundArray.get(position).location : "");

            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(AppDelegate.isValidString(tagValue) ? tagValue : Tags.LOST_FOUND, position);
                    }
                }
            });
            Picasso.with(mContext).load(itemFoundArray.get(position).image_1).into(holder.cimg_item, new Callback() {
                @Override
                public void onSuccess() {
                    holder.img_loading.setVisibility(View.GONE);
                    notifyDataSetChanged();
                }

                @Override
                public void onError() {

                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.itemFoundArray.size();
    }
}
