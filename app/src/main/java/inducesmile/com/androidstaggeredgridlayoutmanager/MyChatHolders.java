package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.rockerhieu.emojicon.EmojiconTextView;
import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.RelativeLayout;
import carbon.widget.TextView;

public class MyChatHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_c_name;
    public EmojiconTextView etxt_c_product_name;
    public ImageView img_c_loading_user, img_c_loading_product, img_c_product;
    public CircleImageView cimg_user;
    public RelativeLayout rl_c_main;

    public MyChatHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);
        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);

        img_c_loading_user = (ImageView) convertView.findViewById(R.id.img_c_loading_user);
        img_c_loading_product = (ImageView) convertView.findViewById(R.id.img_c_loading_product);
        img_c_product = (ImageView) convertView.findViewById(R.id.img_c_product);

        cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);

        etxt_c_product_name = (EmojiconTextView) convertView.findViewById(R.id.etxt_c_product_name);
        rl_c_main = (RelativeLayout) convertView.findViewById(R.id.rl_c_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
