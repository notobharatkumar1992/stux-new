package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.ReviewModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ReviewsRecyclerViewAdapter extends RecyclerView.Adapter<ReviewsViewHolders> {

    private ArrayList<ReviewModel> reviewArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;

    public ReviewsRecyclerViewAdapter(Context mContext, ArrayList<ReviewModel> reviewArray, OnListItemClickListener itemClickListener) {
        this.reviewArray = reviewArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ReviewsViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.review_list_item, null);
        ReviewsViewHolders rcv = new ReviewsViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final ReviewsViewHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(reviewArray.get(position).userDataModel.first_name + " " + reviewArray.get(position).userDataModel.last_name);
            holder.txt_c_description.setText(reviewArray.get(position).reviews);
            String time = reviewArray.get(position).created.substring(0, reviewArray.get(position).created.lastIndexOf("+"));

            try {
//            AppDelegate.LogT("time before = " + time); //        2016-06-27T06:32
                time = new SimpleDateFormat("dd MMMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time));
                holder.txt_c_time.setText("Posted " + time);
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }


            holder.img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
            frameAnimation.setCallback(holder.img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            Picasso.with(mContext).load(reviewArray.get(position).userDataModel.image).into(holder.cimg_user, new Callback() {
                @Override
                public void onSuccess() {
                    holder.img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    holder.img_loading.setVisibility(View.GONE);
                }
            });

            holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
            holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));

            switch (Integer.parseInt(reviewArray.get(position).rating)) {
                case 0:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 1:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 2:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 3:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 4:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_blank));
                    break;
                case 5:
                    holder.img_star_1.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_2.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_3.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_4.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    holder.img_star_5.setImageDrawable(mContext.getResources().getDrawable(R.drawable.star_filled));
                    break;
            }

            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.product, position);
                    }
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.reviewArray.size();
    }
}
