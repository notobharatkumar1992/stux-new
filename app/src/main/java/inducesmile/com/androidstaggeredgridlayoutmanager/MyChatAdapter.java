package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.Message;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

public class MyChatAdapter extends RecyclerView.Adapter<MyChatHolders> {

    private ArrayList<Message> messageArray;
    private Context mContext;
    private OnListItemClickListener itemClickListener;
    private String tagValue = "";

    public MyChatAdapter(Context mContext, ArrayList<Message> messageArray, OnListItemClickListener itemClickListener) {
        this.messageArray = messageArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
    }

    public MyChatAdapter(Context mContext, ArrayList<Message> messageArray, OnListItemClickListener itemClickListener, String tagValue) {
        this.messageArray = messageArray;
        this.mContext = mContext;
        this.itemClickListener = itemClickListener;
        this.tagValue = tagValue;
    }

    @Override
    public MyChatHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_chat_list_items, null);
        MyChatHolders rcv = new MyChatHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final MyChatHolders holder, final int position) {
        try {
            holder.txt_c_name.setText(messageArray.get(position).mUsername);
            holder.etxt_c_product_name.setText(messageArray.get(position).mMessage);
            holder.img_c_loading_user.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_c_loading_user.getDrawable();
            frameAnimation.setCallback(holder.img_c_loading_user);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            holder.img_c_loading_product.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation1 = (AnimationDrawable) holder.img_c_loading_user.getDrawable();
            frameAnimation1.setCallback(holder.img_c_loading_user);
            frameAnimation1.setVisible(true, true);
            frameAnimation1.start();

            holder.rl_c_main.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.LONG_CLICK, position);
                    }
                    return false;
                }
            });

            holder.rl_c_main.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemClickListener != null) {
                        itemClickListener.setOnListItemClickListener(Tags.user, position);
                    }
                }
            });

            if (AppDelegate.isValidString(messageArray.get(position).dataModel.image))
                Picasso.with(mContext).load(messageArray.get(position).dataModel.image).into(holder.cimg_user, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.img_c_loading_product.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });

            if (AppDelegate.isValidString(messageArray.get(position).productModel.image_1))
                Picasso.with(mContext).load(messageArray.get(position).productModel.image_1).into(holder.img_c_product, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.img_c_loading_user.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
                    }
                });

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public int getItemCount() {
        return this.messageArray.size();
    }
}
