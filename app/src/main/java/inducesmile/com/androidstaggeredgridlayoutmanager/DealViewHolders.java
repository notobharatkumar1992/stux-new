package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class DealViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageView img_deals, img_loading;
    TextView txt_c_name, txt_c_description, txt_c_price, txt_c_price_old, txt_c_percent, txt_c_time, txt_c_timer;
    LinearLayout ll_c_main;
    CountDownTimer countDownTimer;

    public DealViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        img_deals = (ImageView) convertView.findViewById(R.id.img_deals);
        img_deals.setImageDrawable(null);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
        txt_c_price = (TextView) convertView.findViewById(R.id.txt_c_price);
        txt_c_price_old = (TextView) convertView.findViewById(R.id.txt_c_price_old);
        txt_c_percent = (TextView) convertView.findViewById(R.id.txt_c_percent);
        txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
        txt_c_timer = (TextView) convertView.findViewById(R.id.txt_c_timer);
        ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();

    }
}
