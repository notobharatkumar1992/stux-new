package inducesmile.com.androidstaggeredgridlayoutmanager;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.stux.R;
import com.stux.Utils.CircleImageView;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class ReviewsViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    LinearLayout ll_c_main;
    CircleImageView cimg_user;
    ImageView img_star_1, img_star_2, img_star_3, img_star_4, img_star_5, img_loading;
    TextView txt_c_name, txt_c_description, txt_c_time;

    public ReviewsViewHolders(View convertView) {
        super(convertView);
        convertView.setOnClickListener(this);

        ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);

        cimg_user = (CircleImageView) convertView.findViewById(R.id.cimg_user);

        img_star_1 = (ImageView) convertView.findViewById(R.id.img_star_1);
        img_star_2 = (ImageView) convertView.findViewById(R.id.img_star_2);
        img_star_3 = (ImageView) convertView.findViewById(R.id.img_star_3);
        img_star_4 = (ImageView) convertView.findViewById(R.id.img_star_4);
        img_star_5 = (ImageView) convertView.findViewById(R.id.img_star_5);
        img_loading = (ImageView) convertView.findViewById(R.id.img_loading);

        txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
        txt_c_description = (TextView) convertView.findViewById(R.id.txt_c_description);
        txt_c_time = (TextView) convertView.findViewById(R.id.txt_c_time);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
    }
}
