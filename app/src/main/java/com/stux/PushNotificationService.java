package com.stux;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.google.android.gms.gcm.GcmListenerService;
import com.stux.Models.DealModel;
import com.stux.Models.InstitutionModel;
import com.stux.Models.Message;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.constants.Tags;
import com.stux.fragments.ChatFragment;
import com.stux.parser.JSONParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Random;

/**
 * Created by bharat on 2/4/16.
 */
public class PushNotificationService extends GcmListenerService {

    public static final String TYPE_DEAL = "New Deal Added in Your Area";
    public static final String TYPE_FOLLOW = "follow";
    public static final String TYPE_PRODUCT = "createProduct";
    public static final String TYPE_MAKE_PRODUCT = "Makeoffer";

    @Override
    public void onMessageReceived(String from, final Bundle data) {
        AppDelegate.LogGC("PushNotificationService message = " + data.toString());
        try {
            AppDelegate.LogGC("push notification type => " + data.getString(Tags.type));
            if (data.getString(Tags.type).equalsIgnoreCase(TYPE_FOLLOW)) {
                // When user followed to another then that Notification will fire.
                showUserFollowNotification(data);
            } else if (data.getString(Tags.type).equalsIgnoreCase(TYPE_DEAL)) {
//                showPush for deals
                generateBigPictireStyleNotification(data);
            } else if (data.getString(Tags.type).equalsIgnoreCase(TYPE_PRODUCT)) {
//                showPush for deals
                generateBigPictireStyleProductNotification(data);
            } else if (data.getString(Tags.type).equalsIgnoreCase(TYPE_MAKE_PRODUCT)) {
//                showPush for deals
                generateBigPictireStyleMakeOfferProductNotification(data);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static void showDemoNotification(Context mContext, String message) {
        try {
            Notification.Builder notificationBuilder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.drawable.logo)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentTitle(mContext.getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle().bigText(message.toString()))
                        .setContentText(message);
            } else {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(mContext.getString(R.string.app_name))
                        .setContentText(message.toString());
            }


            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(mContext, 0, new Intent(), PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(fullScreenPendingIntent);

            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
                notification = notificationBuilder.build();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                notification = new Notification(R.drawable.logo,
                        message.toString(), System.currentTimeMillis());
            } else {
                notification = notificationBuilder.build();
            }

            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            ((NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void generateBigPictireStyleMakeOfferProductNotification(Bundle bundle) {
        Bitmap remote_picture = null;
        Message message = new Message();
        try {
            JSONObject object = new JSONObject(bundle.getString(Tags.message));
//            UserDataModel userDataModel = new UserDataModel();
//            userDataModel.userId = object.getString(Tags.id);
//            userDataModel.first_name = object.getString(Tags.first_name);
//            userDataModel.last_name = object.getString(Tags.last_name);
//            userDataModel.image = object.getString(Tags.image);
//            userDataModel.offer_price = object.getString(Tags.offer_price);
//
//            userDataModel.product_id = object.getJSONObject(Tags.product).getString(Tags.id);
//            userDataModel.product_title = object.getJSONObject(Tags.product).getString(Tags.title);
//            userDataModel.product_image = object.getJSONObject(Tags.product).getString(Tags.image_1_thumb);


            message.receiver_id = object.getString(Tags.id);
            message.sender_id = object.getString(Tags.id);
            message.status = object.getString(Tags.status);
            message.mType = Message.TYPE_MESSAGE;
            message.user_type = Message.USER_RECEIVER;

            message.product_id = object.getJSONObject(Tags.product).getString(Tags.id);
            message.product_name = object.getJSONObject(Tags.product).getString(Tags.title);
            message.product_image = object.getJSONObject(Tags.product).getString(Tags.image_1_thumb);
            message.offer_price = object.getString(Tags.offer_price);

            message.productModel = new ProductModel();
            message.productModel.id = message.product_id;
            message.productModel.title = message.product_name;
            message.productModel.image_1 = message.product_image;

            message.dataModel = new UserDataModel();
            message.dataModel.userId = object.getString(Tags.id);
            message.dataModel.first_name = object.getString(Tags.first_name);
            message.dataModel.last_name = object.getString(Tags.last_name);
            message.dataModel.image = object.getString(Tags.image);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        String comment = message.dataModel.first_name + " " + message.dataModel.last_name + " make offer on your product " + "" + " of price " + message.offer_price + ".";

        // Create the style object with BigPictureStyle subclass.
        NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(getString(R.string.app_name));
        notiStyle.setSummaryText(comment);

        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(message.productModel.image_1).getContent());
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }

// Add the big picture to the style.
        if (remote_picture != null)
            notiStyle.bigPicture(remote_picture);

// Creates an explicit intent for an ResultActivity to receive.
        Intent resultIntent = new Intent(this, NotificationHandler.class);
        Bundle dataBundle = new Bundle();
        dataBundle.putString(Tags.FROM, Tags.CHAT);
        dataBundle.putInt(Tags.PAGE, ChatFragment.FROM_CHAT);
        dataBundle.putParcelable(Tags.message, message);

        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.putExtras(dataBundle);

// This ensures that the back button follows the recommended
// convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

// Adds the back stack for the Intent (but not the Intent itself).
//        stackBuilder.addParentStack(ResultActivity.class);

// Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(comment)
                .setStyle(notiStyle).build();


        notification.flags |= Notification.FLAG_AUTO_CANCEL;

//        if (bundle.getString("sound").equalsIgnoreCase("1"))
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

//        if (bundle.getString("vibrate").equalsIgnoreCase("1"))
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);
    }

    private void generateBigPictireStyleProductNotification(Bundle bundle) {
        Bitmap remote_picture = null;

        ProductModel productModel = null;
        try {
            productModel = getProductModel((new JSONObject(bundle.getString(Tags.message))).getString(Tags.product));
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }

        // Create the style object with BigPictureStyle subclass.
        NotificationCompat.BigPictureStyle notiStyle = new
                NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(getString(R.string.app_name));
        notiStyle.setSummaryText(productModel.title);

        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(productModel.image_1_thumb).getContent());
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }

// Add the big picture to the style.
        if (remote_picture != null)
            notiStyle.bigPicture(remote_picture);

// Creates an explicit intent for an ResultActivity to receive.
        Intent resultIntent = new Intent(this, NotificationHandler.class);
        Bundle dataBundle = new Bundle();
//        dataBundle.putInt(Tags.FROM, AppDelegate.PRODUCT_NOTIFICATION);
        dataBundle.putString(Tags.FROM, Tags.product);
        dataBundle.putParcelable(Tags.product, productModel);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.putExtras(dataBundle);


// This ensures that the back button follows the recommended
// convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

// Adds the back stack for the Intent (but not the Intent itself).
//        stackBuilder.addParentStack(ResultActivity.class);

// Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(productModel.title)
                .setStyle(notiStyle).build();


        notification.flags |= Notification.FLAG_AUTO_CANCEL;

//        if (bundle.getString("sound").equalsIgnoreCase("1"))
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

//        if (bundle.getString("vibrate").equalsIgnoreCase("1"))
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);
    }

    public static DealModel getDealModel(String objectString) {
        DealModel dealModel = new DealModel();
        try {
            JSONObject object = new JSONObject(objectString);
            dealModel.id = JSONParser.getString(object, Tags.id);
            dealModel.user_id = JSONParser.getString(object, Tags.user_id);
            dealModel.deal_area = JSONParser.getString(object, Tags.deal_area);
            dealModel.deal_catid = JSONParser.getString(object, Tags.deal_catid);
            dealModel.institute_id = JSONParser.getString(object, Tags.institute_id);
            dealModel.title = JSONParser.getString(object, Tags.title);
            dealModel.details = JSONParser.getString(object, Tags.details);
            dealModel.venue = JSONParser.getString(object, Tags.venue);
            dealModel.price = JSONParser.getString(object, Tags.price);
            dealModel.discount = JSONParser.getString(object, Tags.discount);
            dealModel.discount_price = JSONParser.getString(object, Tags.discount_price);
            dealModel.coupon_code = JSONParser.getString(object, Tags.coupon_code);
            dealModel.expiry_date = JSONParser.getString(object, Tags.expiry_date);

            JSONObject dealObject = object.getJSONObject(Tags.deal_category);
            dealModel.product_category = JSONParser.getString(dealObject, Tags.title);

            dealModel.image_1 = JSONParser.getString(object, Tags.image_1);
            dealModel.image_2 = JSONParser.getString(object, Tags.image_2);
            dealModel.image_3 = JSONParser.getString(object, Tags.image_3);
            dealModel.image_4 = JSONParser.getString(object, Tags.image_4);

            dealModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
            dealModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
            dealModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
            dealModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

            dealModel.is_grabbed = JSONParser.getString(object, Tags.is_grabbed);

            dealModel.total_deal_views = JSONParser.getString(object, Tags.total_deal_views);
            dealModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

//                    dealModel.sold_status =JSONParser.getString(object, Tags.sold_status);
//                    dealModel.status =JSONParser.getString(object, Tags.status);
            dealModel.created = JSONParser.getString(object, Tags.created);
//                    dealModel.modified =JSONParser.getString(object, Tags.modified);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return dealModel;
    }

    public static ProductModel getProductModel(String objectString) {
        ProductModel productModel = new ProductModel();
        try {
            JSONObject object = new JSONObject(objectString);
            productModel.id = object.getString(Tags.id);
            productModel.cat_id = JSONParser.getString(object, Tags.cat_id);
            productModel.title = JSONParser.getString(object, Tags.title);
            productModel.description = JSONParser.getString(object, Tags.description);
            productModel.price = JSONParser.getString(object, Tags.price);
            productModel.item_condition = JSONParser.getString(object, Tags.item_condition);

            productModel.image_1 = JSONParser.getString(object, Tags.image_1);
            productModel.image_2 = JSONParser.getString(object, Tags.image_2);
            productModel.image_3 = JSONParser.getString(object, Tags.image_3);
            productModel.image_4 = JSONParser.getString(object, Tags.image_4);

            productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
            productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
            productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
            productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

            productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
            productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
            productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

//            float floatValue = Float.parseFloat(object.getString(Tags.rating));
////                    AppDelegate.LogT("floatValue = " + floatValue);
//            productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
////                    AppDelegate.LogT("productModel.rating = " + productModel.rating);

            productModel.sold_status = JSONParser.getString(object, Tags.sold_status);
            productModel.status = JSONParser.getString(object, Tags.status);
            productModel.created = JSONParser.getString(object, Tags.created);
            productModel.modified = JSONParser.getString(object, Tags.modified);
            productModel.total_product_views = JSONParser.getString(object, Tags.total_product_views);
            productModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

            if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category) + "")) {
                JSONObject productObject = object.getJSONObject(Tags.product_category);
                productModel.pc_id = JSONParser.getString(productObject, Tags.id);
                productModel.pc_title = JSONParser.getString(productObject, Tags.cat_name);
                productModel.pc_status = JSONParser.getString(productObject, Tags.status);
            }

            productModel.latitude = JSONParser.getString(object, Tags.latitude);
            productModel.longitude = JSONParser.getString(object, Tags.longitude);

            JSONObject userObject = object.getJSONObject(Tags.user);
            productModel.user_id = JSONParser.getString(userObject, Tags.id);
            productModel.user_first_name = JSONParser.getString(userObject, Tags.first_name);
            productModel.user_last_name = JSONParser.getString(userObject, Tags.last_name);
            productModel.user_email = JSONParser.getString(userObject, Tags.email);
            productModel.user_role = JSONParser.getString(userObject, Tags.role);
            productModel.user_image = JSONParser.getString(userObject, Tags.image);
            productModel.user_social_id = JSONParser.getString(userObject, Tags.social_id);
            productModel.user_gcm_token = JSONParser.getString(userObject, Tags.gcm_token);

            productModel.user_following_count = JSONParser.getInt(userObject, Tags.following_count);
            productModel.user_followers_count = JSONParser.getInt(userObject, Tags.followers_count);
            productModel.user_total_product = JSONParser.getInt(userObject, Tags.total_product);
            productModel.user_follow_status = JSONParser.getInt(userObject, Tags.follow_status);

            JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
            productModel.user_gender = JSONParser.getString(studentObject, Tags.gender);
            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                    productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                }
                if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                } else {
                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                }
            } else {
                productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                productModel.user_department_name = studentObject.getString(Tags.department_name);
            }
            try {
                productModel.user_institute_lat = studentObject.getJSONObject(Tags.institute).getString(Tags.latitude);
                productModel.user_institute_lng = studentObject.getJSONObject(Tags.institute).getString(Tags.longitude);
                productModel.user_institute_name = studentObject.getJSONObject(Tags.institute).getString(Tags.institute_name);
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return productModel;
    }


    private void generateBigPictireStyleNotification(Bundle bundle) {
        Bitmap remote_picture = null;

        DealModel dealModel = getDealModel(bundle.getString(Tags.message));

        // Create the style object with BigPictureStyle subclass.
        NotificationCompat.BigPictureStyle notiStyle = new
                NotificationCompat.BigPictureStyle();
        notiStyle.setBigContentTitle(getString(R.string.app_name));
        notiStyle.setSummaryText(bundle.getString(Tags.comment));

        try {
            remote_picture = BitmapFactory.decodeStream((InputStream) new URL(dealModel.image_1_thumb).getContent());
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }

// Add the big picture to the style.
        notiStyle.bigPicture(remote_picture);

// Creates an explicit intent for an ResultActivity to receive.
        Intent resultIntent = new Intent(this, NotificationHandler.class);
        Bundle dataBundle = new Bundle();
        dataBundle.putString(Tags.FROM, Tags.deal);
        dataBundle.putParcelable(Tags.deal, dealModel);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        resultIntent.putExtras(dataBundle);


// This ensures that the back button follows the recommended
// convention for the back key.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);

// Adds the back stack for the Intent (but not the Intent itself).
//        stackBuilder.addParentStack(ResultActivity.class);

// Adds the Intent that starts the Activity to the top of the stack.
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo)
                .setAutoCancel(true)

                .setContentIntent(resultPendingIntent)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(bundle.getString(Tags.comment))
                .setStyle(notiStyle).build();


        notification.flags |= Notification.FLAG_AUTO_CANCEL;

//        if (bundle.getString("sound").equalsIgnoreCase("1"))
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

//        if (bundle.getString("vibrate").equalsIgnoreCase("1"))
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);
    }

    private void showUserFollowNotification(Bundle data) {
        try {
            JSONObject object = new JSONObject(data.getString(Tags.message));
            UserDataModel userDataModel = new UserDataModel();
            userDataModel.role = JSONParser.getString(object, Tags.role);
            userDataModel.image = JSONParser.getString(object, Tags.image);
            userDataModel.status = JSONParser.getString(object, Tags.status);
            userDataModel.is_verified = JSONParser.getString(object, Tags.is_verified);
            userDataModel.first_name = JSONParser.getString(object, Tags.first_name);
            userDataModel.last_name = JSONParser.getString(object, Tags.last_name);
            userDataModel.email = JSONParser.getString(object, Tags.email);
            userDataModel.created = JSONParser.getString(object, Tags.created);

            userDataModel.facebook_id = JSONParser.getString(object, Tags.social_id);
            userDataModel.following_count = JSONParser.getInt(object, Tags.following_count);
            userDataModel.followers_count = JSONParser.getInt(object, Tags.followers_count);
            userDataModel.total_product = JSONParser.getInt(object, Tags.total_product);
            userDataModel.follow_status = JSONParser.getInt(object, Tags.follow_status);

            JSONObject studentObject = object.getJSONObject(Tags.student_detail);
            userDataModel.str_Gender = studentObject.getString(Tags.gender);
            userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
            userDataModel.userId = studentObject.getString(Tags.user_id);
            userDataModel.dob = studentObject.getString(Tags.dob);
            userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

            userDataModel.institutionModel = new InstitutionModel();
            userDataModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                userDataModel.institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                userDataModel.institutionModel.institution_name = studentObject.getJSONObject(Tags.institute).getString(Tags.institute_name);
            } else {
                userDataModel.institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
            }
            userDataModel.institutionModel.department_name = studentObject.getString(Tags.department_name);

            String message = userDataModel.first_name + " " + userDataModel.last_name + " has followed you.";

            Notification.Builder notificationBuilder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentTitle(getString(R.string.app_name))
                        .setStyle(new Notification.BigTextStyle().bigText(message))
                        .setContentText(message);
            } else {
                notificationBuilder = new Notification.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(message);
            }

            Bundle bundle = new Bundle();
            bundle.putString(Tags.FROM, Tags.follow);
            bundle.putParcelable(Tags.user, userDataModel);

            Intent push = new Intent(this, NotificationHandler.class);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            push.putExtras(bundle);

            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(this, 0, push, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(fullScreenPendingIntent);

            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
                notification = notificationBuilder.build();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                notification = new Notification(R.drawable.logo,
                        message, System.currentTimeMillis());
            } else {
                notification = notificationBuilder.build();
            }

            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static void showUserChatNotification(Context mContext, Message messageModel) {
        try {
            String message = messageModel.dataModel.first_name + " " + messageModel.dataModel.last_name + " has sent you message.";

            Notification.Builder notificationBuilder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.drawable.logo)
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentTitle(message)
                        .setStyle(new Notification.BigTextStyle().bigText(messageModel.mMessage))
                        .setContentText(messageModel.mMessage);
            } else {
                notificationBuilder = new Notification.Builder(mContext)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(message)
                        .setContentText(messageModel.mMessage);
            }

            Bundle bundle = new Bundle();
            bundle.putString(Tags.FROM, Tags.CHAT);
            bundle.putInt(Tags.PAGE, ChatFragment.FROM_CHAT);
            bundle.putParcelable(Tags.message, messageModel);

            Intent push = new Intent(mContext, NotificationHandler.class);
            push.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            push.putExtras(bundle);

            PendingIntent fullScreenPendingIntent = PendingIntent.getActivity(mContext, 0, push, PendingIntent.FLAG_UPDATE_CURRENT);
            notificationBuilder.setContentIntent(fullScreenPendingIntent);

            Notification notification = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setCategory(Notification.CATEGORY_MESSAGE).setFullScreenIntent(fullScreenPendingIntent, true);
                notification = notificationBuilder.build();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                notification = new Notification(R.drawable.logo,
                        message, System.currentTimeMillis());
            } else {
                notification = notificationBuilder.build();
            }

            notification.defaults = Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE;
            notification.flags |= Notification.FLAG_AUTO_CANCEL;
            ((NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE)).notify(getRandomNumer(), notification);

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static int getRandomNumer() {
        Random r = new Random();
        int value = r.nextInt(80 - 65) + 65;
        return value;
    }
}
