package com.stux.Adapters;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.stux.R;

import java.util.Random;

public class SplashPagerAdapter extends PagerAdapter {

    private final Random random = new Random();
    private int mSize;

    public SplashPagerAdapter() {
        mSize = 4;
    }

    public SplashPagerAdapter(int count) {
        mSize = count;
    }

    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        ImageView imageView = new ImageView(view.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        switch (position) {
            case 0:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_01).into(imageView);
                break;
            case 1:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_02).into(imageView);
                break;
            case 2:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_03).into(imageView);
                break;
            case 3:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_04).into(imageView);
                break;
        }
        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

    public void addItem() {
        mSize++;
        notifyDataSetChanged();
    }

    public void removeItem() {
        mSize--;
        mSize = mSize < 0 ? 0 : mSize;

        notifyDataSetChanged();
    }
}