package com.stux.Adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.MovieTime;
import com.stux.R;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

public class MoviesDetailListAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mLayoutInflater;
    private Context mContext;
    private ArrayList<MovieTime> movieArray;
    private OnListItemClickListener itemClickListener;

    public MoviesDetailListAdapter(final Context context, final int textViewResourceId, ArrayList<MovieTime> movieArray, OnListItemClickListener itemClickListener) {
        super(context, textViewResourceId);
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.movieArray = movieArray;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public int getCount() {
        return movieArray.size();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.movies_detail_list_item, parent, false);
            holder = new ViewHolder();
            holder.ll_c_main = (LinearLayout) convertView.findViewById(R.id.ll_c_main);
            holder.txt_c_name = (TextView) convertView.findViewById(R.id.txt_c_name);
            holder.img_trailer = (ImageView) convertView.findViewById(R.id.img_trailer);
            holder.img_loading = (ImageView) convertView.findViewById(R.id.img_loading);
            holder.txt_c_description_1 = (TextView) convertView.findViewById(R.id.txt_c_description_1);
            holder.txt_c_description_2 = (TextView) convertView.findViewById(R.id.txt_c_description_2);
            holder.txt_c_description_3 = (TextView) convertView.findViewById(R.id.txt_c_description_3);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try {
            holder.txt_c_name.setText(movieArray.get(position).cinema_type);
            holder.txt_c_description_1.setText(movieArray.get(position).cinema_time);
//            if (movieArray.get(position).arrayTime.size() == 3) {
//                holder.txt_c_description_1.setText(movieArray.get(position).arrayTime.get(0));
//                holder.txt_c_description_2.setText(movieArray.get(position).arrayTime.get(1));
//                holder.txt_c_description_3.setText(movieArray.get(position).arrayTime.get(2));
//            } else if (movieArray.get(position).arrayTime.size() == 2) {
//                holder.txt_c_description_1.setText(movieArray.get(position).arrayTime.get(0));
//                holder.txt_c_description_2.setText(movieArray.get(position).arrayTime.get(1));
//                holder.txt_c_description_3.setVisibility(View.GONE);
//            } else if (movieArray.get(position).arrayTime.size() == 1) {
//                holder.txt_c_description_1.setText(movieArray.get(position).arrayTime.get(0));
//                holder.txt_c_description_2.setVisibility(View.GONE);
//                holder.txt_c_description_3.setVisibility(View.GONE);
//            } else {
            holder.txt_c_description_1.setVisibility(View.VISIBLE);
            holder.txt_c_description_2.setVisibility(View.GONE);
            holder.txt_c_description_3.setVisibility(View.GONE);
//            }


            if(AppDelegate.isValidString(movieArray.get(position).image)) {
                holder.img_loading.setVisibility(View.VISIBLE);
                if (holder.img_loading != null) {
                    AnimationDrawable frameAnimation = (AnimationDrawable) holder.img_loading.getDrawable();
                    frameAnimation.setCallback(holder.img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                }
                AppDelegate.LogT("Movie => " + movieArray.get(position).image);
                Picasso.with(mContext).load(movieArray.get(position).image).into(holder.img_trailer, new Callback() {
                    @Override
                    public void onSuccess() {
                        holder.img_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
//
//
//            Picasso.with(mContext).load(movieArray.get(position).image).into(new Target() {
//                @Override
//                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
//                    holder.img_trailer.setImageBitmap(bitmap);
//                    AppDelegate.LogT("onBitmapLoaded called");
//                    holder.img_loading.setVisibility(View.GONE);
//                    try {
//                        notifyDataSetChanged();
//                    } catch (Exception e) {
//                        AppDelegate.LogE(e);
//                    }
//                }
//
//                @Override
//                public void onBitmapFailed(Drawable errorDrawable) {
//                }
//
//                @Override
//                public void onPrepareLoad(Drawable placeHolderDrawable) {
//                }
//            });


//            holder.ll_c_main.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (itemClickListener != null) {
//                        itemClickListener.setOnListItemClickListener(Tags.LIST_ITEM_TRENDING, position);
//                    }
//                }
//            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

        return convertView;
    }

    static class ViewHolder {
        LinearLayout ll_c_main;
        ImageView img_trailer, img_loading;
        TextView txt_c_name, txt_c_description_1, txt_c_description_2, txt_c_description_3;
    }

}