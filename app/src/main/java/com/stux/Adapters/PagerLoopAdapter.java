package com.stux.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.stux.fragments.BannerHomeFragment;

import java.util.List;

/**
 * The <code>PagerAdapter</code> serves the fragments when paging.
 *
 * @author mwho
 */
public class PagerLoopAdapter extends FragmentStatePagerAdapter {
    public static int LOOPS_COUNT = 4000;
    public List<Fragment> fragments;

    /**
     * Constructor of the class
     *
     * @param fragments
     */
    public PagerLoopAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
        if (fragments.size() == 0) {
            LOOPS_COUNT = 0;
        } else {
            LOOPS_COUNT = 4000;
        }
    }

    /**
     * This method will be invoked when a page is requested to create
     */
    @Override
    public Fragment getItem(int position) {
        if (position > 0)
            position = position % fragments.size();
        Fragment fragment = new BannerHomeFragment();
        switch (position) {
            case 0:
                fragment.setArguments(fragments.get(0).getArguments());
                break;
            case 1:
                fragment.setArguments(fragments.get(1).getArguments());
                break;
            case 2:
                fragment.setArguments(fragments.get(2).getArguments());
                break;
            case 3:
                fragment.setArguments(fragments.get(3).getArguments());
                break;
            default:
                fragment.setArguments(fragments.get(0).getArguments());
                break;
        }
        return fragment;
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount() {
        return LOOPS_COUNT;
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}