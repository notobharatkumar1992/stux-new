package com.stux.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.stux.AppDelegate;
import com.stux.Models.Place;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.List;

import carbon.widget.LinearLayout;

public class LocationMapAdapter extends BaseAdapter {

    private Context mContext;
    public List<Place> placesArray;
    public OnListItemClickListener clickListener;

    public LocationMapAdapter(Context mContext, List<Place> placesArray, OnListItemClickListener clickListener) {
        this.mContext = mContext;
        this.placesArray = placesArray;
        this.clickListener = clickListener;
    }

    @Override
    public int getCount() {
        return placesArray.size();
    }

    @Override
    public Object getItem(int position) {
        return placesArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.map_list_item, null);

            holder = new Holder();
            holder.txt_name = (TextView) convertView
                    .findViewById(R.id.txt_name);
            holder.txt_description = (TextView) convertView
                    .findViewById(R.id.txt_description);
            holder.ll_c_main_item = (LinearLayout) convertView
                    .findViewById(R.id.ll_c_main_item);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.txt_name.setTag(position);
        holder.txt_description.setTag(position);

        holder.txt_name.setText("");
        holder.txt_description.setText("");

        if (placesArray != null && placesArray.size() > 0) {
            Place place = placesArray.get(position);
            if (place.getName() != null
                    && !place.getName().equalsIgnoreCase("")) {
                holder.txt_name.setText(place.getName());
            }

            if (place.getVicinity() != null
                    && !place.getVicinity().equalsIgnoreCase("")) {
                holder.txt_description.setText(place.getVicinity());
            }
        } else {
            AppDelegate
                    .LogE("LocationMapAdapter length placesArray is null or 0 = "
                            + position);
        }
        holder.ll_c_main_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickListener != null)
                    clickListener.setOnListItemClickListener(Tags.ADDRESS, position);
            }
        });
        return convertView;
    }

    class Holder {
        public TextView txt_name, txt_description;
        public LinearLayout ll_c_main_item;
    }

}
