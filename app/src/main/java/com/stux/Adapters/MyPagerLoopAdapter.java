package com.stux.Adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.stux.R;

/**
 * Created by Bharat on 10/03/2016.
 */
public class MyPagerLoopAdapter extends android.support.v4.view.PagerAdapter {

    public static int LOOPS_COUNT = 4000;
    int count;

    public MyPagerLoopAdapter(int count) {
        this.count = count;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        position = position % count; // use modulo for infinite cycling
        ImageView imageView = new ImageView(view.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        switch (position) {
            case 0:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_01).into(imageView);
                break;
            case 1:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_02).into(imageView);
                break;
            case 2:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_03).into(imageView);
                break;
            case 3:
                Picasso.with(view.getContext()).load(R.drawable.tutorial_04).into(imageView);
                break;
        }
        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }


    @Override
    public int getCount() {
        return count * LOOPS_COUNT; // simulate infinite by big number of products
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}