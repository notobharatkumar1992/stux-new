package com.stux.Adapters;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.Models.SliderModel;
import com.stux.R;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.util.ArrayList;
import java.util.Random;

public class HomeBannerAdapter extends PagerAdapter {

    private final Random random = new Random();
    private ArrayList<SliderModel> sliderArray;
    private OnListItemClickListener onListItemClickListener;
    private Context mContext;

    public HomeBannerAdapter() {
    }

    public HomeBannerAdapter(Context mContext, ArrayList<SliderModel> sliderArray, OnListItemClickListener onListItemClickListener) {
        this.mContext = mContext;
        this.sliderArray = sliderArray;
        this.onListItemClickListener = onListItemClickListener;
    }

    @Override
    public int getCount() {
        return sliderArray.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View viewBanner = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.home_banner_view, null, false);

        final ImageView img_loading = (ImageView) viewBanner.findViewById(R.id.img_loading);
        img_loading.setImageDrawable(view.getContext().getResources().getDrawable(R.drawable.black_spinner));
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        ImageView img_c_large = (carbon.widget.ImageView) viewBanner.findViewById(R.id.img_c_large);
        img_c_large.setScaleType(ImageView.ScaleType.FIT_XY);
        img_c_large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null)
                    onListItemClickListener.setOnListItemClickListener(Tags.LIST_ITEM_TRENDING, position);
            }
        });
        Picasso.with(view.getContext()).load(sliderArray.get(position).banner_thumb_image).into(img_c_large, new Callback() {
            @Override
            public void onSuccess() {
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });


        return viewBanner;
    }
}