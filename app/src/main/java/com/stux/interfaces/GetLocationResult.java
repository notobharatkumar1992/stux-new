package com.stux.interfaces;

import com.google.android.gms.maps.model.LatLng;
import com.stux.Models.Place;

public interface GetLocationResult {

    public void onReciveApiResult(String ApiName, String ActionName, LatLng latLng);

    public void onReciveApiResult(String ApiName, String ActionName, Place placeDetail);

}
