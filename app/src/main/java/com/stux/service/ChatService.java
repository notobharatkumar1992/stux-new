package com.stux.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import com.stux.AppDelegate;
import com.stux.Models.Message;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.PushNotificationService;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.Tags;
import com.stux.fragments.ChatFragment;

import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class ChatService extends Service {

    public static final String DISCONNECT = "disconnect";
    public static final String MESSAGE = "message";
    public static final String ADD_USER = "add user";

    public ChatService() {
    }

    public Prefs prefs;
    public UserDataModel dataModel;
    public static Socket mSocket;
    private boolean isConnected = true;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        AppDelegate.LogS("onCreate called");
        onCreateChat();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
//        callLogoutAsync();
        AppDelegate.LogS("onTaskRemoved called");

        Intent restartService = new Intent(getApplicationContext(),
                this.getClass());
        restartService.setPackage(getPackageName());
        PendingIntent restartServicePI = PendingIntent.getService(
                getApplicationContext(), 1, restartService,
                PendingIntent.FLAG_ONE_SHOT);

        //Restart the service once it has been killed android
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 100, restartServicePI);
    }

    public void onCreateChat() {
        AppDelegate.LogC("onCreateChat called");
        mSocket = AppDelegate.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);

        mSocket.on("send", onNewMessage);
//        mSocket.on("private room created", privateRoom);

//      mSocket.on("add user", onNewMessage);
        mSocket.connect();

//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("sender_id", "15");
//            jsonObject.put("receiver_id", "26");
//            jsonObject.put("product_id", "22");
//            jsonObject.put("message", "test message");
//            jsonObject.put("status", "1");
//        } catch (JSONException e) {
//            AppDelegate.LogE(e);
//        }
//        ChatService.mSocket.emit("send message", jsonObject.toString());
    }

    public Emitter.Listener privateRoom = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
//          {"message":{"status":"1","sender_id":"16","reciver_id":"36","product_id":"36","message":"uhvu"}}
            AppDelegate.LogC("onNewMessage called => " + args[0]);
            JSONObject data = (JSONObject) args[0];
            Message message = new Message();
            JSONObject jsonObject = new JSONObject();
            if (dataModel == null)
                return;
            try {
                jsonObject = data.getJSONObject(Tags.message);
                if (jsonObject.getString(Tags.receiver_id).equalsIgnoreCase(dataModel.userId)) {
                    message.mMessage = jsonObject.getString(Tags.message);
                    message.product_id = jsonObject.getString(Tags.product_id);
                    message.receiver_id = jsonObject.getString(Tags.receiver_id);
                    message.sender_id = jsonObject.getString(Tags.sender_id);
                    message.status = jsonObject.getString(Tags.status);
                    message.mType = Message.TYPE_MESSAGE;
                    message.user_type = Message.USER_RECEIVER;

                    message.product_name = jsonObject.getJSONObject(Tags.product).getString("title");
                    message.product_image = jsonObject.getJSONObject(Tags.product).getString("image_1");
                    message.productModel = new ProductModel();
                    message.productModel.id = message.product_id;
                    message.productModel.title = message.product_name;
                    message.productModel.image_1 = message.product_name;

                    message.dataModel = new UserDataModel();
                    message.dataModel.userId = message.receiver_id;
                    message.dataModel.first_name = jsonObject.getJSONObject(Tags.user).getString("first_name");
                    message.dataModel.last_name = jsonObject.getJSONObject(Tags.user).getString("last_name");

                    if (ChatFragment.onReciveSocketMessage != null) {
                        ChatFragment.onReciveSocketMessage.setOnReciveSocketMessage(Tags.CHAT, message);
                    } else {
                        PushNotificationService.showUserChatNotification(ChatService.this, message);
                    }

                } else {
                    AppDelegate.LogE("ignore => " + data.toString());
                }
            } catch (JSONException e) {
                AppDelegate.LogE(e);
            }
        }
    };

    public Emitter.Listener userId = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            AppDelegate.LogC("userId called => " + args[0]);
        }
    };

    public Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (!isConnected) {
                showToast(getString(R.string.connect));
                isConnected = true;
            }

            AppDelegate.LogC("onConnected called");
        }
    };

    public Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            isConnected = false;
            try {
                showToast(getString(R.string.disconnect));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.LogC("onDisconnect called");
        }
    };

    public Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                showToast(getString(R.string.error_connect));
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            AppDelegate.LogC("onConnectError called");
        }
    };

    public Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
//            String s = "{\"key\":\"value\"}";
            AppDelegate.LogC("onNewMessage called => " + args[0]);
            if (dataModel == null)
                dataModel = prefs.getUserdata();
            if (dataModel == null) {
                return;
            }
            JSONObject data = (JSONObject) args[0];
            Message message = new Message();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject = data.getJSONObject(Tags.message);
                if (jsonObject.getString(Tags.receiver_id).equalsIgnoreCase(dataModel.userId)) {
                    message.mMessage = jsonObject.getString(Tags.message);
                    message.product_id = jsonObject.getString(Tags.product_id);
                    message.receiver_id = jsonObject.getString(Tags.receiver_id);
                    message.sender_id = jsonObject.getString(Tags.sender_id);
                    message.status = jsonObject.getString(Tags.status);
                    message.created = jsonObject.getString(Tags.created);
                    message.mType = Message.TYPE_MESSAGE;
                    message.user_type = Message.USER_RECEIVER;

                    message.product_name = jsonObject.getJSONObject(Tags.product).getString("title");
                    message.product_image = jsonObject.getJSONObject(Tags.product).getString("image_1");
                    message.productModel = new ProductModel();
                    message.productModel.id = message.product_id;
                    message.productModel.title = message.product_name;
                    message.productModel.image_1 = message.product_image;

                    message.dataModel = new UserDataModel();
                    message.dataModel.userId = message.receiver_id;
                    message.dataModel.first_name = jsonObject.getJSONObject(Tags.user).getString("first_name");
                    message.dataModel.last_name = jsonObject.getJSONObject(Tags.user).getString("last_name");
                    message.dataModel.image = jsonObject.getJSONObject(Tags.user).getString("image");

                    sendCallbackToServer(data.getJSONObject(Tags.message));

                    if (ChatFragment.onReciveSocketMessage != null) {
                        ChatFragment.onReciveSocketMessage.setOnReciveSocketMessage(Tags.CHAT, message);
                    } else {
                        PushNotificationService.showUserChatNotification(ChatService.this, message);
                    }

                } else {
                    AppDelegate.LogE("ignore => " + data.toString());
                }
            } catch (JSONException e) {
                AppDelegate.LogE(e);
            }
        }
    };

    private void sendCallbackToServer(JSONObject jsonObject) {
        JSONObject jsonObject1 = new JSONObject();
        try {
            jsonObject1.put("id", jsonObject.getString(Tags.id));
            jsonObject1.put("sender_id", jsonObject.getString(Tags.sender_id));
            jsonObject1.put("receiver_id", jsonObject.getString(Tags.receiver_id));
            jsonObject1.put("product_id", jsonObject.getString(Tags.product_id));
            jsonObject1.put("message", jsonObject.getString(Tags.message));
            jsonObject1.put("status", "1");
        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }
        ChatService.mSocket.emit("receive message", jsonObject1.toString());
        AppDelegate.LogC("receive message => " + jsonObject1.toString());
    }

    public static void sendCallbackToServer(Message messageModel) {
        try {
            JSONObject jsonObject1 = new JSONObject();
            try {
                jsonObject1.put("id", messageModel.id);
                jsonObject1.put("sender_id", messageModel.sender_id);
                jsonObject1.put("receiver_id", messageModel.receiver_id);
                jsonObject1.put("product_id", messageModel.product_id);
                jsonObject1.put("message", messageModel.mMessage);
                jsonObject1.put("status", "1");
            } catch (JSONException e) {
                AppDelegate.LogE(e);
            }
            ChatService.mSocket.emit("receive message", jsonObject1.toString());
            AppDelegate.LogC("receive message => " + jsonObject1.toString());
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void showToast(final String message) {
        try {
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
//                    Toast.makeText(ChatService.this, message, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

//    public Emitter.Listener onUserJoined = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            String username;
//            int numUsers;
//            try {
//                username = data.getString("username");
//                numUsers = data.getInt("numUsers");
//            } catch (JSONException e) {
//                AppDelegate.LogE(e);
//                return;
//            }
////            addLog(getResources().getString(R.string.message_user_joined, username));
////            addParticipantsLog(numUsers);
//            AppDelegate.LogC("onUserJoined called");
//        }
//    };
//
//    public Emitter.Listener onUserLeft = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            String username;
//            int numUsers;
//            try {
//                username = data.getString("username");
//                numUsers = data.getInt("numUsers");
//            } catch (JSONException e) {
//                AppDelegate.LogE(e);
//                return;
//            }
//
////            addLog(getResources().getString(R.string.message_user_left, username));
////            addParticipantsLog(numUsers);
////            removeTyping(username);
//            AppDelegate.LogC("onUserLeft called");
//        }
//    };
//
//    public Emitter.Listener onTyping = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            String username;
//            try {
//                username = data.getString("username");
//            } catch (JSONException e) {
//                AppDelegate.LogE(e);
//                return;
//            }
////            addTyping(username);
//            AppDelegate.LogC("onTyping called");
//        }
//    };
//
//    public Emitter.Listener onStopTyping = new Emitter.Listener() {
//        @Override
//        public void call(final Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            String username;
//            try {
//                username = data.getString("username");
//            } catch (JSONException e) {
//                AppDelegate.LogE(e);
//                return;
//            }
////            removeTyping(username);
//            AppDelegate.LogC("onStopTyping called");
//        }
//    };
//
//    public Runnable onTypingTimeout = new Runnable() {
//        @Override
//        public void run() {
////            if (!mTyping) return;
////            mTyping = false;
//            mSocket.emit("stop typing");
//            AppDelegate.LogC("onTypingTimeout called");
//        }
//    };

}
