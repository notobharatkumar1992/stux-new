package com.stux.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.Message;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.PushNotificationService;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.fragments.ChatFragment;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Bharat on 08/29/2016.
 */
public class ReceiveNotificationService extends Service implements OnReciveServerResponse {

    private Prefs prefs;
    private UserDataModel dataModel;

    @Override
    public void onCreate() {
        super.onCreate();
        prefs = new Prefs(this);
        dataModel = prefs.getUserdata();
        AppDelegate.LogS("ReceiveNotificationService onCreate called");
        execute_getPendingNotificationAsync();
    }

    private void execute_getPendingNotificationAsync() {
        try {
            if (AppDelegate.haveNetworkConnection(this, false)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Tags.receiver_id, dataModel.userId);
                PostAsync mPostAsyncObj = new PostAsync(this,
                        this, ServerRequestConstants.PENDING_MSGLIST,
                        mPostArrayList, null);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.PENDING_MSGLIST)) {
            parseResponse(result);
        }
    }

    private void parseResponse(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.status).equalsIgnoreCase("1")) {
                if (object.has(Tags.response)) {
                    JSONArray jsonArray = object.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        Message message = new Message();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        try {
                            message.mMessage = jsonObject.getString(Tags.message);
                            message.product_id = jsonObject.getString(Tags.product_id);
                            message.receiver_id = jsonObject.getString(Tags.receiver_id);
                            message.sender_id = jsonObject.getString(Tags.sender_id);
                            message.status = jsonObject.getString(Tags.status);
                            message.created = jsonObject.getString(Tags.created);
                            message.mType = Message.TYPE_MESSAGE;
                            message.user_type = Message.USER_RECEIVER;

                            if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.product))) {
                                message.product_name = jsonObject.getJSONObject(Tags.product).getString("title");
                                message.product_image = jsonObject.getJSONObject(Tags.product).getString("image_1");
                                message.productModel = new ProductModel();
                                message.productModel.id = message.product_id;
                                message.productModel.title = message.product_name;
                                message.productModel.image_1 = message.product_name;

                                message.dataModel = new UserDataModel();
                                message.dataModel.userId = message.receiver_id;
                                message.dataModel.first_name = jsonObject.getJSONObject(Tags.sender_data).getString("first_name");
                                message.dataModel.last_name = jsonObject.getJSONObject(Tags.sender_data).getString("last_name");
                                message.dataModel.image = jsonObject.getJSONObject(Tags.sender_data).getString("image");
//                        sendCallbackToServer(data.getJSONObject(Tags.message));
                                if (ChatFragment.onReciveSocketMessage != null) {
                                    ChatFragment.onReciveSocketMessage.setOnReciveSocketMessage(Tags.CHAT, message);
                                } else {
                                    PushNotificationService.showUserChatNotification(this, message);
                                }
                            } else {
                                AppDelegate.LogE("Product id not find message.product_id => " + message.product_id);
                            }
                        } catch (JSONException e) {
                            AppDelegate.LogE(e);
                        }
                    }
                } else {

                }
            } else {

            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
