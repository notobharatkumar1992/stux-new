package com.stux.parser;

import com.stux.AppDelegate;

import org.json.JSONObject;

/**
 * Created by parul on 26/12/15.
 */
public class Fb_details extends JSONObject {

    public Fb_detail_GetSet getFacebookDetail(String Result) {
        if (Result != null) {
            Fb_detail_GetSet userDetailObj = new Fb_detail_GetSet();
            try {
                JSONObject fb_object = new JSONObject(Result);

                if (fb_object.has("id")) {
                    userDetailObj.setId(fb_object.optString("id"));
                    userDetailObj.setImage("http://graph.facebook.com/" + fb_object.optString("id") + "/picture?type=large");
                }
                if (fb_object.has("first_name")) {
                    userDetailObj.setF_name(fb_object.optString("first_name"));
                }
                if (fb_object.has("last_name")) {
                    userDetailObj.setL_name(fb_object.optString("last_name"));
                }
                if (fb_object.has("gender")) {
                    userDetailObj.setGender(fb_object.optString("gender"));
                }
                if (fb_object.has("birthday")) {
                    userDetailObj.setDob(fb_object.optString("birthday"));
                }
                if (fb_object.has("email")) {
                    userDetailObj.setEmail(fb_object.optString("email"));
                }

            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
            return userDetailObj;
        } else {
            return null;
        }
    }


}
