package com.stux.constants;

public class ServerRequestConstants {
    /**
     * (All Constants that were used in server connection are declared)
     */

	/*
     * Post Parameter Type Constants
	 */
    public static final String CODE_200 = "200";
    public static final String CODE_400 = "400";
    public static final String CODE_401 = "401";
    public static final String CODE_402 = "402";
    public static final String CODE_403 = "403";
    public static final String CODE_404 = "404";

    public static final String Key_PostStrValue = "String";
    public static final String Key_PostDoubleValue = "double";
    public static final String Key_PostintValue = "int";
    public static final String Key_PostbyteValue = "byte";
    public static final String Key_PostFileValue = "File";
    public static final String Key_PostStringArrayValue = "String[]";
    public static final String Key_PostStringArrayValue1 = "String[]";

    //    public static final String CHAT_SERVER_URL = "http://192.168.100.38:3000";
    public static final String CHAT_SERVER_URL = "http://notosolutions.net:3000";
    public static final String BASE_URL = "http://notosolutions.net/stux/web-services/";
//    public static final String BASE_URL = "http://192.168.100.38/stux/web-services/";

    public static final String GET_STATE_LIST = BASE_URL + "getStateList";
    public static final String GET_INSTITUTION_NAME = BASE_URL + "getInstitutionName";
    public static final String GET_DEPARTMENT = BASE_URL + "getDepartmentName";
    public static final String REGISTRATION = BASE_URL + "registration";

    public static final String FORGOT_PASSWORD = BASE_URL + "forgotPassword";
    public static final String RESET_PASSWORD = BASE_URL + "resetpassword";
    public static final String LOGIN = BASE_URL + "login";
    public static final String FAV_CATEGORY = BASE_URL + "favCategory";

    public static final String EDIT_PROFILE = BASE_URL + "editProfile";

    public static final String SLIDERS_LIST = BASE_URL + "slidersList";
    public static final String SLIDERS_CLICKS = BASE_URL + "getSliderClicks";

    public static final String CAMPUS_LIST = BASE_URL + "getcampusList";
    public static final String EVENTS_LIST = BASE_URL + "getEvents";
    public static final String DEALS_LIST = BASE_URL + "getTopDeals";
    public static final String MOVIES_LIST = BASE_URL + "getMovies";
    public static final String CHANGE_PASSWORD = BASE_URL + "changePassword";

    public static final String GET_MY_PRODUCT = BASE_URL + "getMyProduct";

    public static final String GET_USER_REVIEW = BASE_URL + "getuserReviews";
    public static final String ADD_REVIEW = BASE_URL + "addReviews";

    public static final String GET_FOLLOWERS = BASE_URL + "getFollowers";
    public static final String GET_FOLLOWING = BASE_URL + "getFollowing";
    public static final String FOLLOW = BASE_URL + "follow";

    public static final String GET_USER = BASE_URL + "getUserData";

    public static final String ITEM_VIEW = BASE_URL + "itemView";
    public static final String ITEM_SOLD = BASE_URL + "itemSold";
    public static final String ITEM_DELETE = BASE_URL + "deleteProduct";

    public static final String GET_DEAL_CATEGORY = BASE_URL + "getDealCategories";
    public static final String GET_PRODUCT_CATEGORY = BASE_URL + "getProductCategories";
    public static final String GET_EVENT_CATEGORY = BASE_URL + "getEventType";

    public static final String CREATE_PRODUCT = BASE_URL + "createProduct";
    public static final String EDIT_PRODUCT = BASE_URL + "editProduct";
    public static final String CREATE_EVENT = BASE_URL + "createEvent";
    public static final String EDIT_EVENT = BASE_URL + "editEvent";

    public static final String MOVIES_VIEW = BASE_URL + "movieView";
    public static final String EVENT_VIEW = BASE_URL + "eventView";
    public static final String DEALS_VIEW = BASE_URL + "dealsView";

    public static final String DEAL_DETAIL = BASE_URL + "dealDetail";


    public static final String GET_MY_EVENTS = BASE_URL + "getMyEvents";
    public static final String MY_COUPON = BASE_URL + "MyCoupons";
    public static final String GRAB_COUPONS = BASE_URL + "grabCoupons";

    public static final String HELP_TERMS_AND_CONDITIONS = "http://notosolutions.net/stux/pages/Display/NQ==";
    public static final String HELP_PRIVACY_POLICY = "http://notosolutions.net/stux/pages/Display/MjI=";
    public static final String HELP_SAFETY_GUIDELINE = "http://notosolutions.net/stux/pages/Display/MjM=";
    public static final String HELP_STUX_RULES = "http://notosolutions.net/stux/pages/Display/MjQ=";
    public static final String HELP_FAQ = "http://notosolutions.net/stux/pages/Display/MjU=";

    public static final String FEEDBACK = BASE_URL + "feedback";

    public static final String PRODUCT_DETAIL = BASE_URL + "productDetail";
    public static final String REPORT_PRODUCT = BASE_URL + "ReportProduct";
    public static final String MAKE_OFFER = BASE_URL + "makeOffer";

    public static final String GET_JOBS = BASE_URL + "getJobs";

    public static final String ADD_ITEM_FOUND = BASE_URL + "addItemFound";
    public static final String GET_FOUND_LIST = BASE_URL + "foundList";

    public static final String REFINE_PRODUCTS = BASE_URL + "refineProducts";

    public static final String GET_NOTIFICATION = BASE_URL + "getNotifications";


    public static final String LOGOUT = BASE_URL + "logout";

    public static final String CHAT_LIST = BASE_URL + "chatList";
    public static final String USER_CHAT_LIST = BASE_URL + "userChatList";

    public static final String DELETE_CHAT = BASE_URL + "deleteChat";
//    public static final String HELP_STUX_RULES = "http://notosolutions.net/stux/pages/Display/MjU=";

    public static final String PRODUCT_LIKES = BASE_URL + "productLikes";
    public static final String GET_COMMENT_LIST = BASE_URL + "getcommentList";
    public static final String PRODUCT_COMMENTS = BASE_URL + "productComments";

    public static final String PENDING_MSGLIST = BASE_URL + "pendingMsgList";

    public static final String EVENT_LIKES = BASE_URL + "eventLikes";
    public static final String GRAB_COUPON_DELETE = BASE_URL + "grabcouponDelete";

}

