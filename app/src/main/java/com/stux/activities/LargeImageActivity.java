package com.stux.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;

import com.stux.Adapters.LargeImagePagerAdapter;
import com.stux.AppDelegate;
import com.stux.R;
import com.stux.constants.Tags;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;

/**
 * Created by Bharat on 06/23/2016.
 */
public class LargeImageActivity extends FragmentActivity {

    private ImageView img_large;
    private ProgressBar progressbar;

    private android.widget.LinearLayout pager_indicator;
    private ViewPager view_pager;
    private LargeImagePagerAdapter mPagerAdapter;
    public ArrayList<String> arrayString = new ArrayList<>();
    public String image_1, image_2, image_3, image_4;

    public int count = 0, position = 0, dotsCount;
    private android.widget.ImageView[] dots;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.large_image_layout);
        initView();
    }

    private void initView() {
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
//        progressbar.setVisibility(View.VISIBLE);

        findViewById(R.id.img_c_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        count = getIntent().getExtras().getInt(Tags.count);
        position = getIntent().getExtras().getInt(Tags.POSITION);

        arrayString.clear();
        if (AppDelegate.isValidString(getIntent().getExtras().getString(Tags.image_1))) {
            image_1 = getIntent().getExtras().getString(Tags.image_1);
            arrayString.add(image_1);
        }
        if (AppDelegate.isValidString(getIntent().getExtras().getString(Tags.image_2))) {
            image_2 = getIntent().getExtras().getString(Tags.image_2);
            arrayString.add(image_2);
        }
        if (AppDelegate.isValidString(getIntent().getExtras().getString(Tags.image_3))) {
            image_3 = getIntent().getExtras().getString(Tags.image_3);
            arrayString.add(image_3);
        }
        if (AppDelegate.isValidString(getIntent().getExtras().getString(Tags.image_4))) {
            image_4 = getIntent().getExtras().getString(Tags.image_4);
            arrayString.add(image_4);
        }

//        img_large = (ImageView) findViewById(R.id.img_large);
//        Picasso.with(this).load(getIntent().getExtras().getString(Tags.image)).into(img_large,
//                new com.squareup.picasso.Callback() {
//                    @Override
//                    public void onSuccess() {
//                        progressbar.setVisibility(View.GONE);
//                    }
//
//                    @Override
//                    public void onError() {
//                        progressbar.setVisibility(View.GONE);
//                        AppDelegate.showToast(LargeImageActivity.this, "Image failed to load, please try after some time.");
//                    }
//                });


        pager_indicator = (android.widget.LinearLayout) findViewById(R.id.pager_indicator);

        view_pager = (ViewPager) findViewById(R.id.view_pager);
        mPagerAdapter = new LargeImagePagerAdapter(LargeImageActivity.this, arrayString, null);
        view_pager.setAdapter(mPagerAdapter);
        setUiPageViewController();
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);

        if (mPagerAdapter.getCount() > position) {
            view_pager.setCurrentItem(position);
            switchBannerPage(position);
        }
    }

    private void switchBannerPage(int position) {
        try {
            if (dotsCount > 0) {
                for (int i = 0; i < arrayString.size(); i++) {
                    if (dots != null && dots.length > i)
                        dots[i].setImageResource(R.drawable.white_radius_square);
                }
                if (dots != null && dots.length > position)
                    dots[position].setImageResource(R.drawable.orange_radius_square);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = arrayString.size();
        if (dotsCount > 0) {
            dots = new android.widget.ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new android.widget.ImageView(this);
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(25, 25);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(15, 15);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }
}
