package com.stux.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.GCMClientManager;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.Tags;
import com.stux.fragments.LoginTutorialFragment;
import com.stux.fragments.NoInternetConnectionFragment;
import com.stux.fragments.SignInFragment;
import com.stux.service.ChatService;

/**
 * Created by NOTO on 5/26/2016.
 */
public class SplashActivity extends FragmentActivity {

    private ImageView img_splash;
    private Prefs prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        AppDelegate.getHashKey(SplashActivity.this);
        AppDelegate.LogT("values folder called = " + getString(R.string.values_folder));
        prefs = new Prefs(this);
        initView();
        initGCM();
        showFragment();
        startService(new Intent(this, ChatService.class));
    }

    private void initView() {
        img_splash = (ImageView) findViewById(R.id.img_splash);
        Picasso.with(SplashActivity.this).load(R.drawable.splash).into(img_splash);
    }

    private void showFragment() {
        if (getIntent().getExtras() != null && getIntent().getExtras().getString(Tags.IS_SPLASH) != null) {
            if (getIntent().getExtras().getString(Tags.IS_SPLASH).equalsIgnoreCase(Tags.FALSE)) {
                checkValidUser();
            }
        } else if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserdata().userId)) {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
            finish();
        } else {
            img_splash.setVisibility(View.VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkValidUser();
                }
            }, 2000);
        }
    }

    private void checkValidUser() {
        try {
            if (prefs.getUserdata() != null && AppDelegate.isValidString(prefs.getUserdata().userId)) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            } else {
                AppDelegate.showFragment(this, new LoginTutorialFragment());
                img_splash.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initGCM() {
        try {
            if (!AppDelegate.isValidString(prefs.getGCMtoken())) {
                new GCMClientManager(this).registerIfNeeded(new GCMClientManager.RegistrationCompletedHandler() {
                    @Override
                    public void onSuccess(final String regId, boolean isNewRegistration) {
                        if (AppDelegate.isValidString(regId)) {
                            AppDelegate.LogGC("registrationId = " + regId + ", isNewRegistration = " + isNewRegistration);
                            prefs.setGCMtoken(regId);
                        } else {
                            initGCM();
                        }
                    }

                    @Override
                    public void onFailure(String ex) {
                        super.onFailure(ex);
                        AppDelegate.LogE("GCMClientManager onFailure = " + ex);
                    }
                });
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        prefs.clearTempPrefs();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof NoInternetConnectionFragment) {
            if (AppDelegate.haveNetworkConnection(SplashActivity.this, false)) {
                super.onBackPressed();
            }
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof SignInFragment) {
            AppDelegate.showFragmentAnimationOppose(getSupportFragmentManager(), new LoginTutorialFragment());
        } else if (getSupportFragmentManager().findFragmentById(R.id.main_content) instanceof LoginTutorialFragment) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("Login onActivityResult called");
        if (SignInFragment.callbackManager != null)
            SignInFragment.callbackManager.onActivityResult(requestCode, resultCode, data);
        else if (LoginTutorialFragment.callbackManager != null)
            LoginTutorialFragment.callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
