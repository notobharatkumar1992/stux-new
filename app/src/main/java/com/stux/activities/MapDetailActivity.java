package com.stux.activities;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.stux.AppDelegate;
import com.stux.Async.LocationAddress;
import com.stux.R;
import com.stux.Utils.WorkaroundMapFragment;
import com.stux.constants.Tags;

/**
 * Created by Bharat on 07/29/2016.
 */
public class MapDetailActivity extends AppCompatActivity {

    private GoogleMap map_business;
    private double lat = 0.0, lng = 0.0;
    private String name = "";
    public static Activity mActivity;
    private Handler mHandler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_detail_page);
        mActivity = this;
        setHandler();
        try {
            lat = Double.parseDouble(getIntent().getStringExtra(Tags.LAT));
            lng = Double.parseDouble(getIntent().getStringExtra(Tags.LNG));
            name = getIntent().getStringExtra(Tags.name);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.haveNetworkConnection(this) && !AppDelegate.isValidString(name)) {
            setLatLngAndFindAddress(new LatLng(lat, lng), 100);
        }
        map_business = ((WorkaroundMapFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_map)).getMap();
        showMap();
    }


    private void setHandler() {
        try {
            mHandler = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);
                    if (msg.what == 10) {
                        AppDelegate.showProgressDialog(MapDetailActivity.this);
                    } else if (msg.what == 11) {
                        AppDelegate.hideProgressDialog(MapDetailActivity.this);
                    } else if (msg.what == 2) {
                        setResultFromGeoCoderApi(msg.getData());
                    }
                }
            };
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private String place_name = "", place_address = "";

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            place_name = bundle.getString(Tags.PLACE_NAME);
            place_address = bundle.getString(Tags.PLACE_ADD);
            showMap();
        } else {
            AppDelegate.showToast(MapDetailActivity.this, "Location not available or something went wrong with server, Please try again later.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mActivity = null;
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
        LocationAddress.getAddressFromLocation(arg0.latitude, arg0.longitude, this, mHandler);
    }

    private void showMap() {
        if (map_business == null) {
            return;
        }
        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        map_business.clear();
        if (AppDelegate.isValidString(name)) {
            map_business.addMarker(AppDelegate.getMarkerOptionsWithTitleSnippet(this, new LatLng(lat, lng), R.drawable.map_pin, 100, 100, name));
        } else
            map_business.addMarker(AppDelegate.getMarkerOptions(this, new LatLng(lat, lng), R.drawable.map_pin));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, lng))
                .zoom(14).build();
        //Zoom in and animate the camera.
        map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
}
