package com.stux.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/27/2016.
 */
public class ContactUsFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    public Prefs prefs;
    public UserDataModel dataModel;
    public Handler mHandler;

    private EditText et_title, et_email, et_description;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.contact_us, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                }
            }
        };
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Contact Us");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        ((TextView) view.findViewById(R.id.txt_c_right)).setText("Publish");
        view.findViewById(R.id.txt_c_right).setVisibility(View.GONE);
        view.findViewById(R.id.txt_c_right).setOnClickListener(this);


        et_title = (EditText) view.findViewById(R.id.et_title);
        et_title.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_email = (EditText) view.findViewById(R.id.et_email);
        et_email.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_description = (EditText) view.findViewById(R.id.et_description);
        et_description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));

        view.findViewById(R.id.txt_c_submit).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.txt_c_submit:
                execute_contactUsAsync();
                break;
        }
    }

    private void execute_contactUsAsync() {
        if (et_title.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter title.");
        } else if (et_email.length() == 0) {
            AppDelegate.showAlert(getActivity(), getActivity().getString(R.string.Alert_Enter_email));
        } else if (!(AppDelegate.CheckEmail(et_email.getText().toString())) /*|| !et_email.getText().toString().substring(et_email.getText().toString().indexOf("@"), et_email.getText().toString().length()).equals("@edu.com")*/) {
            AppDelegate.showAlert(getActivity(), getString(R.string.please_fill_email_address_in_format));
        } else if (et_description.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter description.");
        } else if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.username, dataModel.first_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.title, et_title.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, et_email.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.description, et_description.getText().toString());

            PostAsync mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.FEEDBACK,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FEEDBACK)) {
            mHandler.sendEmptyMessage(11);
            parseContactUsResult(result);
        }
    }

    private void parseContactUsResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.MESSAGE));
                getFragmentManager().popBackStack();
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }
}
