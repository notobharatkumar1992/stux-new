package com.stux.fragments;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.stux.AppDelegate;
import com.stux.R;
import com.stux.activities.MainActivity;
import com.stux.constants.Tags;
import com.stux.parser.Fb_detail_GetSet;
import com.stux.parser.Fb_details;

import org.json.JSONObject;

import java.util.Arrays;

import carbon.widget.TextView;

/**
 * Created by Bharat on 07/25/2016.
 */
public class HelpFragment extends Fragment implements View.OnClickListener {

    private Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.help, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        facebookSDKInitialize();
        initView(view);
    }

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Help");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        view.findViewById(R.id.txt_c_terms_condition).setOnClickListener(this);
        view.findViewById(R.id.txt_c_privacy_policy).setOnClickListener(this);
        view.findViewById(R.id.txt_c_safety_guide).setOnClickListener(this);
        view.findViewById(R.id.txt_c_rules).setOnClickListener(this);

        view.findViewById(R.id.txt_c_like_fb).setOnClickListener(this);
        view.findViewById(R.id.txt_c_like_twitter).setOnClickListener(this);
        view.findViewById(R.id.txt_c_like_twitter).setVisibility(View.GONE);

        view.findViewById(R.id.txt_c_faq).setOnClickListener(this);
        view.findViewById(R.id.txt_c_contact_us).setOnClickListener(this);
        view.findViewById(R.id.txt_c_software).setOnClickListener(this);
        try {
            PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            ((TextView) view.findViewById(R.id.txt_c_version)).setText(pInfo.versionCode + "");
        } catch (PackageManager.NameNotFoundException e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getActivity().getSupportFragmentManager().popBackStack();
                break;

            case R.id.txt_c_terms_condition:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 0);
                AppDelegate.showFragmentAnimation(getActivity(), new WebViewDetailFragment(), bundle, null);
                break;

            case R.id.txt_c_privacy_policy:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 1);
                AppDelegate.showFragmentAnimation(getActivity(), new WebViewDetailFragment(), bundle, null);
                break;

            case R.id.txt_c_safety_guide:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 2);
                AppDelegate.showFragmentAnimation(getActivity(), new WebViewDetailFragment(), bundle, null);
                break;

            case R.id.txt_c_rules:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 3);
                AppDelegate.showFragmentAnimation(getActivity(), new WebViewDetailFragment(), bundle, null);
                break;

            case R.id.txt_c_faq:
                bundle = new Bundle();
                bundle.putInt(Tags.FROM, 4);
                AppDelegate.showFragmentAnimation(getActivity(), new WebViewDetailFragment(), bundle, null);
                break;

            case R.id.txt_c_like_fb:
                if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                } else {
                    openFacebook();
                }
                break;

            case R.id.txt_c_contact_us:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new ContactUsFragment());
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callbackManager = null;
    }

    /*Facebook integration for Invite User for app */
    public static CallbackManager callbackManager;
    private String fb_LoginToken;
    private boolean isCalledOnce = false;
    private Fb_detail_GetSet fbUserData;

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
    }


    public void openFacebook() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        AppDelegate.showProgressDialog(getActivity());
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        AppDelegate.LogFB("Graph response" + response.toString());
                                        fbUserData = new Fb_detail_GetSet();
                                        fbUserData = new Fb_details().getFacebookDetail(response.getJSONObject().toString());

                                        AppDelegate.hideProgressDialog(getActivity());
                                        inviteFbFriends();
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("Login onActivityResult called");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void inviteFbFriends() {
        String appLinkUrl, previewImageUrl;
        appLinkUrl = "app url(create it from facebook)"; //your applink url
        previewImageUrl = "image url";//your image url

        appLinkUrl = "app url(create it from facebook)";
        previewImageUrl = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7U-uGd4v8UJ7Xynkjoi9upDVmEczdlvsVPOZpjvbcUbOczcEcpg";
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(getActivity(), content);
        }
        AppDelegate.LogT("inviteFbFriends called");
    }

}
