package com.stux.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.Adapters.OptionsListAdapter;
import com.stux.Adapters.ProductPagerAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.PushNotificationService;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.activities.LargeImageActivity;
import com.stux.activities.MainActivity;
import com.stux.activities.MapDetailActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.Card;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;
import com.wunderlist.slidinglayer.SlidingLayer;
import com.wunderlist.slidinglayer.transformer.AlphaTransformer;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/30/2016.
 */
public class ProductDetailFragment extends Fragment implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse {

    public ArrayList<String> arrayString = new ArrayList<>();
    private MapView mapview;
    private TextView txt_c_header, txt_c_date, txt_c_category, txt_c_description_tittle, txt_c_description, txt_c_image, txt_c_viewer, txt_c_condition, txt_c_address, txt_c_price, txt_c_name, txt_c_price_my_product, txt_c_item_sold, txt_c_chat, txt_c_make_offer, txt_c_like_count, txt_c_comment_count;
    private LinearLayout ll_c_flag, ll_c_actions;
    private RelativeLayout rl_map;
    private CircleImageView cimg_user;
    private ImageView img_product, img_loading, img_like;
    private ScrollView scrollView;
    private GoogleMap map_business;
    public ProductModel productModel;
    private int fromPage = 0, image_count = 0;

    private ViewPager view_pager;
    private ProductPagerAdapter mPagerAdapter;

    private Handler mHandler;

    private Prefs prefs;
    private UserDataModel dataModel;

    public static boolean dataChanged = false;
    private int selected_page = 0;

    public SlidingLayer sl_more;
    public ListView list_options;
    public OptionsListAdapter adapter;
    public ArrayList<String> arrayOptions = new ArrayList<>();

    public static TwitterLoginButton loginButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MapsInitializer.initialize(getActivity());
        return inflater.inflate(R.layout.product_detail_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        productModel = getArguments().getParcelable(Tags.product);
        if (productModel == null || !AppDelegate.isValidString(productModel.id)) {
            AppDelegate.showToast(getActivity(), "Something went wrong please try after some time!!!");
            getFragmentManager().popBackStack();
            return;
        }
        if (getArguments().getInt(Tags.from) != -1)
            fromPage = getArguments().getInt(Tags.from);
        try {
            mapview = (MapView) view.findViewById(R.id.mapview);
            mapview.onCreate(savedInstanceState);
            map_business = mapview.getMap();
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Something went wrong with google map, please try after some time!!!");
            getFragmentManager().popBackStack();
        }
        FacebookSdk.sdkInitialize(getActivity());
        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();
        initView(view);
        setHandler();
//        if (!AppDelegate.isValidString(productModel.cat_id)) {
        callProjectDetailAsync(productModel.id);
//        } else {
//            setValues();
//        }

        loginButton = (TwitterLoginButton) view.findViewById(R.id.login_button);
        loginButton.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                AppDelegate.LogT("Twitter success called");
                if (OriginalPhoto != null) {
                    if (capturedFile == null)
                        writeImageFile(OriginalPhoto);
                    final TwitterSession session = TwitterCore.getInstance().getSessionManager()
                            .getActiveSession();
                    if (capturedFile == null) {
                        AppDelegate.showToast(getActivity(), "Failed to create image for share please try again later.");
                        return;
                    }
                    Card card = new Card.AppCardBuilder(getActivity())
                            .imageUri(Uri.fromFile(capturedFile))
                            .googlePlayId(getActivity().getPackageName())
                            .build();

                    String condition = "";
                    if (productModel.item_condition.equalsIgnoreCase("1")) {
                        condition = "Condition : " + "New";
                    } else if (productModel.item_condition.equalsIgnoreCase("2")) {
                        condition = "Condition : " + "Almost New";
                    } else if (productModel.item_condition.equalsIgnoreCase("3")) {
                        condition = "Condition : " + "Used";
                    }

                    Intent intent = new ComposerActivity.Builder(getActivity())
                            .session(session)
                            .card(card)
                            .hashtags("#" + productModel.title, "#" + condition)
                            .createIntent();
                    startActivity(intent);
                } else {
                    AppDelegate.showToast(getActivity(), "For tweet you have to wait until first image get downloaded.");
                }
            }

            @Override
            public void failure(TwitterException exception) {
                AppDelegate.LogT("Twitter failure called => " + exception);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        callbackManager = null;
        loginButton = null;
        AppDelegate.LogT("ProductDetailFragment onDestroyView called");
    }

    @Override
    public void onResume() {
        super.onResume();
        if (dataChanged) {
            dataChanged = false;
            mHandler.sendEmptyMessage(1);
        }
    }

    private void callProjectDetailAsync(String product_id) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.PRODUCT_DETAIL,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1 || msg.what == 2) {
                    setValues();
                }
            }
        };
    }

    private void executeItemViewApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProductDetailFragment.this, ServerRequestConstants.ITEM_VIEW,
                    mPostArrayList, ProductDetailFragment.this);
//            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void executeItemSoldApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.sold_status, "1");
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProductDetailFragment.this, ServerRequestConstants.ITEM_SOLD,
                    mPostArrayList, ProductDetailFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void executeDeleteProductApi() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProductDetailFragment.this, ServerRequestConstants.ITEM_DELETE,
                    mPostArrayList, ProductDetailFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }


    /**
     * [07/22/2016 06:13:22 PM] kapil dev: Api -> deleteProduct Done
     * [07/22/2016 06:13:38 PM] kapil dev:  Params => product_id
     */
    private void setValues() {
        try {
            if (productModel != null) {
                if (AppDelegate.isValidString(productModel.modified)) {
                    try {
                        Date modifiedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(productModel.modified);
                        txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + new SimpleDateFormat("dd'" + AppDelegate.getDayOfMonthSuffix(new SimpleDateFormat("dd").format(modifiedDate)) + "' MMMM yyyy.").format(modifiedDate) + "  <b>Time : </b>" + new SimpleDateFormat("hh:mmaa.").format(modifiedDate)));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + productModel.modified + ""));
                    }
                } else if (AppDelegate.isValidString(productModel.created)) {
                    try {
                        Date modifiedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(productModel.created);
                        txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + new SimpleDateFormat("dd'" + AppDelegate.getDayOfMonthSuffix(new SimpleDateFormat("dd").format(modifiedDate)) + "' MMMM yyyy.").format(modifiedDate) + "  <b>Time : </b>" + new SimpleDateFormat("hh:mmaa.").format(modifiedDate)));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                        txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + productModel.created + ""));
                    }
                } else {
                    txt_c_date.setVisibility(View.GONE);
                }

                if (AppDelegate.isValidString(productModel.pc_title)) {
                    txt_c_category.setVisibility(View.VISIBLE);
                    txt_c_category.setText(Html.fromHtml("<b>Category : </b>" + productModel.pc_title + ""));
                } else {
                    txt_c_category.setVisibility(View.GONE);
                }

                txt_c_header.setText(productModel.title);
                txt_c_description_tittle.setText(Html.fromHtml("<b>Description : </b>"));
                txt_c_description_tittle.setVisibility(View.GONE);
                txt_c_description.setText(Html.fromHtml("<b>Description : </b>" + productModel.description + ""));

                txt_c_viewer.setText(AppDelegate.isValidString(productModel.total_product_views) ? productModel.total_product_views : "0");

                if (productModel.item_condition.equalsIgnoreCase("1")) {
                    txt_c_condition.setText(Html.fromHtml("<b>Condition :</b>" + " New"));
                } else if (productModel.item_condition.equalsIgnoreCase("2")) {
                    txt_c_condition.setText(Html.fromHtml("<b>Condition :</b>" + " Almost New"));
                } else if (productModel.item_condition.equalsIgnoreCase("3")) {
                    txt_c_condition.setText(Html.fromHtml("<b>Condition :</b>" + " Used"));
                }

                arrayString.clear();
                if (AppDelegate.isValidString(productModel.image_4_thumb)) {
                    image_count = 4;
//                    txt_c_image.setText("4");
                    arrayString.add(productModel.image_1_thumb);
                    arrayString.add(productModel.image_2_thumb);
                    arrayString.add(productModel.image_3_thumb);
                    arrayString.add(productModel.image_4_thumb);
                } else if (AppDelegate.isValidString(productModel.image_3_thumb)) {
                    image_count = 3;
//                    txt_c_image.setText("3");
                    arrayString.add(productModel.image_1_thumb);
                    arrayString.add(productModel.image_2_thumb);
                    arrayString.add(productModel.image_3_thumb);
                } else if (AppDelegate.isValidString(productModel.image_2_thumb)) {
                    image_count = 2;
//                    txt_c_image.setText("2");
                    arrayString.add(productModel.image_1_thumb);
                    arrayString.add(productModel.image_2_thumb);
                } else if (AppDelegate.isValidString(productModel.image_1_thumb)) {
                    image_count = 1;
//                    txt_c_image.setText("1");
                    arrayString.add(productModel.image_1_thumb);
                } else {
                    image_count = 0;
//                    txt_c_image.setText("0");
//                arrayString.add("");
                }

                AppDelegate.LogE("arrayString size => " + arrayString.size());
                mPagerAdapter = new ProductPagerAdapter(getActivity(), arrayString, this);
                view_pager.setAdapter(mPagerAdapter);
                view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        if (position > -1) {
                            txt_c_image.setText((position + 1) + "/" + image_count);
                        } else {
                            txt_c_image.setText("0/" + image_count);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {

                    }
                });
                if (image_count > 0) {
                    txt_c_image.setText("1/" + image_count);
                } else {
                    txt_c_image.setText("0/0");
                }

                if (mPagerAdapter.getCount() > selected_page) {
                    view_pager.setCurrentItem(selected_page);
                }

                img_loading.setVisibility(View.VISIBLE);
                AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                frameAnimation.setCallback(img_loading);
                frameAnimation.setVisible(true, true);
                frameAnimation.start();

                if (AppDelegate.isValidString(productModel.image_1_thumb))
                    Picasso.with(getActivity()).load(productModel.image_1_thumb).into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            OriginalPhoto = bitmap;
                            imageLoaded = true;
                        }

                        @Override
                        public void onBitmapFailed(Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {

                        }
                    });

                if (AppDelegate.isValidString(productModel.user_image)) {
                    Picasso.with(getActivity()).load(productModel.user_image).into(cimg_user, new Callback() {
                        @Override
                        public void onSuccess() {
                            img_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            img_loading.setVisibility(View.GONE);
                        }
                    });
                } else {
                    if (AppDelegate.isValidString(productModel.user_gender) && productModel.user_gender.equalsIgnoreCase(Tags.FEMALE)) {
                        cimg_user.setImageResource(R.drawable.user_female_1);
                    } else {
                        cimg_user.setImageResource(R.drawable.user_female_2);
                    }
                    img_loading.setVisibility(View.GONE);
                }
                txt_c_name.setText(productModel.user_first_name);
                txt_c_address.setText(AppDelegate.getFormatedAddress(productModel.user_institute_name, productModel.user_department_name));
                txt_c_price.setText("N" + productModel.price);
                txt_c_price_my_product.setText("N" + productModel.price);

                txt_c_like_count.setText("" + productModel.total_product_likes);
                txt_c_comment_count.setText("" + productModel.total_comments);

                img_like.setSelected(productModel.product_like_status == 1);
                if (productModel.sold_status.equalsIgnoreCase("1")) {
                    ll_c_actions.setVisibility(View.GONE);
                } else {
                    ll_c_actions.setVisibility(View.VISIBLE);
                    if (productModel.user_id.equalsIgnoreCase(dataModel.userId)) {
                        rl_map.setVisibility(View.GONE);

                        txt_c_item_sold.setVisibility(View.VISIBLE);
                        txt_c_chat.setVisibility(View.GONE);
                        txt_c_make_offer.setVisibility(View.GONE);
                    } else {
                        rl_map.setVisibility(View.VISIBLE);
                        ll_c_flag.setOnClickListener(this);
                        ll_c_flag.setVisibility(View.VISIBLE);

                        txt_c_item_sold.setVisibility(View.GONE);
                        txt_c_chat.setVisibility(View.VISIBLE);
                        txt_c_make_offer.setVisibility(View.VISIBLE);
                    }
                }
                if (productModel.logged_user_view_status.equalsIgnoreCase("0")) {
                    executeItemViewApi();
                }

                if (arrayOptions.size() == 0) {
                    arrayOptions.add("Delete Product");
                    AppDelegate.LogT("productModel.sold_status => " + productModel.sold_status);
                    if (productModel != null && AppDelegate.isValidString(productModel.sold_status) && productModel.sold_status.equalsIgnoreCase("0"))
                        arrayOptions.add("Edit Product");

                }
                list_options.setAdapter(adapter);

            } else {
                AppDelegate.showAlert(getActivity(), "Data not refreshed");
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private File capturedFile;
    private Bitmap OriginalPhoto;
    private boolean imageLoaded = false;

    public void writeImageFile(Bitmap OriginalPhoto) {
        FileOutputStream fOut = null;
        try {
            if (capturedFile == null)
                capturedFile = new File(getNewFile());
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            try {
                File capturedFile_3 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                        + ".png");
                if (capturedFile_3.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile_3.getAbsolutePath());
                    return capturedFile_3.getAbsolutePath();
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    private void showDialogDelete() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
        dialog.setContentView(R.layout.dialog_delete_item);
        dialog.findViewById(R.id.btn_no).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btn_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                executeDeleteProductApi();
            }
        });
        dialog.show();
    }

    private void initView(View view) {
        txt_c_header = ((TextView) view.findViewById(R.id.txt_c_header));
        txt_c_header.setVisibility(View.VISIBLE);

        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        if (productModel != null && AppDelegate.isValidString(productModel.user_id) && productModel.user_id.equalsIgnoreCase(dataModel.userId)) {
            ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.view_more);
            view.findViewById(R.id.img_c_right).setVisibility(View.VISIBLE);
            view.findViewById(R.id.img_c_right).setOnClickListener(this);
        } else {
//            ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
            view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        }

        txt_c_date = (TextView) view.findViewById(R.id.txt_c_date);
        txt_c_category = (TextView) view.findViewById(R.id.txt_c_category);
        txt_c_description_tittle = (TextView) view.findViewById(R.id.txt_c_description_tittle);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        txt_c_image = (TextView) view.findViewById(R.id.txt_c_image);
        txt_c_viewer = (TextView) view.findViewById(R.id.txt_c_viewer);
        txt_c_condition = (TextView) view.findViewById(R.id.txt_c_condition);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_price = (TextView) view.findViewById(R.id.txt_c_price);
        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);
        txt_c_price_my_product = (TextView) view.findViewById(R.id.txt_c_price_my_product);
        txt_c_item_sold = (TextView) view.findViewById(R.id.txt_c_item_sold);
        txt_c_chat = (TextView) view.findViewById(R.id.txt_c_chat);
        txt_c_make_offer = (TextView) view.findViewById(R.id.txt_c_make_offer);
        txt_c_like_count = (TextView) view.findViewById(R.id.txt_c_like_count);
        txt_c_comment_count = (TextView) view.findViewById(R.id.txt_c_comment_count);

        ll_c_actions = (LinearLayout) view.findViewById(R.id.ll_c_actions);

//        rl_profile_img = (RelativeLayout) view.findViewById(R.id.rl_profile_img);
        ll_c_flag = (LinearLayout) view.findViewById(R.id.ll_c_flag);
        ll_c_flag.setVisibility(View.INVISIBLE);
        rl_map = (RelativeLayout) view.findViewById(R.id.rl_map);

        img_product = (ImageView) view.findViewById(R.id.img_product);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);
        img_like = (ImageView) view.findViewById(R.id.img_like);
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);

        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
        mapview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return true;
            }
        });

        view.findViewById(R.id.view_map).setOnClickListener(this);

        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
        mPagerAdapter = new ProductPagerAdapter(getActivity(), arrayString, this);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                selected_page = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        view.findViewById(R.id.mapview).setOnClickListener(this);
        view.findViewById(R.id.txt_c_chat).setOnClickListener(this);
        view.findViewById(R.id.txt_c_make_offer).setOnClickListener(this);
        view.findViewById(R.id.txt_c_item_sold).setOnClickListener(this);

        sl_more = (SlidingLayer) view.findViewById(R.id.sl_more);
        sl_more.setStickTo(SlidingLayer.STICK_TO_RIGHT);
        sl_more.setLayerTransformer(new AlphaTransformer());
        sl_more.setShadowSize(0);
        sl_more.setShadowDrawable(null);

        adapter = new OptionsListAdapter(getActivity(), arrayOptions, this);
        list_options = (ListView) view.findViewById(R.id.list_options);
        list_options.setAdapter(adapter);

        view.findViewById(R.id.ll_c_like).setOnClickListener(this);
        view.findViewById(R.id.ll_c_comment).setOnClickListener(this);
        view.findViewById(R.id.img_c_fb).setOnClickListener(this);
        view.findViewById(R.id.img_c_twitter).setOnClickListener(this);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showMap();
    }

    private void showMap() {
        if (map_business == null) {
            AppDelegate.LogE("map_business null at showMap");
            return;
        }

        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        if (productModel != null && AppDelegate.isValidString(productModel.latitude) && AppDelegate.isValidString(productModel.longitude)) {
            AppDelegate.LogT("location showing => " + productModel.latitude + "," + productModel.longitude);
            map_business.addMarker(AppDelegate.getMarkerOptions(getActivity(), new LatLng(Double.parseDouble(productModel.latitude), Double.parseDouble(productModel.longitude)), R.drawable.map_pin));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(productModel.latitude), Double.parseDouble(productModel.longitude)))
                    .zoom(14).build();
            //Zoom in and animate the camera.
            map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private Bundle bundle;
    private Fragment fragment;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;

            case R.id.img_c_right:
                if (productModel.user_id.equalsIgnoreCase(dataModel.userId)) {
                    if (sl_more.isOpened()) {
                        sl_more.closeLayer(true);
                    } else {
                        sl_more.openLayer(true);
                    }
                } else {
                    bundle = new Bundle();
                    bundle.putParcelable(Tags.product, productModel);
                    fragment = new ReasonFragment();
                    fragment.setArguments(bundle);
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                }
                break;

            case R.id.txt_c_chat:
                bundle = new Bundle();
                bundle.putParcelable(Tags.product, productModel);
                bundle.putInt(Tags.PAGE, ChatFragment.FROM_PRODUCT);
                fragment = new ChatFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;

            case R.id.txt_c_item_sold:
                showAlertDialog();
                break;

            case R.id.txt_c_make_offer:
                bundle = new Bundle();
                bundle.putParcelable(Tags.product, productModel);
                fragment = new MakeOfferFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;

            case R.id.cimg_user:
                if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    UserDataModel sellersDataModel = new UserDataModel();
                    sellersDataModel.userId = productModel.user_id;
                    sellersDataModel.first_name = productModel.user_first_name;
                    sellersDataModel.last_name = productModel.user_last_name;
                    sellersDataModel.image = productModel.user_image;
                    sellersDataModel.is_view = productModel.user_is_view;
                    sellersDataModel.role = productModel.user_role;
                    sellersDataModel.social_id = productModel.user_social_id;
                    sellersDataModel.status = productModel.user_status;
                    sellersDataModel.login_type = productModel.user_login_type;
                    sellersDataModel.dob = productModel.user_dob;

                    sellersDataModel.institution_state_id = productModel.user_institution_state_id;
                    sellersDataModel.institution_id = productModel.user_institution_id;
                    sellersDataModel.institute_name = productModel.user_institute_name;
                    sellersDataModel.department_name = productModel.user_department_name;

                    sellersDataModel.following_count = productModel.user_following_count;
                    sellersDataModel.followers_count = productModel.user_followers_count;
                    sellersDataModel.total_product = productModel.user_total_product;
                    sellersDataModel.follow_status = productModel.user_follow_status;

                    Bundle bundle = new Bundle();
                    bundle.putParcelable(Tags.user, sellersDataModel);
                    Fragment fragment = new SellersProfileFragment();
                    if (sellersDataModel.userId.equalsIgnoreCase(dataModel.userId)) {
                        fragment = new MyProfileFragment();
                    } else {
                        fragment = new SellersProfileFragment();
                    }
                    fragment.setArguments(bundle);
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                } else {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                }
                break;

            case R.id.view_map:
                if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                } else {
                    Intent intent = new Intent(getActivity(), MapDetailActivity.class);
                    intent.putExtra(Tags.LAT, productModel.user_institute_lat);
                    intent.putExtra(Tags.LNG, productModel.user_institute_lng);
                    intent.putExtra(Tags.name, productModel.user_institute_name);
                    startActivity(intent);
                }
                break;

            case R.id.ll_c_like:
                callProductLikesAsync(productModel.id, (productModel.product_like_status == 1 ? 0 : 1) + "");
                break;

            case R.id.ll_c_comment:
                bundle = new Bundle();
                bundle.putParcelable(Tags.product, productModel);
                fragment = new CommentListingFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;

            case R.id.ll_c_flag:
                AppDelegate.showAlert(getActivity(), "", getResources().getString(R.string.we_will_look_into_your_report) + "\n\n" + getResources().getString(R.string.are_you_sure_report), getResources().getString(R.string.Yes), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bundle = new Bundle();
                        bundle.putParcelable(Tags.product, productModel);
                        fragment = new ReasonFragment();
                        fragment.setArguments(bundle);
                        AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                    }
                }, getResources().getString(R.string.Cancel), null);
                break;

            case R.id.img_c_fb:
                openFacebook(productModel);
                break;

            case R.id.img_c_twitter:
                loginButton.performClick();
                break;
        }
    }

    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;


    public void openFacebook(final ProductModel productModel) {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(productModel);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(productModel);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(productModel);
                                }
                            }
                        }
                    }
                });
    }

    private void shareFacebook(ProductModel productModel) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(productModel.title)
//                    .setImageUrl(imageURI)
                    .setImageUrl(Uri.parse(productModel.image_1))
                    .setContentDescription(productModel.description)
                    .setContentUrl(Uri.parse(productModel.image_1))
                    .build();
            shareDialog.show(linkContent);
        }
    }

    private void callProductLikesAsync(String product_id, String status) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.PRODUCT_LIKES,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void showAlertDialog() {
        try {
            AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(false);
            mAlert.setTitle("STUX");
            mAlert.setMessage("Are you sure you want to mark this product as a sold out.");
            mAlert.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            executeItemSoldApi();
                            dialog.dismiss();
                        }
                    });

            mAlert.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private int item_position = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image_1, productModel.image_1);
                bundle.putString(Tags.image_2, productModel.image_2);
                bundle.putString(Tags.image_3, productModel.image_3);
                bundle.putString(Tags.image_4, productModel.image_4);
                bundle.putInt(Tags.POSITION, position);
                bundle.putInt(Tags.count, 4);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            }
        } else if (name.equalsIgnoreCase(Tags.Options)) {
            sl_more.closeLayer(true);
            switch (position) {
                case 0:
                    showDialogDelete();
                    break;
                case 1:
                    bundle = new Bundle();
                    bundle.putParcelable(Tags.product, productModel);
                    bundle.putInt(Tags.FROM, fromPage);
                    fragment = new SellItemFragment();
                    fragment.setArguments(bundle);
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                    break;
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.ITEM_VIEW)) {
//            mHandler.sendEmptyMessage(11);
            parseItemViewResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ITEM_SOLD)) {
            mHandler.sendEmptyMessage(11);
            parseItemSoldResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.ITEM_DELETE)) {
            mHandler.sendEmptyMessage(11);
            parseDeleteResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.PRODUCT_DETAIL)) {
            mHandler.sendEmptyMessage(11);
            parseProductDetail(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.PRODUCT_LIKES)) {
            mHandler.sendEmptyMessage(11);
            parseLikesResponse(result);
        }
    }

    private void parseLikesResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                if (productModel.product_like_status == 0) {
                    productModel.product_like_status = 1;
                    productModel.total_product_likes += 1;
                } else {
                    productModel.product_like_status = 0;
                    productModel.total_product_likes -= 1;
                    if (productModel.total_product_likes < 0) {
                        productModel.total_product_likes = 0;
                    }
                }
                setValues();
                updateProduct(productModel);
            }
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseProductDetail(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                productModel = PushNotificationService.getProductModel(jsonObject.getJSONArray(Tags.response).getJSONObject(0).toString());
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "No record found, or may be some server exception occur.");
            getFragmentManager().popBackStack();
        }
    }

    private void parseDeleteResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                removeItemFromList();
                AppDelegate.showFragmentAnimationOppose(getActivity().getSupportFragmentManager(), new HomeFragment());
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void removeItemFromList() {
        for (int i = 0; i < HomeFragment.productArray.size(); i++) {
            if (HomeFragment.productArray.get(i) != null && AppDelegate.isValidString(HomeFragment.productArray.get(i).id) && HomeFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                HomeFragment.productArray.remove(i);
                break;
            }
        }
        for (int i = 0; i < MyProductListFragment.productArray.size(); i++) {
            if (MyProductListFragment.productArray.get(i) != null && AppDelegate.isValidString(MyProductListFragment.productArray.get(i).id) && MyProductListFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                MyProductListFragment.productArray.remove(i);
                break;
            }
        }
        for (int i = 0; i < MyProfileFragment.productArray.size(); i++) {
            if (MyProfileFragment.productArray.get(i) != null && AppDelegate.isValidString(MyProfileFragment.productArray.get(i).id) && MyProfileFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                MyProfileFragment.productArray.remove(i);
                break;
            }
        }
        for (int i = 0; i < RefineProductFragment.productArray.size(); i++) {
            if (RefineProductFragment.productArray.get(i) != null && AppDelegate.isValidString(RefineProductFragment.productArray.get(i).id) && RefineProductFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                RefineProductFragment.productArray.remove(i);
                break;
            }
        }
    }

    private void parseItemSoldResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                removeItemFromList();
                AppDelegate.showFragmentAnimationOppose(getActivity().getSupportFragmentManager(), new HomeFragment());
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void parseItemViewResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
            } else {
                productModel.total_product_views = (AppDelegate.getIntValue(productModel.total_product_views) + 1) + "";
                productModel.logged_user_view_status = "1";
                setValues();
                updateProduct(productModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void updateProduct(ProductModel productModel) {
        String total_product_views, logged_user_view_status;
        int product_like_status = 0, total_product_likes = 0;

        total_product_views = this.productModel.total_product_views;
        logged_user_view_status = this.productModel.logged_user_view_status;
        product_like_status = productModel.product_like_status;
        total_product_likes = productModel.total_product_likes;

        this.productModel = productModel;
        AppDelegate.LogT("this.productModel => " + this.productModel.image_1 + ", " + this.productModel.image_2 + ", " + this.productModel.image_3 + ", " + this.productModel.image_4);
        this.productModel.total_product_views = total_product_views;
        this.productModel.logged_user_view_status = logged_user_view_status;
        this.productModel.product_like_status = product_like_status;
        this.productModel.total_product_likes = total_product_likes;

        ProductDetailFragment.dataChanged = true;
        for (int i = 0; i < HomeFragment.productArray.size(); i++) {
            if (HomeFragment.productArray.get(i).id.equalsIgnoreCase(this.productModel.id)) {
                HomeFragment.productArray.remove(i);
                HomeFragment.productArray.add(i, this.productModel);
                AppDelegate.LogT("added at HomeFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < MyProfileFragment.productArray.size(); i++) {
            if (MyProfileFragment.productArray.get(i).id.equalsIgnoreCase(this.productModel.id)) {
                MyProfileFragment.productArray.remove(i);
                MyProfileFragment.productArray.add(i, this.productModel);
                AppDelegate.LogT("added at MyProfileFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < MyProductListFragment.productArray.size(); i++) {
            if (MyProductListFragment.productArray.get(i).id.equalsIgnoreCase(this.productModel.id)) {
                MyProductListFragment.productArray.remove(i);
                MyProductListFragment.productArray.add(i, this.productModel);
                AppDelegate.LogT("added at MyProductListFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < RefineProductFragment.productArray.size(); i++) {
            if (RefineProductFragment.productArray.get(i).id.equalsIgnoreCase(this.productModel.id)) {
                RefineProductFragment.productArray.remove(i);
                RefineProductFragment.productArray.add(i, this.productModel);
                AppDelegate.LogT("added at RefineProductFragment at position => " + i);
                break;
            }
        }
    }
}
