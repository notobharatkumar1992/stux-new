package com.stux.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/24/2016.
 */
public class ResetPasswordFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private TextView txt_c_header;
    private ImageView img_c_left, img_c_password_view, img_c_confirm_password_view;
    private EditText edit_code, edit_password, edit_confirm_password;

    public Prefs prefs;
    public Handler mHandler;
    public AppDelegate mInstance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reset_password, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mInstance = AppDelegate.getInstance(getActivity());
        prefs = new Prefs(getActivity());
        initView(view);
        setHandler();
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.hideKeyBoard(getActivity());
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {

                }
            }
        };
    }

    private void initView(View view) {
        txt_c_header = (TextView) view.findViewById(R.id.txt_c_header);
        txt_c_header.setText(getString(R.string.Reset_Password));
        img_c_left = (ImageView) view.findViewById(R.id.img_c_left);
        img_c_left.setImageDrawable(getResources().getDrawable(R.drawable.back));
        img_c_left.setOnClickListener(this);

        img_c_password_view = (ImageView) view.findViewById(R.id.img_c_password_view);
        img_c_password_view.setOnClickListener(this);
        img_c_confirm_password_view = (ImageView) view.findViewById(R.id.img_c_confirm_password_view);
        img_c_confirm_password_view.setOnClickListener(this);

        edit_code = (EditText) view.findViewById(R.id.edit_code);
        edit_code.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        edit_password = (EditText) view.findViewById(R.id.edit_password);
        edit_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        edit_confirm_password = (EditText) view.findViewById(R.id.edit_confirm_password);
        edit_confirm_password.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));

        view.findViewById(R.id.txt_c_register).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;
            case R.id.txt_c_register:
                if (AppDelegate.haveNetworkConnection(getActivity(), false))
                    execute_resetPasswordAsync();
                else
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignInFragment());
                break;

            case R.id.img_c_password_view:
                img_c_password_view.setSelected(!img_c_password_view.isSelected());
                if (img_c_password_view.isSelected()) {
                    edit_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    edit_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                edit_password.setSelection(edit_password.length());
                break;

            case R.id.img_c_confirm_password_view:
                img_c_confirm_password_view.setSelected(!img_c_confirm_password_view.isSelected());
                if (img_c_confirm_password_view.isSelected()) {
                    edit_confirm_password.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                } else {
                    edit_confirm_password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
                edit_confirm_password.setSelection(edit_confirm_password.length());
                break;
        }
    }

    private void execute_resetPasswordAsync() {
        if (edit_code.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please Enter Your Activation Code.");
        } else if (edit_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please Enter Your Password.");
        } else if (edit_confirm_password.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please Enter Your Confirm Password.");
        } else if (!edit_password.getText().toString().equals(edit_confirm_password.getText().toString())) {
            AppDelegate.showAlert(getActivity(), "Password And Confirm Password Must Be same.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            mInstance.setPostParamsSecond(mPostArrayList, Tags.activation_code, edit_code.getText().toString());
            mInstance.setPostParamsSecond(mPostArrayList, Tags.password, edit_password.getText().toString());
            mInstance.setPostParamsSecond(mPostArrayList, Tags.confirm_password, edit_confirm_password.getText().toString());
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    ResetPasswordFragment.this, ServerRequestConstants.RESET_PASSWORD,
                    mPostArrayList, ResetPasswordFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.RESET_PASSWORD)) {
            parseResetPasswordResult(result);
        }
    }

    private void parseResetPasswordResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignInFragment());
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

}
