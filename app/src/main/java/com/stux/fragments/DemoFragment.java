package com.stux.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stux.R;

/**
 * Created by Bharat on 07/01/2016.
 */
public class DemoFragment extends Fragment {

//    private SuperSwipeRefreshLayout swipe_refresh;
//    private GridView gridViewCampusList;
//
//    private ArrayList<String> stringArray = new ArrayList<>();
//
//    // Footer View
//    private ProgressBar footerProgressBar;
//    private android.widget.TextView footerTextView;
//    private ImageView footerImageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.demo_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        swipe_refresh = (SuperSwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
//        swipe_refresh.setFooterView(createFooterView());
//        swipe_refresh.setOnPullRefreshListener(new SuperSwipeRefreshLayout.OnPullRefreshListener() {
//            @Override
//            public void onRefresh() {
//                swipe_refresh.setRefreshing(false);
//            }
//
//            @Override
//            public void onPullDistance(int distance) {
//
//            }
//
//            @Override
//            public void onPullEnable(boolean enable) {
//
//            }
//        });
//        swipe_refresh.setOnPushLoadMoreListener(new SuperSwipeRefreshLayout.OnPushLoadMoreListener() {
//            @Override
//            public void onLoadMore() {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        swipe_refresh.setLoadMore(false);
//                    }
//                }, 1000);
//            }
//
//            @Override
//            public void onPushDistance(int distance) {
//
//            }
//
//            @Override
//            public void onPushEnable(boolean enable) {
//
//            }
//        });
//
//        for (int i = 0; i < 50; i++) {
//            stringArray.add("item " + i);
//        }
//
//        gridViewCampusList = (GridView) view.findViewById(R.id.gridViewCampusList);
//        gridViewCampusList.setAdapter(new SpinnerArrayStringAdapter(getActivity(), stringArray));
//        setListViewHeightBasedOnChildren(gridViewCampusList, gridViewCampusList.getAdapter());
    }


//    public void setListViewHeightBasedOnChildren(GridView listView, ListAdapter gridAdapter) {
//        try {
//            if (gridAdapter == null) {
//                // pre-condition
//                AppDelegate.LogE("Adapter is null");
//                ViewGroup.LayoutParams params = listView.getLayoutParams();
//                params.height = 80;
//                listView.setLayoutParams(params);
//                listView.requestLayout();
//                return;
//            }
//            int totalHeight = 0;
//
//            float itemCount = gridAdapter.getCount();
//            float value = itemCount / 2;
//            AppDelegate.LogT("height = after device = " + value);
//
//            String string_value = value + "";
//            string_value = string_value.substring(string_value.lastIndexOf(".") + 1, string_value.length());
//            int intValue = (int) value;
//            if (Integer.parseInt(string_value) > 0) {
//                intValue += 1;
//            }
//            AppDelegate.LogT("intValue = " + intValue);
//            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
//                    View.MeasureSpec.AT_MOST);
//            for (int i = 0; i < intValue; i++) {
//                View listItem = gridAdapter.getView(i, null, listView);
//                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//                totalHeight += AppDelegate.dpToPix(getActivity(), 150);
//            }
//            AppDelegate.LogT("totalHeight = " + totalHeight);
//            ViewGroup.LayoutParams params = listView.getLayoutParams();
//            params.height = totalHeight + (listView.getVerticalSpacing() * intValue);
//            listView.setLayoutParams(params);
//            listView.requestLayout();
//
//            swipe_refresh.setLayoutParams(params);
//            swipe_refresh.requestLayout();
//        } catch (Exception e) {
//            AppDelegate.LogE(e);
//        }
//    }
//
//    private View createFooterView() {
//        View footerView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_footer, null);
//        footerProgressBar = (ProgressBar) footerView
//                .findViewById(R.id.footer_pb_view);
//        footerImageView = (ImageView) footerView
//                .findViewById(R.id.footer_image_view);
//        footerTextView = (android.widget.TextView) footerView
//                .findViewById(R.id.footer_text_view);
//        footerProgressBar.setVisibility(View.GONE);
//        footerImageView.setVisibility(View.VISIBLE);
////        footerImageView.setImageResource(R.drawable.down_arrow);
//        footerTextView.setText("Loading...");
//        return footerView;
//    }


}
