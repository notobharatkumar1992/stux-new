package com.stux.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.widget.AppInviteDialog;
import com.stux.Adapters.MyPagerLoopAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.MyViewPager;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.Constants;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.Fb_detail_GetSet;
import com.stux.parser.Fb_details;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by NOTO on 5/24/2016.
 */
public class LoginTutorialFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private ImageView img_logo_01, img_logo_02, img_01, img_02, img_03, img_04;
    private TextView txt_c_tut_1, txt_c_tut_2;
    private MyViewPager view_pager;

    private MyPagerLoopAdapter mPagerAdapter;

    public static CallbackManager callbackManager;
    private String fb_LoginToken;
    private boolean isCalledOnce = false;

    private Handler mHandler;
    private Prefs prefs;

    private CheckBox cb_tnc;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_tutorial_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Constants.isMapScreen = true;
        prefs = new Prefs(getActivity());
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        callbackManager = null;
        mHandler.removeCallbacks(mRunnable);
    }

    public int pageCount = 4;

    private void initView(View view) {
        img_logo_01 = (ImageView) view.findViewById(R.id.img_logo_01);
        img_logo_02 = (ImageView) view.findViewById(R.id.img_logo_02);

        img_01 = (ImageView) view.findViewById(R.id.img_01);
        img_01.setOnClickListener(this);
        img_02 = (ImageView) view.findViewById(R.id.img_02);
        img_02.setOnClickListener(this);
        img_03 = (ImageView) view.findViewById(R.id.img_03);
        img_03.setOnClickListener(this);
        img_04 = (ImageView) view.findViewById(R.id.img_04);
        img_04.setOnClickListener(this);

        txt_c_tut_1 = (TextView) view.findViewById(R.id.txt_c_tut_1);
        txt_c_tut_2 = (TextView) view.findViewById(R.id.txt_c_tut_2);
        switchPage(0);
        view_pager = (MyViewPager) view.findViewById(R.id.view_pager);
        mPagerAdapter = new MyPagerLoopAdapter(pageCount);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchPage(position % pageCount);
                timeOutLoop();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        view.findViewById(R.id.txt_c_already).setOnClickListener(this);
        view.findViewById(R.id.rl_c_sign_in_fb).setOnClickListener(this);
        view.findViewById(R.id.rl_c_find_my_institution).setOnClickListener(this);

        view.findViewById(R.id.txt_c_tnc).setOnClickListener(this);
        cb_tnc = (CheckBox) view.findViewById(R.id.cb_tnc);
        timeOutLoop();
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    int value = view_pager.getCurrentItem();
                    if (value == 4 * 1000 - 1) {
                        value = 0;
                    } else {
                        value++;
                    }
                    view_pager.setCurrentItem(value, true);
                    if (isAdded()) {
                        mHandler.postDelayed(this, 3000);
                    } else {
                        mHandler.removeCallbacks(this);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void switchPage(int position) {
        img_logo_01.setVisibility(View.GONE);
        txt_c_tut_1.setVisibility(View.GONE);
        img_logo_02.setVisibility(View.GONE);
        txt_c_tut_2.setVisibility(View.GONE);

        img_01.setImageResource(R.drawable.white_radius_square);
        img_02.setImageResource(R.drawable.white_radius_square);
        img_03.setImageResource(R.drawable.white_radius_square);
        img_04.setImageResource(R.drawable.white_radius_square);
        switch (position) {
            case 0:
                img_logo_01.setVisibility(View.INVISIBLE);
                txt_c_tut_1.setTextColor(getResources().getColor(R.color.blue_color));
                txt_c_tut_1.setVisibility(View.VISIBLE);
                txt_c_tut_1.setText(getString(R.string.tutorial_01));
                img_01.setImageResource(R.drawable.yellow_radius_square);
                break;

            case 1:
                img_logo_02.setVisibility(View.INVISIBLE);
                txt_c_tut_1.setTextColor(Color.WHITE);
                txt_c_tut_1.setVisibility(View.VISIBLE);
                txt_c_tut_1.setText(getString(R.string.tutorial_02));
                img_02.setImageResource(R.drawable.yellow_radius_square);
                break;

            case 2:
                img_logo_02.setVisibility(View.INVISIBLE);
                txt_c_tut_2.setVisibility(View.VISIBLE);
                txt_c_tut_2.setText(getString(R.string.tutorial_03));
                img_03.setImageResource(R.drawable.yellow_radius_square);
                break;

            case 3:
                img_logo_02.setVisibility(View.INVISIBLE);
                txt_c_tut_1.setTextColor(Color.WHITE);
                txt_c_tut_1.setVisibility(View.VISIBLE);
                txt_c_tut_1.setText(getString(R.string.tutorial_04));
                img_04.setImageResource(R.drawable.yellow_radius_square);
                break;
        }
    }

    private Fb_detail_GetSet fbUserData;

    public void openFacebook() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        AppDelegate.showProgressDialog(getActivity());
                        fb_LoginToken = loginResult.getAccessToken().toString();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        // Application code
                                        AppDelegate.LogFB("Graph response" + response.toString());
                                        fbUserData = new Fb_detail_GetSet();
                                        fbUserData = new Fb_details().getFacebookDetail(response.getJSONObject().toString());

                                        AppDelegate.hideProgressDialog(getActivity());

                                        callAsyncFacebookVerify(fbUserData);


                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "first_name,last_name,email,id,name,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook();
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook();
                                }
                            }
                        }
                    }
                });
    }

    private void InviteFbFriends() {
        String appLinkUrl, previewImageUrl;
        appLinkUrl = "app url(create it from facebook)"; //your applink url
        previewImageUrl = "image url";//your image url
        if (AppInviteDialog.canShow()) {
            AppInviteContent content = new AppInviteContent.Builder()
                    .setApplinkUrl(appLinkUrl)
                    .setPreviewImageUrl(previewImageUrl)
                    .build();
            AppInviteDialog.show(this, content);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_c_sign_in_fb:
                if (cb_tnc.isChecked()) {
                    if (AppDelegate.haveNetworkConnection(getActivity(), false))
                        openFacebook();
                    else
                        AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                } else {
                    AppDelegate.showAlert(getActivity(), getString(R.string.alert_terms_conditions));
                }
                break;
            case R.id.txt_c_already:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignInFragment());
                break;

            case R.id.rl_c_find_my_institution:
                if (cb_tnc.isChecked()) {
                    new Prefs(getActivity()).clearTempPrefs();
                    if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                        AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignUpInstitutionFragment());
                    } else {
                        AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
                    }
                } else {
                    AppDelegate.showAlert(getActivity(), getString(R.string.alert_terms_conditions));
                }
                break;
            case R.id.img_01:
                view_pager.setCurrentItem(0);
                break;
            case R.id.img_02:
                view_pager.setCurrentItem(1);
                break;
            case R.id.img_03:
                view_pager.setCurrentItem(2);
                break;
            case R.id.img_04:
                view_pager.setCurrentItem(3);
                break;

            case R.id.txt_c_tnc:
                Bundle bundle = new Bundle();
                bundle.putInt(Tags.FROM, 0);
                AppDelegate.showFragmentAnimation(getActivity(), new WebViewDetailFragment(), bundle, null);
                break;
        }
    }

    public void onFacebookResult(String name, Bundle bundle) {
        AppDelegate.LogT("Login onFacebookResult called");
        if (name.equalsIgnoreCase(Tags.FACEBOOK)) {
            if (bundle != null) {
//                FragTransaction.showFragmentAnimationWithBackFlip(getActivity(), new Verify_profile_details(), R.id.container, bundle, null, 200);
            } else {
                AppDelegate.LogE("facebook bundle is null at " + name);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        AppDelegate.LogT("Login onActivityResult called");
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void callAsyncFacebookVerify(Fb_detail_GetSet fbUserData) {
        if (fbUserData == null) {
            AppDelegate.showAlert(getActivity(), "Facebook data is incorrect, please try after some time.");
        } else if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.social_id, fbUserData.getId());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.login_type, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.LOGIN,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.LOGIN)) {
            parseLoginFacebookResult(result);
        }
    }

    private void parseLoginFacebookResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
            } else if (jsonObject.getString(Tags.status).equalsIgnoreCase("2")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {

                if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null && jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null) {
                    AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                } else {
                    prefs.setTempFacebookData(fbUserData);
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SignUpInstitutionFragment());
                }

            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 0);
            AppDelegate.LogE(e);
        }
    }
}
