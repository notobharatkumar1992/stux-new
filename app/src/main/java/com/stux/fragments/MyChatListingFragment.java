package com.stux.fragments;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.MyChatAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class MyChatListingFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener {

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    public static int eventCounter = 1, eventTotalPage = -1;
    private boolean eventAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private MyChatAdapter rcAdapter;

    private ArrayList<com.stux.Models.Message> messageArray = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_event_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        messageArray.clear();
        eventCounter = 1;
        eventTotalPage = -1;
        if (messageArray.size() == 0) {
            eventAsyncExcecuting = true;
            callCampusListAsync();
        } else
            mHandler.sendEmptyMessage(2);
    }

    //    1=active,2=waiting for approval,3=reject from admin,4=expired
    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.USER_CHAT_LIST,
                    mPostArrayList, null);
            if (eventCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    txt_c_no_list.setVisibility(messageArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No chat found.");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("My Chat");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });

        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new MyChatAdapter(getActivity(), messageArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (eventTotalPage != 0 && !eventAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callCampusListAsync();
                                 eventAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + eventTotalPage + ", " + eventAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    public void showChatOptionsAlert(final int position) {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Open", "  Delete", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.FROM, Tags.CHAT);
                        bundle.putInt(Tags.PAGE, ChatFragment.FROM_CHAT);
                        bundle.putParcelable(Tags.message, messageArray.get(position));
                        Fragment fragment = new ChatFragment();
                        fragment.setArguments(bundle);
                        AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                        break;
                    case 1:
                        dialog.dismiss();
                        callDeleteChatAsync(position);
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.USER_CHAT_LIST)) {
            eventAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.DELETE_CHAT)) {
            parseDeleteChatResult(result);
        }
    }

    private void parseDeleteChatResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                messageArray.remove(selected_item);
                mHandler.sendEmptyMessage(2);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.LogT("messageArray => " + messageArray.size() + ", " + jsonObject.getString(Tags.status));
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.response))) {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    if (JSONParser.getString(object, Tags.delete).equalsIgnoreCase("1")) {
                    } else {
                        com.stux.Models.Message message = new com.stux.Models.Message();
                        message.id = JSONParser.getString(object, Tags.ID);
                        if (!JSONParser.getString(object, Tags.sender_id).equalsIgnoreCase(userData.userId)) {
                            message.sender_id = JSONParser.getString(object, Tags.sender_id);
                            message.receiver_id = JSONParser.getString(object, Tags.receiver_id);

                            message.dataModel = new UserDataModel();
                            message.dataModel.first_name = object.getJSONObject("sender_data").getString("first_name");
                            message.dataModel.last_name = object.getJSONObject("sender_data").getString("last_name");
                            message.dataModel.image = object.getJSONObject("sender_data").getString("image");
                        } else {

                            message.sender_id = JSONParser.getString(object, Tags.receiver_id);
                            message.receiver_id = JSONParser.getString(object, Tags.sender_id);

                            message.dataModel = new UserDataModel();
                            message.dataModel.first_name = object.getJSONObject("reciver_data").getString("first_name");
                            message.dataModel.last_name = object.getJSONObject("reciver_data").getString("last_name");
                            message.dataModel.image = object.getJSONObject("reciver_data").getString("image");

                        }

                        message.product_id = JSONParser.getString(object, Tags.product_id);
                        message.mMessage = JSONParser.getString(object, Tags.message);
                        message.status = JSONParser.getString(object, Tags.status);
                        message.created = JSONParser.getString(object, Tags.created);
                        message.mType = com.stux.Models.Message.TYPE_MESSAGE;
                        message.user_type = com.stux.Models.Message.USER_RECEIVER;


                        message.mUsername = message.dataModel.first_name + " " + message.dataModel.last_name;

                        message.productModel = new ProductModel();
                        message.productModel = ChatFragment.getProductModelFromJSONObject(object.getJSONObject("product_data").toString());

                        message.product_image = message.productModel.image_1_thumb;
                        message.product_name = message.productModel.title;

                        AppDelegate.LogT("messageArray => " + messageArray.size());
                        messageArray.add(message);
                    }
                }
            } else {
                eventTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            eventCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.user)) {
            AppDelegate.LogT("event_list_view onItemClick");
            Bundle bundle = new Bundle();
            bundle.putString(Tags.FROM, Tags.CHAT);
            bundle.putInt(Tags.PAGE, ChatFragment.FROM_CHAT);
            bundle.putParcelable(Tags.message, messageArray.get(position));
            Fragment fragment = new ChatFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.LONG_CLICK)) {
            showChatOptionsAlert(position);
        }
    }

    public int selected_item = 0;

    private void callDeleteChatAsync(int position) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, messageArray.get(position).product_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId);
            if (messageArray.get(position).sender_id.equalsIgnoreCase(userData.userId)) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.chat_user, messageArray.get(position).receiver_id);
            } else {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.chat_user, messageArray.get(position).sender_id);
            }

            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.DELETE_CHAT,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            selected_item = position;
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

}
