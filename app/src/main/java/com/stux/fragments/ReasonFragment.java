package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.ReasonModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.ReasonRecyclerViewAdapter;

/**
 * Created by Bharat on 07/29/2016.
 */
public class ReasonFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener {

    private Prefs prefs;
    private UserDataModel dataModel;

    private Handler mHandler;

    private ArrayList<ReasonModel> arrayReasonList = new ArrayList<>();
    private GridLayoutManager gaggeredGridLayoutManager;
    private RecyclerView recyclerView;
    private ReasonRecyclerViewAdapter rcAdapter;
    private ProductModel productModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.reason_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        productModel = getArguments().getParcelable(Tags.product);
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                } else if (msg.what == 13) {
                } else if (msg.what == 2) {
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Reason");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setHasFixedSize(true);

        fillArray();
        gaggeredGridLayoutManager = new GridLayoutManager(getActivity(), 3);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5), 3));
        rcAdapter = new ReasonRecyclerViewAdapter(getActivity(), arrayReasonList, this);
        recyclerView.setAdapter(rcAdapter);

    }

    private void fillArray() {
        if (arrayReasonList.size() == 0) {
            arrayReasonList.add(new ReasonModel("2", "Wrong category", R.drawable.reason_1));
            arrayReasonList.add(new ReasonModel("4", "Fake Item", R.drawable.reason_2));
            arrayReasonList.add(new ReasonModel("5", "Photo doesn't Match", R.drawable.reason_3));
            arrayReasonList.add(new ReasonModel("6", "Food", R.drawable.reason_4));
            arrayReasonList.add(new ReasonModel("7", "Drugs or Medicine", R.drawable.reason_5));
            arrayReasonList.add(new ReasonModel("8", "People or Animals", R.drawable.reason_6));
            arrayReasonList.add(new ReasonModel("9", "Offensive Content", R.drawable.reason_7));
            arrayReasonList.add(new ReasonModel("10", "Joke", R.drawable.reason_8));
            arrayReasonList.add(new ReasonModel("11", "This item should not be for sale because", R.drawable.reason_9));
        }
    }

    private void callReportProductAsync(ReasonModel reasonModel) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
//            user_id=5&product_id=1&reason_id=3&reason_desc=xrtcfgvybu
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.reason_id, reasonModel.id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.reason_desc, reasonModel.name);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.REPORT_PRODUCT,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.REPORT_PRODUCT)) {
            parseReportProductResult(result);
        }

    }

    private void parseReportProductResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                getFragmentManager().popBackStack();
                AppDelegate.showToast(getActivity(), "Thanks for your report.");
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.reason_id) && position == 8) {
            Fragment fragment = new ReasonOtherFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, productModel);
            fragment.setArguments(bundle);
            AppDelegate.showFragment(getActivity(), fragment);
        } else if (name.equalsIgnoreCase(Tags.reason_id)) {
            callReportProductAsync(arrayReasonList.get(position));
        }
    }
}
