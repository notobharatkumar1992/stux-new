package com.stux.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.Adapters.OptionsListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.EventModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;
import com.wunderlist.slidinglayer.SlidingLayer;
import com.wunderlist.slidinglayer.transformer.AlphaTransformer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.EventRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class MyEventListingFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener {

    public static ArrayList<EventModel> eventArray = new ArrayList<>();

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    public static int eventCounter = 1, eventTotalPage = -1;
    private boolean eventAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private EventRecyclerViewAdapter rcAdapter;

    public static final String EVENT_ALL = "", EVENT_ACTIVE = "1", EVENT_WAITING = "2", EVENT_REJECTED = "3", EVENT_EXPIRED = "4";
    private String event_status = EVENT_ALL;

    public static SlidingLayer sl_more;
    public ListView list_options;
    public OptionsListAdapter adapter;
    public ArrayList<String> arrayOptions = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_event_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        eventArray.clear();
        eventCounter = 1;
        eventTotalPage = -1;
        if (eventArray.size() == 0) {
            eventAsyncExcecuting = true;
            callCampusListAsync();
        } else
            mHandler.sendEmptyMessage(2);
    }

    //    1=active,2=waiting for approval,3=reject from admin,4=expired
    private void callCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, event_status);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, eventCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_MY_EVENTS,
                    mPostArrayList, null);
            if (eventCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("mEventAdapter notified = " + eventArray.size());
                    txt_c_no_list.setVisibility(eventArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No event available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Event Lists");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.view_more);
        view.findViewById(R.id.img_c_right).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sl_more.isOpened()) {
                    sl_more.closeLayer(true);
                } else {
                    sl_more.openLayer(true);
                }
            }
        });


        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new EventRecyclerViewAdapter(getActivity(), eventArray, this);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setPadding(AppDelegate.dpToPix(getActivity(), 15), 0, AppDelegate.dpToPix(getActivity(), 15), 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (eventTotalPage != 0 && !eventAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callCampusListAsync();
                                 eventAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + eventTotalPage + ", " + eventAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

        if (arrayOptions.size() == 0) {
            arrayOptions.add("Active Events");
            arrayOptions.add("Waiting for approval");
            arrayOptions.add("Reject from admin");
            arrayOptions.add("Expired");
            arrayOptions.add("All");
            arrayOptions.add("Create New Event");
        }

        sl_more = (SlidingLayer) view.findViewById(R.id.sl_more);
        sl_more.setStickTo(SlidingLayer.STICK_TO_RIGHT);
        sl_more.setLayerTransformer(new AlphaTransformer());
        sl_more.setShadowSize(0);
        sl_more.setShadowDrawable(null);

        adapter = new OptionsListAdapter(getActivity(), arrayOptions, this);
        list_options = (ListView) view.findViewById(R.id.list_options);
        list_options.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        sl_more = null;
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MY_EVENTS)) {
            eventAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENT_LIKES)) {
            parseEventLikeSatatusResult(result);
        }
    }

    private void parseEventLikeSatatusResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                if (eventArray.get(selected_item).event_like_status.equalsIgnoreCase("1")) {
                    eventArray.get(selected_item).event_like_status = "0";
                    try {
                        int value = Integer.parseInt(eventArray.get(selected_item).total_event_likes);
                        value -= 1;
                        eventArray.get(selected_item).total_event_likes = value + "";
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                } else {
                    eventArray.get(selected_item).event_like_status = "1";
                    try {
                        int value = Integer.parseInt(eventArray.get(selected_item).total_event_likes);
                        value += 1;
                        eventArray.get(selected_item).total_event_likes = value + "";
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
                mHandler.sendEmptyMessage(2);
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        EventModel eventModel = new EventModel();
                        eventModel.id = object.getString(Tags.id);
                        eventModel.location_type = object.getString(Tags.location_type);
                        eventModel.event_type = object.getString(Tags.event_type);
                        eventModel.event_name = object.getString(Tags.event_name);
                        eventModel.banner_image = object.getString(Tags.banner_image);

                        eventModel.total_event_views = object.getString(Tags.total_event_views);
                        eventModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);
                        eventModel.email_address = object.getString(Tags.email);
                        eventModel.theme = object.getString(Tags.theme);
                        eventModel.details = object.getString(Tags.details);
                        eventModel.venue = object.getString(Tags.venue);
                        eventModel.location = JSONParser.getString(object, Tags.location);
                        eventModel.tickets = object.getString(Tags.tickets);

                        eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
                        eventModel.gate_fees = object.getString(Tags.gate_fees);
                        eventModel.contact_no = object.getString(Tags.contact_no);
                        eventModel.event_start_time = object.getString(Tags.event_start_time);

                        eventModel.event_end_time = object.getString(Tags.event_end_time);
                        eventModel.created = object.getString(Tags.created);
//                    eventModel.modified = object.getString(Tags.modified);
                        eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                        eventModel.latitude = object.getString(Tags.latitude);
                        eventModel.longitude = object.getString(Tags.longitude);

                        JSONObject userObject = object.getJSONObject(Tags.user);
                        eventModel.user_id = JSONParser.getString(userObject, Tags.id);
                        eventModel.user_first_name = userObject.getString(Tags.first_name);
                        eventModel.user_last_name = userObject.getString(Tags.last_name);
                        eventModel.user_image = userObject.getString(Tags.image);

                        JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                        if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                            eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                            eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
                            if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                                eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
                            }
                            if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                eventModel.user_department_name = studentObject.getString(Tags.department_name);
                            } else {
                                eventModel.user_department_name = studentObject.getString(Tags.department_name);
                            }
                        } else {
                            eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        }

                        eventModel.total_event_likes = object.getString(Tags.total_event_likes);
                        eventModel.event_like_status = object.getString(Tags.event_like_status);

                        eventArray.add(eventModel);
                    }
                } else {
                    eventTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                eventTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            eventCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void callLikeEventAsync(String event_id, String event_like_status) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.event_id, event_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, event_like_status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.EVENT_LIKES,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    int selected_item = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("event_list_view onItemClick => " + name + " == " + Tags.event_name + "_LIKE" + " , " + (name.equalsIgnoreCase(Tags.event_name + "_LIKE")));
        selected_item = position;
        if (name.equalsIgnoreCase(Tags.event_name)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.EVENT, eventArray.get(position));
            Fragment fragment = new EventDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.event_name + "_LIKE")) {
            AppDelegate.LogT("callLikeEventAsync called");
            callLikeEventAsync(eventArray.get(position).id + "", eventArray.get(position).event_like_status.equalsIgnoreCase("0") ? "1" : "0");

        } else if (name.equalsIgnoreCase(Tags.Options)) {
            sl_more.closeLayer(true);
            switch (position) {
                case 0:
                    event_status = EVENT_ACTIVE;
                    break;
                case 1:
                    event_status = EVENT_WAITING;
                    break;
                case 2:
                    event_status = EVENT_REJECTED;
                    break;
                case 3:
                    event_status = EVENT_EXPIRED;
                    break;

                case 4:
                    event_status = EVENT_ALL;
                    break;

                case 5:
                    break;
            }

            if (position == 5) {
                if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                } else
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new CreateEventFragment());
            } else {
                eventArray.clear();
                eventCounter = 1;
                eventTotalPage = -1;
                eventAsyncExcecuting = true;
                callCampusListAsync();
            }
        }
    }
}
