package com.stux.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.activities.SplashActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by NOTO on 5/24/2016.
 */
public class ProfileLandingFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, OnPictureResult {

    public static OnPictureResult onPictureResult;
    public static File capturedFile;
    public static Uri imageURI = null;

    private CircleImageView cimg_user;
    private android.widget.ImageView img_loading;
    private TextView txt_c_user_name, txt_c_header, txt_c_right, txt_c_description;
    private ImageView img_c_left, img_c_right;
    private Handler mHandler;
    private Bitmap OriginalPhoto;
    private Prefs prefs;
    private UserDataModel dataModel;
    private InstitutionModel institutionModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.profile_landing_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        institutionModel = prefs.getInstitutionModel();
        onPictureResult = this;
        initView(view);
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                }
            }
        };
    }

    private void initView(View view) {
        txt_c_header = (TextView) view.findViewById(R.id.txt_c_header);
        txt_c_header.setText(getString(R.string.Profile));

        img_c_left = (ImageView) view.findViewById(R.id.img_c_left);
        img_c_left.setImageResource(R.drawable.menu);
        img_c_left.setVisibility(View.VISIBLE);
        img_c_left.setOnClickListener(this);

        img_c_right = (ImageView) view.findViewById(R.id.img_c_right);
        img_c_right.setImageResource(R.drawable.filter);
        img_c_right.setVisibility(View.GONE);
        img_c_right.setOnClickListener(this);

        txt_c_right = (TextView) view.findViewById(R.id.txt_c_right);
        txt_c_right.setVisibility(View.VISIBLE);
        txt_c_right.setOnClickListener(this);

        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);
        img_loading = (android.widget.ImageView) view.findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);
        txt_c_user_name = (TextView) view.findViewById(R.id.txt_c_user_name);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        view.findViewById(R.id.txt_c_continue).setOnClickListener(this);
        view.findViewById(R.id.ll_c_upload).setOnClickListener(this);

        txt_c_user_name.setText("Hello " + dataModel.first_name + ",");

        txt_c_description.setText("\"Welcome to " + institutionModel.institution_name + " Stux Market Place\"\nCongrats! Lets get started by postings things to sell\nand make money !");
        if (AppDelegate.isValidString(dataModel.image)) {
            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();
            Picasso.with(getActivity()).load(dataModel.image).into(new Target() {
                @Override
                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                    img_loading.setVisibility(View.GONE);
                    if (bitmap != null) {
                        cimg_user.setImageBitmap(bitmap);
                        OriginalPhoto = bitmap;
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {

                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {

                }
            });
        } else {
            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
            } else {
                OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
            }
            cimg_user.setImageBitmap(OriginalPhoto);
        }
    }

    private void execute_updateProfileApi() {
        if (OriginalPhoto == null) {
            AppDelegate.showAlert(getActivity(), "Please select image.");
        } else if (AppDelegate.haveNetworkConnection(getActivity())) {
            writeImageFile();
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.first_name, dataModel.first_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.last_name, dataModel.last_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, dataModel.email);
            if (AppDelegate.isValidString(dataModel.dob))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.dob, dataModel.dob);
            if (AppDelegate.isValidString(dataModel.str_Gender))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gender, dataModel.str_Gender);
            if (AppDelegate.isValidString(dataModel.city_name))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_name, "");
            if (AppDelegate.isValidString(dataModel.fav_cat_id))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.fav_cat_id, "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ProfileLandingFragment.this, ServerRequestConstants.EDIT_PROFILE,
                    mPostArrayList, ProfileLandingFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_continue:
                execute_updateProfileApi();
                break;

            case R.id.ll_c_upload:
                showImageSelectorList();
                break;

            case R.id.txt_c_right:
                prefs.clearTempPrefs();
                prefs.clearSharedPreference();
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.IS_SPLASH, Tags.FALSE);
                intent.putExtras(bundle);
                startActivity(intent);
                getActivity().finish();
                break;

            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Avatar", "  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        showAvatarAlert();
                        break;
                    case 1:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 2:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private void showAvatarAlert() {
        try {
            final AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(true);
            mAlert.setTitle("Use image");
            LinearLayout ll_c_img_01, ll_c_img_02;
            ImageView img_01, img_02;
            View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_default_user_image, null, false);

            img_01 = (ImageView) view.findViewById(R.id.img_01);
            img_02 = (ImageView) view.findViewById(R.id.img_02);
            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                img_01.setImageResource(R.drawable.user_male_1);
                img_02.setImageResource(R.drawable.user_male_2);
            } else {
                img_01.setImageResource(R.drawable.user_female_1);
                img_02.setImageResource(R.drawable.user_female_2);
            }

            ll_c_img_01 = (LinearLayout) view.findViewById(R.id.ll_c_img_01);
            ll_c_img_02 = (LinearLayout) view.findViewById(R.id.ll_c_img_02);
            mAlert.setView(view);
            mAlert.setNegativeButton(
                    Tags.CANCEL,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog dialog = mAlert.show();

            ll_c_img_01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
                    }

                    cimg_user.setImageBitmap(OriginalPhoto);
                }
            });
            ll_c_img_02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_2);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_2);
                    }

                    cimg_user.setImageBitmap(OriginalPhoto);
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void writeImageFile() {
        if (capturedFile == null)
            capturedFile = new File(getNewFile());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 100, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        getActivity().startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PROFILE)) {
            parseUpdateProfileResult(result);
        }
    }

    private void parseUpdateProfileResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);

                ((MainActivity) getActivity()).updateUserDetail();
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new HomeFragment());

            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                cimg_user.setImageBitmap(OriginalPhoto);
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onPictureResult = null;
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            getActivity().startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }
}
