package com.stux.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.Adapters.PagerLoopAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.EventModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.SliderModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.MyViewPager;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.EventRecyclerViewAdapter;
import inducesmile.com.androidstaggeredgridlayoutmanager.ProductRecyclerViewAdapter;

/**
 * Created by NOTO on 5/27/2016.
 */
public class EventsCampusBasedFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnListItemClickListener {

    public ArrayList<ProductModel> productArray = new ArrayList<>();
    private MyViewPager view_pager_banner;
    private PagerLoopAdapter bannerPagerAdapter;
    private LinearLayout ll_c_off_campus, ll_c_on_campus;
    private ArrayList<Fragment> bannerFragment = new ArrayList<>();
    private ArrayList<SliderModel> sliderArray = new ArrayList<>();
    private RelativeLayout rl_c_banner;
    private Handler mHandler;
    private ProgressBar progressbar;
    private android.widget.LinearLayout pager_indicator;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;
    private ImageView[] dots;
    private TextView txt_c_shop_now;
    private Prefs prefs;
    private UserDataModel dataModel;

    private RecyclerView recyclerView;
    private SpacesItemDecoration eventOffCampusItemDecoration;
    private LinearLayoutManager linearLayoutManager;

    // Event OFF campus list
    public static ArrayList<EventModel> eventOffCampusArray = new ArrayList<>();
    private EventRecyclerViewAdapter eventOffCampusAdapter;
    public static int eventOffCampusCounter = 1, eventOffCampusTotalPage = -1;

    // Event On campus list
    private EventRecyclerViewAdapter eventOnCampusAdapter;
    public static ArrayList<EventModel> eventOnCampusArray = new ArrayList<>();
    public static int eventOnCampusCounter = 1, eventOnCampusTotalPage = -1;

    private PostAsync sliderAsync;

    private boolean eventOffCampusAsyncExcecuting = false, eventOnCampusAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.events_on_off_campus_page, container, false);
    }

    public View convertView;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        convertView = view;
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        sliderArray.clear();
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_shop_now.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 14) {
//                    progressbar_1.setVisibility(View.VISIBLE);
//                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 15) {
//                    progressbar_1.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    AppDelegate.LogT("sliderArray = " + sliderArray.size());
                    bannerFragment.clear();
                    for (int i = 0; i < sliderArray.size(); i++) {
                        Fragment fragment = new BannerHomeFragment();
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Tags.slider_id, sliderArray.get(i));
                        fragment.setArguments(bundle);
                        bannerFragment.add(fragment);
                    }

                    bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
                    view_pager_banner.setAdapter(bannerPagerAdapter);

                    setUiPageViewController();
                    if (sliderArray.size() == 0) {
                        txt_c_shop_now.setVisibility(View.VISIBLE);
                        if (selected_tab == 0) {
                            txt_c_shop_now.setText("Top Events banner are not available.");
                        } else if (selected_tab == 1) {
                            txt_c_shop_now.setText("Top Events banner are not available.");
                        }
                    } else {
                        txt_c_shop_now.setVisibility(View.GONE);
                        rl_c_banner.setVisibility(View.VISIBLE);
                        view_pager_banner.setVisibility(View.VISIBLE);
                    }
                    timeOutLoop();
                } else if (msg.what == 3) {
                    AppDelegate.LogT("handler 3 Off => selected_tab = " + selected_tab + ", countDown = " + eventOffCampusCounter);
                    if (selected_tab == 1) {
                        eventOffCampusAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                        recyclerView.smoothScrollToPosition(0);
                    }
                } else if (msg.what == 4) {
                    AppDelegate.LogT("handler 4 on => selected_tab = " + selected_tab + ", countDown = " + eventOnCampusCounter);
                    if (selected_tab == 0) {
                        eventOnCampusAdapter.notifyDataSetChanged();
                        recyclerView.invalidate();
                        recyclerView.smoothScrollToPosition(0);
                    }
                } else if (msg.what == 20) {
                    if (convertView != null)
                        convertView.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
                }
            }
        };
    }

    private void initView(View view) {
        rl_c_banner = (RelativeLayout) view.findViewById(R.id.rl_c_banner);

        android.widget.LinearLayout.LayoutParams layoutParams = (android.widget.LinearLayout.LayoutParams) rl_c_banner.getLayoutParams();
        layoutParams.height = AppDelegate.getDeviceWith(getActivity()) / 2 - AppDelegate.dpToPix(getActivity(), 10);
        rl_c_banner.setLayoutParams(layoutParams);

        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Events");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        ll_c_off_campus = (LinearLayout) view.findViewById(R.id.ll_c_off_campus);
        ll_c_off_campus.setOnClickListener(this);
        ll_c_on_campus = (LinearLayout) view.findViewById(R.id.ll_c_on_campus);
        ll_c_on_campus.setOnClickListener(this);

        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        txt_c_shop_now = (TextView) view.findViewById(R.id.txt_c_shop_now);

        pager_indicator = (android.widget.LinearLayout) view.findViewById(R.id.pager_indicator);
        view_pager_banner = (MyViewPager) view.findViewById(R.id.view_pager_banner);
        bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
        view_pager_banner.setAdapter(bannerPagerAdapter);
        view_pager_banner.setVisibility(View.VISIBLE);
        setUiPageViewController();
        view_pager_banner.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        switchBannerPage(0);


        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        gaggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        campusItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5));


        eventOffCampusItemDecoration = new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 15), true);
        linearLayoutManager = new LinearLayoutManager(getActivity());

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setPadding(0, 0, 0, 0);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(eventOffCampusItemDecoration);

        // Movies list Data init
        eventOnCampusAdapter = new EventRecyclerViewAdapter(getActivity(), eventOnCampusArray, this, Tags.eventOnCampus);
        recyclerView.setAdapter(eventOnCampusAdapter);

        // Deal list Data init
        eventOffCampusAdapter = new EventRecyclerViewAdapter(getActivity(), eventOffCampusArray, this, Tags.eventOffCampus);
        recyclerView.setAdapter(eventOffCampusAdapter);


        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (selected_tab == 1) {
                                 if (eventOffCampusTotalPage != 0 && !eventOffCampusAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(2);
                                     callEventOffCampusListAsync();
                                     eventOffCampusAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 1, " + eventOffCampusTotalPage + ", " + eventOffCampusAsyncExcecuting);
                                 }
                             } else {
                                 if (eventOnCampusTotalPage != 0 && !eventOnCampusAsyncExcecuting) {
                                     mHandler.sendEmptyMessage(4);
                                     callEventOnCampusListAsync();
                                     eventOnCampusAsyncExcecuting = true;
                                 } else {
                                     swipyrefreshlayout.setRefreshing(false);
                                     AppDelegate.LogT("selected_tab = 2, " + eventOnCampusTotalPage + ", " + eventOnCampusAsyncExcecuting);
                                 }
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );

        switchPage(selected_tab);
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        int value = view_pager_banner.getCurrentItem();
                        if (value == bannerPagerAdapter.getCount() - 1) {
                            value = 0;
                        } else {
                            value++;
                        }
                        view_pager_banner.setCurrentItem(value, true);
                        if (isAdded()) {
                            mHandler.postDelayed(this, 3000);
                        } else {
                            mHandler.removeCallbacks(this);
                        }
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }


    private void callEventOffCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, eventOffCampusCounter + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.location_type, "2");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, Tags.eventOffCampus, ServerRequestConstants.EVENTS_LIST,
                    mPostArrayList, null);
            if (eventOffCampusCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callEventOnCampusListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, eventOnCampusCounter + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.location_type, "1");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, Tags.eventOnCampus, ServerRequestConstants.EVENTS_LIST,
                    mPostArrayList, null);
            if (eventOnCampusCounter == 1)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void switchBannerPage(int position) {
        try {
            if (dotsCount > 0) {
                for (int i = 0; i < dotsCount; i++) {
                    if (dots != null && dots.length > i)
                        dots[i].setImageResource(R.drawable.white_radius_square);
                }
                if (dots != null && dots.length > position)
                    dots[position].setImageResource(R.drawable.orange_radius_square);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        timeOutLoop();
    }

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(20, 20);
                if (getString(R.string.values_folder).equalsIgnoreCase("values-hdpi")) {
                    params = new LinearLayout.LayoutParams(12, 12);
                    params.setMargins(7, 0, 7, 0);
                } else
                    params.setMargins(10, 0, 10, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.orange_radius_square));
        }
    }

    private void callClicksAsync(SliderModel sliderModel, int type) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_id, sliderModel.id);
            if (type == 0)
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.click_status, "1");
            else
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.thumb_status, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, AppDelegate.getUUID(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_CLICKS,
                    mPostArrayList, this);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void callSliderAsync(String value) {
        if (sliderArray.size() > 0)
            return;
        try {
            if (sliderAsync != null) {
                sliderAsync.cancelAsync(false);
                sliderAsync.cancel(true);
            }
            sliderArray.clear();
            bannerFragment.clear();
            bannerPagerAdapter = new PagerLoopAdapter(getChildFragmentManager(), bannerFragment);
            view_pager_banner.setAdapter(bannerPagerAdapter);
            mHandler.sendEmptyMessage(1);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_type, value);
            sliderAsync = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_LIST,
                    mPostArrayList, this);
            txt_c_shop_now.setVisibility(View.GONE);
            mHandler.sendEmptyMessage(12);
            sliderAsync.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_LIST)) {
            mHandler.sendEmptyMessage(13);
            parseSliderAsync(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_CLICKS)) {
            mHandler.sendEmptyMessage(13);
            parseSlidersClick(result);
        } else if (apiName.equalsIgnoreCase(Tags.eventOffCampus)) {
            if (eventOffCampusCounter > 1) {
                eventOffCampusAsyncExcecuting = false;
            } else if (eventOffCampusCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 1)
                swipyrefreshlayout.setRefreshing(false);
            parseEventOffCampusResult(result);
        } else if (apiName.equalsIgnoreCase(Tags.eventOnCampus)) {
            if (eventOnCampusCounter > 1) {
                eventOnCampusAsyncExcecuting = false;
            } else if (eventOnCampusCounter == 1) {
                mHandler.sendEmptyMessage(11);
            }
            if (selected_tab == 0)
                swipyrefreshlayout.setRefreshing(false);
            parseEventOnCampusResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EVENT_LIKES)) {
            mHandler.sendEmptyMessage(11);
            parseEventLikeSatatusResult(result);
        }
    }

    private void parseEventLikeSatatusResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                if (from_campus == FROM_OFF_CAMPUS) {
                    AppDelegate.LogT("size => " + eventOffCampusArray.size() + " = " + selected_item);
                    if (eventOffCampusArray.get(selected_item).event_like_status.equalsIgnoreCase("1")) {
                        eventOffCampusArray.get(selected_item).event_like_status = "0";
                        try {
                            int value = Integer.parseInt(eventOffCampusArray.get(selected_item).total_event_likes);
                            value -= 1;
                            eventOffCampusArray.get(selected_item).total_event_likes = value + "";
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    } else {
                        eventOffCampusArray.get(selected_item).event_like_status = "1";
                        try {
                            int value = Integer.parseInt(eventOffCampusArray.get(selected_item).total_event_likes);
                            value += 1;
                            eventOffCampusArray.get(selected_item).total_event_likes = value + "";
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                    mHandler.sendEmptyMessage(3);
                } else {
                    if (eventOnCampusArray.get(selected_item).event_like_status.equalsIgnoreCase("1")) {
                        eventOnCampusArray.get(selected_item).event_like_status = "0";
                        try {
                            int value = Integer.parseInt(eventOnCampusArray.get(selected_item).total_event_likes);
                            value -= 1;
                            eventOnCampusArray.get(selected_item).total_event_likes = value + "";
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    } else {
                        eventOnCampusArray.get(selected_item).event_like_status = "1";
                        try {
                            int value = Integer.parseInt(eventOnCampusArray.get(selected_item).total_event_likes);
                            value += 1;
                            eventOnCampusArray.get(selected_item).total_event_likes = value + "";
                        } catch (Exception e) {
                            AppDelegate.LogE(e);
                        }
                    }
                    mHandler.sendEmptyMessage(4);
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseEventOffCampusResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        EventModel eventModel = new EventModel();
                        eventModel.id = object.getString(Tags.id);
                        eventModel.location_type = object.getString(Tags.location_type);
                        eventModel.event_type = object.getString(Tags.event_type);
                        eventModel.event_name = object.getString(Tags.event_name);
                        eventModel.banner_image = object.getString(Tags.banner_image);

                        eventModel.total_event_views = object.getString(Tags.total_event_views);
                        eventModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);
                        eventModel.email_address = object.getString(Tags.email);
                        eventModel.theme = object.getString(Tags.theme);
                        eventModel.details = object.getString(Tags.details);
                        eventModel.venue = object.getString(Tags.venue);
                        eventModel.location = JSONParser.getString(object, Tags.location);
                        eventModel.tickets = object.getString(Tags.tickets);

                        eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
                        eventModel.gate_fees = object.getString(Tags.gate_fees);
                        eventModel.contact_no = object.getString(Tags.contact_no);
                        eventModel.event_start_time = object.getString(Tags.event_start_time);

                        eventModel.event_end_time = object.getString(Tags.event_end_time);
                        eventModel.created = object.getString(Tags.created);
//                    eventModel.modified = object.getString(Tags.modified);
                        eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                        eventModel.latitude = object.getString(Tags.latitude);
                        eventModel.longitude = object.getString(Tags.longitude);

                        JSONObject userObject = object.getJSONObject(Tags.user);
                        eventModel.user_id = JSONParser.getString(userObject, Tags.id);
                        eventModel.user_first_name = userObject.getString(Tags.first_name);
                        eventModel.user_last_name = userObject.getString(Tags.last_name);
                        eventModel.user_image = userObject.getString(Tags.image);

                        JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                        if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                            eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                            eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
                            if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                                eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
                            }
                            if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                eventModel.user_department_name = studentObject.getString(Tags.department_name);
                            } else {
                                eventModel.user_department_name = studentObject.getString(Tags.department_name);
                            }
                        } else {
                            eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        }

                        eventModel.total_event_likes = object.getString(Tags.total_event_likes);
                        eventModel.event_like_status = object.getString(Tags.event_like_status);

                        eventOffCampusArray.add(eventModel);
                    }
                } else {
                    eventOffCampusTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                eventOffCampusTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            eventOffCampusCounter++;

            mHandler.sendEmptyMessage(3);
        } catch (Exception e) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }


    private void parseEventOnCampusResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        EventModel eventModel = new EventModel();
                        eventModel.id = object.getString(Tags.id);
                        eventModel.location_type = object.getString(Tags.location_type);
                        eventModel.event_type = object.getString(Tags.event_type);
                        eventModel.event_name = object.getString(Tags.event_name);
                        eventModel.banner_image = object.getString(Tags.banner_image);

                        eventModel.total_event_views = object.getString(Tags.total_event_views);
                        eventModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);
                        eventModel.email_address = object.getString(Tags.email);
                        eventModel.theme = object.getString(Tags.theme);
                        eventModel.details = object.getString(Tags.details);
                        eventModel.venue = object.getString(Tags.venue);
                        eventModel.location = JSONParser.getString(object, Tags.location);
                        eventModel.tickets = object.getString(Tags.tickets);

                        eventModel.gate_fees_type = object.getInt(Tags.gate_fees_type);
                        eventModel.gate_fees = object.getString(Tags.gate_fees);
                        eventModel.contact_no = object.getString(Tags.contact_no);
                        eventModel.event_start_time = object.getString(Tags.event_start_time);

                        eventModel.event_end_time = object.getString(Tags.event_end_time);
                        eventModel.created = object.getString(Tags.created);
//                    eventModel.modified = object.getString(Tags.modified);
                        eventModel.banner_image_thumb = object.getString(Tags.banner_image_thumb);

                        eventModel.latitude = object.getString(Tags.latitude);
                        eventModel.longitude = object.getString(Tags.longitude);

                        JSONObject userObject = object.getJSONObject(Tags.user);
                        eventModel.user_id = JSONParser.getString(userObject, Tags.id);
                        eventModel.user_first_name = userObject.getString(Tags.first_name);
                        eventModel.user_last_name = userObject.getString(Tags.last_name);
                        eventModel.user_image = userObject.getString(Tags.image);

                        JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                        if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                            eventModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                            eventModel.user_institution_id = studentObject.getString(Tags.institution_id);
                            if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                                eventModel.user_institution_name = studentObject.getString(Tags.institution_name);
                            }
                            if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                eventModel.user_department_name = studentObject.getString(Tags.department_name);
                            } else {
                                eventModel.user_department_name = studentObject.getString(Tags.department_name);
                            }
                        } else {
                            eventModel.user_institution_name = studentObject.getString(Tags.other_ins_name);
                            eventModel.user_department_name = studentObject.getString(Tags.department_name);
                        }

                        eventModel.total_event_likes = object.getString(Tags.total_event_likes);
                        eventModel.event_like_status = object.getString(Tags.event_like_status);

                        eventOnCampusArray.add(eventModel);
                    }
                } else {
                    eventOnCampusTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                eventOnCampusTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            eventOnCampusCounter++;

            mHandler.sendEmptyMessage(4);
        } catch (Exception e) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSlidersClick(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            } else {
                if (click_type == 1) {
                    sliderArray.get(item_position).thump_clicked = 1;
                } else {
                    sliderArray.get(item_position).single_clicked = 1;
                }
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    private void parseSliderAsync(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            sliderArray.clear();
            ((MainActivity) getActivity()).updateNotificationCount(JSONParser.getString(jsonObject, Tags.total_notifications));
            mHandler.sendEmptyMessage(20);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    SliderModel sliderModel = new SliderModel();
                    sliderModel.id = object.getString(Tags.id);
                    sliderModel.banner_image = object.getString(Tags.img);
//                    sliderModel.banner_thumb_image = object.getString(Tags.thumb);
                    sliderModel.banner_thumb_image = sliderModel.banner_image;
                    sliderModel.expiry_date = object.getString(Tags.expiry_date);
                    sliderModel.slider_category = object.getString(Tags.slider_category);
                    sliderModel.status = object.getString(Tags.status);
                    sliderModel.created = object.getString(Tags.created);
                    sliderModel.title = object.getString(Tags.title);
                    sliderModel.descriptions = object.getString(Tags.description);
                    sliderModel.url = object.getString(Tags.url);
                    sliderModel.banner_type = object.getInt(Tags.banner_type);
                    sliderModel.type_of_slider = selected_tab;
                    sliderModel.from_page = 2;

                    if (object.has(Tags.click_status) && object.optJSONObject(Tags.click_status) != null) {
                        sliderModel.single_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.click_status));
                        sliderModel.thump_clicked = Integer.parseInt(object.getJSONObject(Tags.click_status).getString(Tags.thumb_status));
                    } else {
                        sliderModel.single_clicked = 0;
                        sliderModel.thump_clicked = 0;
                    }
                    sliderArray.add(sliderModel);
                }
            }
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
        mHandler.sendEmptyMessage(1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity) getActivity()).toggleSlider();
                    }
                }, 300);
                break;

            case R.id.img_c_right:
                break;

            case R.id.ll_c_on_campus:
                selected_tab = 0;
                switchPage(0);
                break;

            case R.id.ll_c_off_campus:
                selected_tab = 1;
                switchPage(1);
                break;

            case R.id.txt_c_shop_now:
                break;
        }
    }

    private void switchPage(int i) {
        swipyrefreshlayout.setRefreshing(false);

        ll_c_on_campus.setSelected(false);
        ll_c_off_campus.setSelected(false);
        callDealsAdapterBeforeGridAdapter();

        switch (i) {
            case 0:
                ll_c_on_campus.setSelected(true);
                recyclerView.setHasFixedSize(true);
                recyclerView.setBackgroundColor(Color.WHITE);
                recyclerView.setPadding(0, 0, 0, 0);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(eventOffCampusItemDecoration);

                eventOnCampusAdapter = new EventRecyclerViewAdapter(getActivity(), eventOnCampusArray, this, Tags.eventOnCampus);
                recyclerView.setAdapter(eventOnCampusAdapter);
                if (eventOnCampusArray.size() == 0) {
                    callEventOnCampusListAsync();
                } else {
                    mHandler.sendEmptyMessage(4);
                }
                callSliderAsync("2");
                break;

            case 1:
                ll_c_off_campus.setSelected(true);

                recyclerView.setHasFixedSize(true);
                recyclerView.setBackgroundColor(Color.WHITE);
                recyclerView.setPadding(0, 0, 0, 0);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.addItemDecoration(eventOffCampusItemDecoration);

                eventOffCampusAdapter = new EventRecyclerViewAdapter(getActivity(), eventOffCampusArray, this, Tags.eventOffCampus);
                recyclerView.setAdapter(eventOffCampusAdapter);
                if (eventOffCampusArray.size() == 0) {
                    callEventOffCampusListAsync();
                } else {
                    mHandler.sendEmptyMessage(3);
                }
                callSliderAsync("2");
                break;
        }
    }

    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private SpacesItemDecoration campusItemDecoration;
    private ProductRecyclerViewAdapter rcAdapter;

    private void callDealsAdapterBeforeGridAdapter() {
        // Adapter was not notifying for that reason i used to this function
        recyclerView.removeItemDecoration(campusItemDecoration);
        recyclerView.removeItemDecoration(eventOffCampusItemDecoration);

        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
        recyclerView.setAdapter(rcAdapter);
    }

    private void callLikeEventAsync(String event_id, String event_like_status) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.event_id, event_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, event_like_status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.EVENT_LIKES,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    public final int FROM_OFF_CAMPUS = 0, FROM_ON_CAMPUS = 1;
    int selected_item = 0, from_campus = FROM_OFF_CAMPUS;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        selected_item = position;
        if (name.equalsIgnoreCase(Tags.eventOffCampus)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.EVENT, eventOffCampusArray.get(position));
            bundle.putString(Tags.FROM, Tags.eventOffCampus);
            Fragment fragment = new EventDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);

        } else if (name.equalsIgnoreCase(Tags.eventOffCampus + "_LIKE")) {
            from_campus = FROM_OFF_CAMPUS;
            callLikeEventAsync(eventOffCampusArray.get(position).id + "", eventOffCampusArray.get(position).event_like_status.equalsIgnoreCase("0") ? "1" : "0");

        } else if (name.equalsIgnoreCase(Tags.eventOnCampus + "_LIKE")) {
            from_campus = FROM_ON_CAMPUS;
            callLikeEventAsync(eventOnCampusArray.get(position).id + "", eventOnCampusArray.get(position).event_like_status.equalsIgnoreCase("0") ? "1" : "0");

        } else if (name.equalsIgnoreCase(Tags.eventOnCampus)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.EVENT, eventOnCampusArray.get(position));
            bundle.putString(Tags.FROM, Tags.eventOnCampus);
            Fragment fragment = new EventDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }
}
