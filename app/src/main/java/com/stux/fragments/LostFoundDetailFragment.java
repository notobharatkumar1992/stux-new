package com.stux.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.stux.Adapters.ProductPagerAdapter;
import com.stux.AppDelegate;
import com.stux.Models.ItemFoundModel;
import com.stux.R;
import com.stux.activities.LargeImageActivity;
import com.stux.activities.MainActivity;
import com.stux.activities.MapDetailActivity;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/03/2016.
 */
public class LostFoundDetailFragment extends Fragment implements OnListItemClickListener {

    public ArrayList<String> arrayString = new ArrayList<>();
    private ImageView img_product;
    private TextView txt_c_name, txt_c_condition, txt_c_image_count, txt_c_address, txt_c_contact, txt_c_description, txt_c_date;
    private ItemFoundModel foundModel;

    private ViewPager view_pager;
    private ProductPagerAdapter mPagerAdapter;


    private GoogleMap map_business;
    private SupportMapFragment fragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lost_found_detail_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        foundModel = getArguments().getParcelable(Tags.FOUND);
        initView(view);
        setValues();
    }

    private void setValues() {
        txt_c_name.setText(foundModel.item_name);
        txt_c_address.setText(foundModel.location);
        txt_c_contact.setText(foundModel.contact_no);
        txt_c_description.setText(Html.fromHtml("<b>Description : </b>" + foundModel.item_description));
        if (AppDelegate.isValidString(foundModel.created)) {
            try {
                txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + new SimpleDateFormat("dd MMM yyyy, hh:mm aa").format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(foundModel.created))));
            } catch (Exception e) {
                AppDelegate.LogE(e);
                txt_c_date.setText(Html.fromHtml("<b>Date : </b>" + foundModel.created + ""));
            }
        } else {
            txt_c_date.setVisibility(View.GONE);
        }
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Lost and Found");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.edit);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);

        img_product = (ImageView) view.findViewById(R.id.img_product);

        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);


        txt_c_condition = (TextView) view.findViewById(R.id.txt_c_condition);
        txt_c_image_count = (TextView) view.findViewById(R.id.txt_c_image_count);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_contact = (TextView) view.findViewById(R.id.txt_c_contact);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        txt_c_date = (TextView) view.findViewById(R.id.txt_c_date);

        if (arrayString.size() == 0) {
            if (AppDelegate.isValidString(foundModel.image_thumb_4)) {
                AppDelegate.LogT("LostFound => 4");
                txt_c_image_count.setText("4");
                arrayString.add(foundModel.image_thumb_1);
                arrayString.add(foundModel.image_thumb_2);
                arrayString.add(foundModel.image_thumb_3);
                arrayString.add(foundModel.image_thumb_4);
            } else if (AppDelegate.isValidString(foundModel.image_thumb_3)) {
                txt_c_image_count.setText("3");
                arrayString.add(foundModel.image_thumb_1);
                arrayString.add(foundModel.image_thumb_2);
                arrayString.add(foundModel.image_thumb_3);
            } else if (AppDelegate.isValidString(foundModel.image_thumb_2)) {
                txt_c_image_count.setText("2");
                arrayString.add(foundModel.image_thumb_1);
                arrayString.add(foundModel.image_thumb_2);
            } else if (AppDelegate.isValidString(foundModel.image_thumb_1)) {
                txt_c_image_count.setText("1");
                arrayString.add(foundModel.image_thumb_1);
            } else {
                txt_c_image_count.setText("0");
            }
        }

        mPagerAdapter = new ProductPagerAdapter(getActivity(), arrayString, this);
        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
        view_pager.setAdapter(mPagerAdapter);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        view.findViewById(R.id.view_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                } else {
                    Intent intent = new Intent(getActivity(), MapDetailActivity.class);
                    intent.putExtra(Tags.LAT, foundModel.lat);
                    intent.putExtra(Tags.LNG, foundModel.lng);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        fragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.map_frame, fragment, "MAP1").addToBackStack(null)
                .commit();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showMap();
            }
        }, 1500);
    }

    private void showMap() {
        map_business = fragment.getMap();
        if (map_business == null) {
            return;
        }

        map_business.setMyLocationEnabled(true);
        map_business.getUiSettings().setMapToolbarEnabled(false);
        map_business.getUiSettings().setZoomControlsEnabled(false);
        map_business.getUiSettings().setCompassEnabled(false);
        map_business.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        AppDelegate.setMyLocationBottomRightButton(getActivity(), fragment);
        if (foundModel != null && AppDelegate.isValidString(foundModel.lat) && AppDelegate.isValidString(foundModel.lng)) {
            AppDelegate.LogT("location showing => " + foundModel.lat + "," + foundModel.lng);
            map_business.addMarker(AppDelegate.getMarkerOptions(getActivity(), new LatLng(Double.parseDouble(foundModel.lat), Double.parseDouble(foundModel.lng)), R.drawable.map_pin));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(Double.parseDouble(foundModel.lat), Double.parseDouble(foundModel.lng)))
                    .zoom(14).build();
            //Zoom in and animate the camera.
            map_business.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    private int item_position = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
                Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image_1, foundModel.image_1);
                bundle.putString(Tags.image_2, foundModel.image_2);
                bundle.putString(Tags.image_3, foundModel.image_3);
                bundle.putString(Tags.image_4, foundModel.image_4);
                bundle.putInt(Tags.POSITION, position);
                bundle.putInt(Tags.count, 4);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }
}
