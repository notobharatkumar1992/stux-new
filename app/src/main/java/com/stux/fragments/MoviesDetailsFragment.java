package com.stux.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.stux.Adapters.MoviesDetailListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.MovieTime;
import com.stux.Models.MoviesModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.LargeImageActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.tweetcomposer.Card;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import carbon.widget.TextView;

/**
 * Created by NOTO on 5/30/2016.
 */
public class MoviesDetailsFragment extends FragmentActivity implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse, YouTubePlayer.OnInitializedListener {

    private TextView txt_c_movies_name, txt_c_genres, txt_c_viewer, txt_c_timer, txt_c_starring, txt_c_imdb, txt_c_sub_title, txt_c_synopsis, txt_c_directed_by, txt_c_ticket_fee, txt_c_cinema_count;
    private carbon.widget.ImageView img_c_movies_banner;
    private ImageView img_loading;
    private ScrollView scrollView;

    private YouTubePlayerFragment youTubePlayerFragment;
    private ListView list_view;
    private MoviesDetailListAdapter listAdapter;

    private MoviesModel moviesModel;
    private Prefs prefs;
    private UserDataModel dataModel;

    private boolean asyncExecuting = false;
    TwitterLoginButton loginButton;
    private Handler mHandler;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MapsInitializer.initialize(this);
        setContentView(R.layout.movies_detail);
        FacebookSdk.sdkInitialize(this);
        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();

        prefs = new Prefs(MoviesDetailsFragment.this);
        dataModel = prefs.getUserdata();
        moviesModel = getIntent().getExtras().getParcelable(Tags.movies);
        initView();
        setValues();
        setHandler();
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_UP);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (callbackManager != null)
            callbackManager.onActivityResult(requestCode, resultCode, data);
        if (loginButton != null)
            loginButton.onActivityResult(requestCode, resultCode, data);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 10:
                        AppDelegate.showProgressDialog(MoviesDetailsFragment.this);
                        break;
                    case 11:
                        AppDelegate.hideProgressDialog(MoviesDetailsFragment.this);
                        break;
                }
            }
        };
    }

    private void setValues() {
        if (moviesModel != null) {
            txt_c_movies_name.setText("Title: " + moviesModel.title);
            txt_c_viewer.setText(AppDelegate.isValidString(moviesModel.total_movie_views) ? moviesModel.total_movie_views : "0");
            txt_c_genres.setText(Html.fromHtml("<b>" + moviesModel.genre + "</b>"), TextView.BufferType.SPANNABLE);

            txt_c_directed_by.setText(Html.fromHtml("<b>Directer :</b>" + moviesModel.directed_by), TextView.BufferType.SPANNABLE);
            txt_c_starring.setText(Html.fromHtml("<b>Starring :</b>" + moviesModel.starring), TextView.BufferType.SPANNABLE);
            txt_c_sub_title.setText(Html.fromHtml("<b>Sub Title :</b>" + moviesModel.sub_title), TextView.BufferType.SPANNABLE);

//            Spannable wordtoSpan = new SpannableString("IMDB: " + moviesModel.imdb_rating);
//            wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.orange)), 0, 5, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            txt_c_imdb.setText(moviesModel.imdb_rating);

            txt_c_timer.setText(AppDelegate.isValidString(moviesModel.run_time) ? moviesModel.run_time : "0 MINUTES" + "");

            txt_c_synopsis.setText(moviesModel.synopsis);

//            wordtoSpan = new SpannableString("Rating: " + moviesModel.rating);
//            wordtoSpan.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.orange)), 0, 7, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_c_ticket_fee.setText(moviesModel.rating);

            if (moviesModel.logged_user_view_status.equalsIgnoreCase("0") && !asyncExecuting) {
                executeItemViewApi();
            }

            img_loading.setVisibility(View.VISIBLE);
            AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
            frameAnimation.setCallback(img_loading);
            frameAnimation.setVisible(true, true);
            frameAnimation.start();

            AppDelegate.LogT("Picasso called");
            Picasso.with(this).load(moviesModel.banner_image).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    OriginalPhoto = bitmap;
                    img_c_movies_banner.setImageBitmap(bitmap);
                    img_loading.setVisibility(View.GONE);
                    AppDelegate.LogT("onBitmapLoaded called");
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            });
        }
    }

    private void executeItemViewApi() {
        if (AppDelegate.haveNetworkConnection(MoviesDetailsFragment.this, false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(MoviesDetailsFragment.this).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(MoviesDetailsFragment.this).setPostParamsSecond(mPostArrayList, Tags.movie_id, moviesModel.id);
            PostAsync mPostasyncObj = new PostAsync(MoviesDetailsFragment.this,
                    MoviesDetailsFragment.this, ServerRequestConstants.MOVIES_VIEW,
                    mPostArrayList, null);
//            mHandler.sendEmptyMessage(10);
            asyncExecuting = true;
            mPostasyncObj.execute();
        }
    }

    public static void setListViewHeight(Context mContext, final ListView listView, ListAdapter gridAdapter) {
        try {
            if (gridAdapter == null) {
                // pre-condition
                AppDelegate.LogE("Adapter is null");
                ViewGroup.LayoutParams params = listView.getLayoutParams();
                params.height = 80;
                listView.setLayoutParams(params);
                listView.requestLayout();
                return;
            }
            int totalHeight = 0;
            int itemCount = gridAdapter.getCount();

            int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST);
            AppDelegate.LogT("itemCount = " + itemCount);
            for (int i = 0; i < itemCount; i++) {
                View listItem = gridAdapter.getView(i, null, listView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
//                totalHeight += AppDelegate.dpToPix(MoviesDetailsFragment.this, 120);
                totalHeight += AppDelegate.dpToPix(mContext, 112);
                AppDelegate.LogT("totalHeight = " + totalHeight);
            }
//            totalHeight += AppDelegate.dpToPix(mContext, 120);
//            totalHeight += AppDelegate.dpToPix(mContext, 10);
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalHeight;
            listView.setLayoutParams(params);
            listView.post(new Runnable() {
                @Override
                public void run() {
                    listView.requestLayout();
                }
            });

        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void initView() {
        findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) findViewById(R.id.txt_c_header)).setText("Movies Detail");
        findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        findViewById(R.id.img_c_left).setOnClickListener(this);

        txt_c_movies_name = (TextView) findViewById(R.id.txt_c_movies_name);
        txt_c_genres = (TextView) findViewById(R.id.txt_c_genres);
        txt_c_viewer = (TextView) findViewById(R.id.txt_c_viewer);
        txt_c_timer = (TextView) findViewById(R.id.txt_c_timer);
        txt_c_starring = (TextView) findViewById(R.id.txt_c_starring);
        txt_c_sub_title = (TextView) findViewById(R.id.txt_c_sub_title);
        txt_c_imdb = (TextView) findViewById(R.id.txt_c_imdb);
        txt_c_synopsis = (TextView) findViewById(R.id.txt_c_synopsis);

        txt_c_directed_by = (TextView) findViewById(R.id.txt_c_directed_by);
        txt_c_ticket_fee = (TextView) findViewById(R.id.txt_c_ticket_fee);
        txt_c_cinema_count = (TextView) findViewById(R.id.txt_c_cinema_count);

        img_c_movies_banner = (carbon.widget.ImageView) findViewById(R.id.img_c_movies_banner);
        img_c_movies_banner.setOnClickListener(this);
        img_loading = (ImageView) findViewById(R.id.img_loading);
        img_loading.setVisibility(View.GONE);

        scrollView = (ScrollView) findViewById(R.id.scrollView);

        list_view = (ListView) findViewById(R.id.list_view);
        try {
            JSONArray jsonArray = new JSONArray(moviesModel.cinema_type_with_time);
            AppDelegate.LogT("response => " + jsonArray.toString());
            ArrayList<MovieTime> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                MovieTime movieTime = new MovieTime();
                movieTime.cinema_type = jsonArray.getJSONObject(i).getString(Tags.cinema_type);
                movieTime.cinema_time = jsonArray.getJSONObject(i).getString(Tags.cinema_time);
                try {
                    movieTime.image = jsonArray.getJSONObject(i).getString(Tags.image);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

//                JSONArray array = jsonArray.getJSONObject(i).getJSONArray(Tags.cinema_time);
//                for (int j = 0; j < array.length(); j++) {
//                    movieTime.arrayTime.add(array.getString(j));
//                    AppDelegate.LogT("Time => " + j + ", " + array.getString(j));
//                }
                arrayList.add(movieTime);
            }

            txt_c_cinema_count.setText("Currently showing in " + arrayList.size() + " cinema");
            listAdapter = new MoviesDetailListAdapter(MoviesDetailsFragment.this, -1, arrayList, this);
            list_view.setAdapter(listAdapter);
            setListViewHeight(MoviesDetailsFragment.this, list_view, listAdapter);
            list_view.smoothScrollToPosition(0);
            list_view.setSelection(0);

        } catch (JSONException e) {
            AppDelegate.LogE(e);
        }

        findViewById(R.id.img_c_fb).setOnClickListener(this);
        findViewById(R.id.img_c_twitter).setOnClickListener(this);

        loginButton = (TwitterLoginButton) findViewById(R.id.login_button);
        loginButton.setCallback(new com.twitter.sdk.android.core.Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                AppDelegate.LogT("Twitter success called");
                if (OriginalPhoto != null) {
                    if (capturedFile == null)
                        writeImageFile(OriginalPhoto);
                    if (capturedFile == null) {
                        AppDelegate.showToast(MoviesDetailsFragment.this, "Failed to create image for share please try again later.");
                        return;
                    }
                    final TwitterSession session = TwitterCore.getInstance().getSessionManager()
                            .getActiveSession();
                    Card card = new Card.AppCardBuilder(MoviesDetailsFragment.this)
                            .imageUri(Uri.fromFile(capturedFile))
                            .googlePlayId(getPackageName())
                            .build();
                    Intent intent = new ComposerActivity.Builder(MoviesDetailsFragment.this)
                            .session(session)
                            .card(card)
                            .hashtags("#" + moviesModel.title, "#" + "Directed By: " + moviesModel.directed_by, "#" + "Staring: " + moviesModel.starring, "#" + "Genre: " + moviesModel.genre)
                            .createIntent();
                    startActivity(intent);
                } else {
                    AppDelegate.showToast(MoviesDetailsFragment.this, "For tweet you have to wait until image get loaded.");
                }
            }

            @Override
            public void failure(TwitterException exception) {
                AppDelegate.LogT("Twitter failure called => " + exception);
            }
        });

        youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_fragment);
        youTubePlayerFragment.initialize(AppDelegate.DEVELOPER_KEY, this);
    }

    private File capturedFile;
    private Bitmap OriginalPhoto;
    private boolean imageLoaded = false;

    public void writeImageFile(Bitmap OriginalPhoto) {
        FileOutputStream fOut = null;
        try {
            if (capturedFile == null)
                capturedFile = new File(getNewFile());
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }


    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            try {
                File capturedFile_3 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                        + ".png");
                if (capturedFile_3.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile_3.getAbsolutePath());
                    return capturedFile_3.getAbsolutePath();
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    private int item_position = 0;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                finish();
                break;
            case R.id.img_c_fb:
                openFacebook(moviesModel);
                break;

            case R.id.img_c_twitter:
                loginButton.performClick();
                break;

            case R.id.img_c_movies_banner:
                if (AppDelegate.haveNetworkConnection(MoviesDetailsFragment.this)) {
                    Intent intent = new Intent(MoviesDetailsFragment.this, LargeImageActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Tags.image_1, moviesModel.banner_image);
                    bundle.putInt(Tags.count, 1);
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
//                    AppDelegate.addFragment(MoviesDetailsFragment.this.getFragmentManager(), new NoInternetConnectionFragment(), 1);
                }
                break;

        }
    }


    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;

    public void openFacebook(final MoviesModel productModel) {
        FacebookSdk.sdkInitialize(this);
        shareDialog = new ShareDialog(this);
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(productModel);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(productModel);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(MoviesDetailsFragment.this);
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(productModel);
                                }
                            }
                        }
                    }
                });
    }

    private void shareFacebook(MoviesModel productModel) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            String string = productModel.title + "\n" + "Directed By: " + productModel.directed_by + "\n" + "Staring: " + productModel.starring + ", Genre: " + productModel.genre + "\n" + moviesModel.synopsis;
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(productModel.title)
//                    .setImageUrl(imageURI)
                    .setImageUrl(Uri.parse(productModel.banner_image_thumb))
                    .setContentDescription(string)
                    .setContentUrl(Uri.parse(productModel.trailer_link))
                    .build();
            shareDialog.show(linkContent);
        }
    }


    @Override
    public void setOnListItemClickListener(String name, int position) {
        AppDelegate.LogT("setOnListItemClickListener => name = " + name + ", pos = " + position);
        this.item_position = position;
        if (name.equalsIgnoreCase(Tags.LIST_ITEM_TRENDING)) {
            if (AppDelegate.haveNetworkConnection(MoviesDetailsFragment.this)) {
                Intent intent = new Intent(MoviesDetailsFragment.this, LargeImageActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Tags.image, moviesModel.banner_image);
                bundle.putInt(Tags.count, 1);
                intent.putExtras(bundle);
                startActivity(intent);
            } else {
//                AppDelegate.addFragment(MoviesDetailsFragment.this.getFragmentManager(), new NoInternetConnectionFragment(), 1);
            }
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.MOVIES_VIEW)) {
            parseDealsResponse(result);
        }
    }

    private void parseDealsResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                asyncExecuting = false;
            } else {
                moviesModel.total_movie_views = (AppDelegate.getIntValue(moviesModel.total_movie_views) + 1) + "";
                moviesModel.logged_user_view_status = "1";
                setValues();
                updateMovies(moviesModel);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void updateMovies(MoviesModel mModel) {
        String total_deal_views, logged_user_view_status;
        total_deal_views = mModel.total_movie_views;
        logged_user_view_status = mModel.logged_user_view_status;

        this.moviesModel = mModel;
        this.moviesModel.total_movie_views = total_deal_views;
        this.moviesModel.logged_user_view_status = logged_user_view_status;

        for (int i = 0; i < HomeFragment.moviesArray.size(); i++) {
            if (HomeFragment.moviesArray.get(i).id.equalsIgnoreCase(this.moviesModel.id)) {
                HomeFragment.moviesArray.remove(i);
                HomeFragment.moviesArray.add(i, this.moviesModel);
                AppDelegate.LogT("added at HomeFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < DealsMoviesFragment.moviesArray.size(); i++) {
            if (DealsMoviesFragment.moviesArray.get(i).id.equalsIgnoreCase(this.moviesModel.id)) {
                DealsMoviesFragment.moviesArray.remove(i);
                DealsMoviesFragment.moviesArray.add(i, this.moviesModel);
                AppDelegate.LogT("added at DealsMoviesFragment at position => " + i);
                break;
            }
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {
            AppDelegate.LogT("onInitializationSuccess called. => " + moviesModel.trailer_link + "\n" + moviesModel.trailer_link.substring(moviesModel.trailer_link.indexOf("v=") + 2));
            youTubePlayer.cueVideo(moviesModel.trailer_link.substring(moviesModel.trailer_link.indexOf("v=") + 2));
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        String errorMessage = String.format(getString(R.string.error_player), youTubeInitializationResult.toString());
        Toast.makeText(MoviesDetailsFragment.this, errorMessage, Toast.LENGTH_LONG).show();
    }
}
