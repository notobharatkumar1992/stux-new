package com.stux.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.stux.Adapters.MakeOfferAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 08/03/2016.
 */
public class MakeOfferFragment extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnListItemClickListener {

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel dataModel;

    private EditText et_currency;
    private TextView txt_c_offer_text, txt_c_continue;

    private ProductModel productModel;

    private ViewPager view_pager;
    private MakeOfferAdapter mPagerAdapter;
    public ArrayList<String> arrayString = new ArrayList<>();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.make_offer, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        productModel = getArguments().getParcelable(Tags.product);
        setHandler();
        initView(view);
    }

    private Runnable mRunnable;

    private void timeOutLoop() {
        mHandler.removeCallbacks(mRunnable);
        if (mRunnable == null)
            mRunnable = new Runnable() {
                @Override
                public void run() {

                    int value = view_pager.getCurrentItem();
                    if (value == mPagerAdapter.getCount() - 1) {
                        value = 0;
                    } else {
                        value++;
                    }
                    view_pager.setCurrentItem(value, true);
                    if (isAdded()) {
                        mHandler.postDelayed(this, 3000);
                    } else {
                        mHandler.removeCallbacks(this);
                    }
                }
            };
        mHandler.postDelayed(mRunnable, 3000);
    }

    private void callMakeOfferAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.buyer_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.seller_id, productModel.user_id + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.offer_price, et_currency.getText().toString() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.MAKE_OFFER,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Make Offer");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        et_currency = (EditText) view.findViewById(R.id.et_currency);
        et_currency.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_currency.setText("" + productModel.price);

        txt_c_offer_text = (TextView) view.findViewById(R.id.txt_c_offer_text);
        txt_c_continue = (TextView) view.findViewById(R.id.txt_c_continue);
        txt_c_continue.setOnClickListener(this);

        txt_c_offer_text.setText("Enter your offer for " + productModel.title);

        view_pager = (ViewPager) view.findViewById(R.id.view_pager);
        if (arrayString.size() == 0)
//            if (AppDelegate.isValidString(productModel.image_4_thumb)) {
//                arrayString.add(productModel.image_1_thumb);
//                arrayString.add(productModel.image_2_thumb);
//                arrayString.add(productModel.image_3_thumb);
//                arrayString.add(productModel.image_4_thumb);
//            } else if (AppDelegate.isValidString(productModel.image_3_thumb)) {
//                arrayString.add(productModel.image_1_thumb);
//                arrayString.add(productModel.image_2_thumb);
//                arrayString.add(productModel.image_3_thumb);
//            } else if (AppDelegate.isValidString(productModel.image_2_thumb)) {
//                arrayString.add(productModel.image_1_thumb);
//                arrayString.add(productModel.image_2_thumb);
//            } else if (AppDelegate.isValidString(productModel.image_1_thumb)) {
            arrayString.add(productModel.image_1);
//            }

        mPagerAdapter = new MakeOfferAdapter(getActivity(), arrayString, this);
        view_pager.setAdapter(mPagerAdapter);
        timeOutLoop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_continue:
                callMakeOfferAsync();
                break;

            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.MAKE_OFFER)) {
            parseMakeOfferResult(result);
        }
    }

    private void parseMakeOfferResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                getFragmentManager().popBackStack();
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server Error");
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
    }
}
