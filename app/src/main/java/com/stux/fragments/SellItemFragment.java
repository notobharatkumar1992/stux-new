package com.stux.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;
import com.stux.Adapters.SpinnerArrayStringAdapter;
import com.stux.Adapters.SpinnerProductCategoryAdapter;
import com.stux.AppDelegate;
import com.stux.Async.LocationAddress;
import com.stux.Async.PostAsync;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductCategoryModel;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/18/2016.
 */
public class SellItemFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse, com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnPictureResult {

    private LinearLayout ll_c_img_layout;
    private ImageView img_c_pic_0, img_c_pic_1, img_c_pic_2, img_c_pic_3, img_c_check;
    private EditText et_name, et_item_description, et_currency, et_location;

    public ArrayList<String> arrayStringProductCondition = new ArrayList<>();
    private SpinnerArrayStringAdapter adapterProductCondition;

    public ArrayList<ProductCategoryModel> arrayProductCategory = new ArrayList<>();
    private SpinnerProductCategoryAdapter adapterCategory;

    private Spinner spn_category, spn_product_type;

    private int selected_type = 0, selected_product_category = 0;

    private Prefs prefs;
    private UserDataModel dataModel;
    private InstitutionModel institutionModel;

    private ShareDialog shareDialog;
    public static CallbackManager callbackManager;
    private boolean isCalledOnce = false;

    private Handler mHandler;
    private int selected_image = 0;
    private Bitmap bitmap_pic_0, bitmap_pic_1, bitmap_pic_2, bitmap_pic_3;
    public static File capturedFile_0, capturedFile_1, capturedFile_2, capturedFile_3;
    public static OnPictureResult onPictureResult;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mCurrentLocation;
    private CountDownTimer countDownTimer;
    private String city_name = "";

    private ProductModel productModel;
    private int fromPage = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sell_an_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        facebookSDKInitialize();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        institutionModel = prefs.getInstitutionModel();
        shareDialog = new ShareDialog(this);  // intialize facebook shareDialog.
        onPictureResult = this;
        initView(view);
        setHandler();
        callGetProductTypeAsync();
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
//        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
//            buildAlertMessageNoGps();
//        }
        showGPSalert();
        if (getArguments() != null && getArguments().getParcelable(Tags.product) != null) {
            ((TextView) view.findViewById(R.id.txt_c_right)).setText("Update");
            productModel = getArguments().getParcelable(Tags.product);
            fromPage = getArguments().getInt(Tags.FROM);
            setValues();
        }
    }

    private void showGPSalert() {
        try {
            if (mGoogleApiClient == null) {
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .addApi(LocationServices.API)
                        .addConnectionCallbacks(this)
                        .addOnConnectionFailedListener(this).build();
                mGoogleApiClient.connect();
            }
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    AppDelegate.LogT("state => " + state + ", status = " + status);
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                if (status != null)
                                    status.startResolutionForResult(getActivity(), 1000);
                                else
                                    AppDelegate.showToast(getActivity(), "Something wrong with your GPS please try again later");
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (Exception e) {
                                AppDelegate.LogE(e);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }

            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        getFragmentManager().popBackStack();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    // Initialize the facebook sdk and then callback manager will handle the login responses.

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callbackManager = null;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGoogleApiClient.connect();
    }

    private void requestForLocationUpdate() {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(100); // Update location every second
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        AppDelegate.LogT("request for Location update = " + mCurrentLocation);
        if (!findAddressCalled && mCurrentLocation != null) {
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    public TextView txt_c_right;

    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Sell an Item");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.filter);
        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);
        txt_c_right = ((TextView) view.findViewById(R.id.txt_c_right));
        txt_c_right.setText("Publish");
        txt_c_right.setVisibility(View.VISIBLE);
        txt_c_right.setOnClickListener(this);

        ll_c_img_layout = (LinearLayout) view.findViewById(R.id.ll_c_img_layout);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) ll_c_img_layout.getLayoutParams();
        layoutParams.height = (AppDelegate.getDeviceWith(getActivity()) - AppDelegate.dpToPix(getActivity(), 45)) / 4;
        ll_c_img_layout.setLayoutParams(layoutParams);
        ll_c_img_layout.invalidate();

        img_c_pic_0 = (ImageView) view.findViewById(R.id.img_c_pic_0);
        img_c_pic_0.setOnClickListener(this);
        img_c_pic_1 = (ImageView) view.findViewById(R.id.img_c_pic_1);
        img_c_pic_1.setOnClickListener(this);
        img_c_pic_2 = (ImageView) view.findViewById(R.id.img_c_pic_2);
        img_c_pic_2.setOnClickListener(this);
        img_c_pic_3 = (ImageView) view.findViewById(R.id.img_c_pic_3);
        img_c_pic_3.setOnClickListener(this);
        img_c_check = (ImageView) view.findViewById(R.id.img_c_check);
        img_c_check.setOnClickListener(this);
        img_c_check.setSelected(false);

        et_name = (EditText) view.findViewById(R.id.et_name);
        et_name.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_item_description = (EditText) view.findViewById(R.id.et_item_description);
        et_item_description.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_currency = (EditText) view.findViewById(R.id.et_currency);
        et_currency.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_location = (EditText) view.findViewById(R.id.et_location);
        et_location.setTypeface(Typeface.createFromAsset(getActivity().getAssets(), getString(R.string.font_roman)));
        et_location.setEnabled(false);

        if (arrayStringProductCondition.size() == 0) {
            arrayStringProductCondition.add("Item Condition");
            arrayStringProductCondition.add("New");
            arrayStringProductCondition.add("Almost New");
            arrayStringProductCondition.add("Used");
        }

        adapterProductCondition = new SpinnerArrayStringAdapter(getActivity(), arrayStringProductCondition);
        spn_product_type = (Spinner) view.findViewById(R.id.spn_product_type);
        spn_product_type.setAdapter(adapterProductCondition);
        spn_product_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_type = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        if (arrayProductCategory.size() == 0) {
            arrayProductCategory.add(new ProductCategoryModel("Select a Category"));
        }

        spn_category = (Spinner) view.findViewById(R.id.spn_category);
        adapterCategory = new SpinnerProductCategoryAdapter(getActivity(), arrayProductCategory);
        spn_category.setAdapter(adapterCategory);
        spn_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_product_category = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    public void openFacebook(final ProductModel productModel) {
        txt_c_right.setEnabled(false);
        txt_c_right.setOnClickListener(null);
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "user_friends", "user_birthday", "email"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        AppDelegate.LogFB("onSuccess = " + loginResult.getAccessToken());
                        shareFacebook(productModel);
                    }

                    @Override
                    public void onCancel() {
                        AppDelegate.LogFB("login cancel");
                        if (AccessToken.getCurrentAccessToken() != null)
                            LoginManager.getInstance().logOut();
                        if (!isCalledOnce) {
                            isCalledOnce = true;
                            openFacebook(productModel);
                        }
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        AppDelegate.LogFB("login error = " + exception.getMessage());
                        if (exception.getMessage().contains("CONNECTION_FAILURE")) {
                            AppDelegate.hideProgressDialog(getActivity());
                        } else if (exception instanceof FacebookAuthorizationException) {
                            if (AccessToken.getCurrentAccessToken() != null) {
                                LoginManager.getInstance().logOut();
                                if (!isCalledOnce) {
                                    isCalledOnce = true;
                                    openFacebook(productModel);
                                }
                            }
                        }
                    }
                });
    }

    private void setHandler() {
        try {
            mHandler = new Handler() {
                @Override
                public void dispatchMessage(Message msg) {
                    super.dispatchMessage(msg);
                    if (msg.what == 10) {
                        AppDelegate.showProgressDialog(getActivity());
                    } else if (msg.what == 11) {
                        AppDelegate.hideProgressDialog(getActivity());
                    } else if (msg.what == 1) {
                        adapterCategory.notifyDataSetChanged();
                        spn_category.invalidate();
                    } else if (msg.what == 2) {
                        setResultFromGeoCoderApi(msg.getData());
                    }
                }
            };
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void setResultFromGeoCoderApi(Bundle bundle) {
        mHandler.sendEmptyMessage(11);
        if (bundle.getString(Tags.PLACE_NAME) != null) {
            city_name = bundle.getString(Tags.PLACE_ADD);
            et_location.setText(city_name);
        } else {
            et_location.setEnabled(true);
            AppDelegate.showToast(getActivity(), "Location not available or something went wrong with server, Please try again later.");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_pic_0:
                selected_image = 0;
                showImageSelectorList();
                break;

            case R.id.img_c_pic_1:
                if (fromPage != 0 && !AppDelegate.isValidString(productModel.image_1)) {
                    AppDelegate.LogT("fromPage => " + fromPage + ", image 1 => " + productModel.image_1);
                    AppDelegate.showAlert(getActivity(), "Please select 1st image.");
                    return;
                } else if (fromPage == 0 && capturedFile_0 == null) {
                    AppDelegate.showAlert(getActivity(), "Please select 1st image.");
                    return;
                }
                selected_image = 1;
                showImageSelectorList();
                break;

            case R.id.img_c_pic_2:
                if (fromPage != 0 && !AppDelegate.isValidString(productModel.image_2)) {
                    AppDelegate.LogT("fromPage => " + fromPage + ", image 2 => " + productModel.image_2);
                    AppDelegate.showAlert(getActivity(), "Please select 2nd image.");
                    return;
                } else if (fromPage == 0 && capturedFile_1 == null) {
                    AppDelegate.showAlert(getActivity(), "Please select 2nd image.");
                    return;
                }
                selected_image = 2;
                showImageSelectorList();
                break;

            case R.id.img_c_pic_3:
                if (fromPage != 0 && !AppDelegate.isValidString(productModel.image_3)) {
                    AppDelegate.LogT("fromPage => " + fromPage + ", image 3 => " + productModel.image_3);
                    AppDelegate.showAlert(getActivity(), "Please select 3rd image.");
                    return;
                } else if (fromPage == 0 && capturedFile_2 == null) {
                    AppDelegate.showAlert(getActivity(), "Please select 3rd image.");
                    return;
                }
                selected_image = 3;
                showImageSelectorList();
                break;

            case R.id.img_c_check:
                img_c_check.setSelected(img_c_check.isSelected() ? false : true);
                AppDelegate.LogT("img_c_check => " + img_c_check.isSelected());
                break;

            case R.id.txt_c_right:
                if (img_c_check.isSelected()) {
                    callSellItemAsync();
                } else {
                    showAlertDialog();
                }
                break;

            case R.id.img_c_left:
                ((MainActivity) getActivity()).toggleSlider();
                break;
        }
    }

    private void showAlertDialog() {
        try {
            AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(false);
            mAlert.setMessage("Sharing on Facebook leads to faster sale! Do you wish to link STUX to your Facebook now #Stuxmobile");
            mAlert.setPositiveButton(
                    "YES",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            img_c_check.setSelected(true);
                            callSellItemAsync();
                            dialog.dismiss();
                        }
                    });

            mAlert.setNegativeButton(
                    "NO",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callSellItemAsync();
                            dialog.dismiss();
                        }
                    });
            mAlert.show();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }


    private void callGetProductTypeAsync() {
        if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_PRODUCT_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }


    private void setValues() {
        et_name.setText(productModel.title);
        et_item_description.setText(productModel.description);
        et_currency.setText(productModel.price);
        spn_product_type.setSelection(Integer.parseInt(productModel.item_condition));
        if (AppDelegate.isValidString(productModel.image_1_thumb))
            Picasso.with(getActivity()).load(productModel.image_1_thumb).into(img_c_pic_0);
        if (AppDelegate.isValidString(productModel.image_2_thumb))
            Picasso.with(getActivity()).load(productModel.image_2_thumb).into(img_c_pic_1);
        if (AppDelegate.isValidString(productModel.image_3_thumb))
            Picasso.with(getActivity()).load(productModel.image_3_thumb).into(img_c_pic_2);
        if (AppDelegate.isValidString(productModel.image_4_thumb))
            Picasso.with(getActivity()).load(productModel.image_4_thumb).into(img_c_pic_3);
        mCurrentLocation = new Location("");
        mCurrentLocation.setLatitude(Double.parseDouble(productModel.latitude));
        mCurrentLocation.setLongitude(Double.parseDouble(productModel.longitude));
//        et_location.setText(productModel.);
        setSelectedCategoryId();
    }

    private void setSelectedCategoryId() {
        if (arrayProductCategory.size() > 0 && productModel != null && AppDelegate.isValidString(productModel.cat_id)) {
            for (int i = 0; i < arrayProductCategory.size(); i++) {
                if (AppDelegate.isValidString(arrayProductCategory.get(i).id) && arrayProductCategory.get(i).id.equalsIgnoreCase(productModel.cat_id)) {
                    spn_category.setSelection(i);
                }
            }
        }
    }

    private void callSellItemAsync() {
        if (fromPage == 0 && capturedFile_0 == null && capturedFile_1 == null && capturedFile_2 == null && capturedFile_3 == null) {
            AppDelegate.showAlert(getActivity(), "Please select single image at least.");
        } else if (et_name.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter title name.");
        } else if (et_item_description.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter description.");
        } else if (selected_product_category == 0) {
            AppDelegate.showAlert(getActivity(), "Please select product category.");
        } else if (spn_product_type.getSelectedItemPosition() == 0) {
            AppDelegate.showAlert(getActivity(), "Please select product condition.");
        } else if (mCurrentLocation == null) {
            AppDelegate.showAlert(getActivity(), "Please make sure your GPS in working and try again later.");
        } else if (et_location.length() == 0) {
            AppDelegate.showAlert(getActivity(), "Please enter location");
        } else if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.cat_id, arrayProductCategory.get(selected_product_category).id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.title, et_name.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.description, et_item_description.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.price, et_currency.getText().toString());
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.item_condition, spn_product_type.getSelectedItemPosition() + "");
            if (capturedFile_0 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_1, capturedFile_0.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
            if (capturedFile_1 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_2, capturedFile_1.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
            if (capturedFile_2 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_3, capturedFile_2.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }
            if (capturedFile_3 != null) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image_4, capturedFile_3.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            }

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, mCurrentLocation.getLatitude() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, mCurrentLocation.getLongitude() + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_name, et_location.getText().toString() + "");

            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, "1");
            PostAsync mPostAsyncObj;
            if (fromPage > 0) {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, productModel.id);
                mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.EDIT_PRODUCT,
                        mPostArrayList, null);
            } else {
                mPostAsyncObj = new PostAsync(getActivity(), this, ServerRequestConstants.CREATE_PRODUCT,
                        mPostArrayList, null);
            }
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }

    private void shareFacebook(ProductModel productModel) {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle(et_name.getText().toString())
//                    .setImageUrl(imageURI)
                    .setImageUrl(Uri.parse(productModel.image_1))
                    .setContentDescription(et_item_description.getText().toString())
                    .setContentUrl(Uri.parse(productModel.image_1))
                    .build();
            shareDialog.show(linkContent);
//            getFragmentManager().popBackStack();
            AppDelegate.showFragmentAnimationOppose(getActivity().getSupportFragmentManager(), new HomeFragment());
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CREATE_PRODUCT)) {
            mHandler.sendEmptyMessage(11);
            parseProductItemResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PRODUCT_CATEGORY)) {
            mHandler.sendEmptyMessage(11);
            parseProductCategoryResult(result);
            setSelectedCategoryId();
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PRODUCT)) {
            mHandler.sendEmptyMessage(11);
            parseEditProductItemResult(result);
        }
    }

    private void parseEditProductItemResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);

                productModel = new ProductModel();
                productModel.id = object.getString(Tags.id);
                productModel.cat_id = object.getString(Tags.cat_id);
                productModel.title = object.getString(Tags.title);
                productModel.description = object.getString(Tags.description);
                productModel.price = object.getString(Tags.price);
                try {
                    productModel.price = productModel.price.replaceAll(",", "");
                    productModel.price = AppDelegate.getPriceFormatted(Integer.parseInt(productModel.price));
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                productModel.item_condition = object.getString(Tags.item_condition);

                productModel.image_1 = JSONParser.getString(object, Tags.image_1);
                productModel.image_2 = JSONParser.getString(object, Tags.image_2);
                productModel.image_3 = JSONParser.getString(object, Tags.image_3);
                productModel.image_4 = JSONParser.getString(object, Tags.image_4);

                productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
                productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

                productModel.sold_status = JSONParser.getString(object, Tags.sold_status);
                productModel.status = JSONParser.getString(object, Tags.status);
                productModel.created = JSONParser.getString(object, Tags.created);
                productModel.modified = JSONParser.getString(object, Tags.modified);
                productModel.total_product_views = JSONParser.getString(object, Tags.total_product_views);
                productModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

                try {
                    productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                    productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
                    productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                    productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }

                if (object.has(Tags.product_category) && object.optJSONObject(Tags.product_category) != null) {
                    JSONObject productObject = object.getJSONObject(Tags.product_category);
                    productModel.pc_id = productObject.getString(Tags.id);
                    productModel.pc_title = productObject.getString(Tags.cat_name);
                    productModel.pc_status = productObject.getString(Tags.status);
                }

                productModel.latitude = JSONParser.getString(object, Tags.latitude);
                productModel.longitude = JSONParser.getString(object, Tags.longitude);

                productModel.user_id = dataModel.userId;
                productModel.user_first_name = dataModel.first_name;
                productModel.user_last_name = dataModel.last_name;
                productModel.user_email = dataModel.email;
                productModel.user_role = dataModel.role;
                productModel.user_image = dataModel.image;
                productModel.user_social_id = dataModel.social_id;
                productModel.user_gcm_token = dataModel.gcm_token;

                productModel.user_institution_state_id = institutionModel.institution_state_id;
                productModel.user_institution_id = institutionModel.institution_name_id;
                productModel.user_institute_name = institutionModel.institution_name;
                productModel.user_department_name = institutionModel.department_name;

                updateProduct(productModel);
                if (img_c_check.isSelected()) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                    openFacebook(productModel);
                } else {
                    AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
//                    getFragmentManager().popBackStack();
                    AppDelegate.showFragmentAnimationOppose(getActivity().getSupportFragmentManager(), new HomeFragment());
                }

            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showAlert(getActivity(), "Response not proper.");
        }
    }

    private void addProductToList(ProductModel productModel) {
        if (HomeFragment.productArray.size() > 0) {
            HomeFragment.productArray.add(0, productModel);
        }
        if (MyProfileFragment.productArray.size() > 0) {
            MyProfileFragment.productArray.add(0, productModel);
        }
        if (MyProductListFragment.productArray.size() > 0) {
            MyProductListFragment.productArray.add(0, productModel);
        }
        if (RefineProductFragment.productArray.size() > 0) {
            RefineProductFragment.productArray.add(0, productModel);
        }
    }

    public static void updateProduct(ProductModel productModel) {
        for (int i = 0; i < HomeFragment.productArray.size(); i++) {
            if (HomeFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                HomeFragment.productArray.remove(i);
                HomeFragment.productArray.add(i, productModel);
                AppDelegate.LogT("Added at HomeFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < MyProfileFragment.productArray.size(); i++) {
            if (MyProfileFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                MyProfileFragment.productArray.remove(i);
                MyProfileFragment.productArray.add(i, productModel);
                AppDelegate.LogT("added at MyProfileFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < MyProductListFragment.productArray.size(); i++) {
            if (MyProductListFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                MyProductListFragment.productArray.remove(i);
                MyProductListFragment.productArray.add(i, productModel);
                AppDelegate.LogT("added at MyProductListFragment at position => " + i);
                break;
            }
        }
        for (int i = 0; i < RefineProductFragment.productArray.size(); i++) {
            if (RefineProductFragment.productArray.get(i).id.equalsIgnoreCase(productModel.id)) {
                RefineProductFragment.productArray.remove(i);
                RefineProductFragment.productArray.add(i, productModel);
                AppDelegate.LogT("added at RefineProductFragment at position => " + i);
                break;
            }
        }
    }

    private void parseProductCategoryResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                    productCategoryModel.id = jsonObject.getString(Tags.id);
                    productCategoryModel.cat_name = jsonObject.getString(Tags.cat_name);
                    productCategoryModel.status = jsonObject.getString(Tags.id);
                    arrayProductCategory.add(productCategoryModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server Error");
        }
    }

    private void parseProductItemResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);

                productModel = new ProductModel();

                productModel.id = object.getString(Tags.id);
                productModel.cat_id = object.getString(Tags.cat_id);
                productModel.title = object.getString(Tags.title);
                productModel.description = object.getString(Tags.description);
                productModel.price = object.getString(Tags.price);
                try {
                    productModel.price = productModel.price.replaceAll(",", "");
                    productModel.price = AppDelegate.getPriceFormatted(Integer.parseInt(productModel.price));
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                productModel.item_condition = object.getString(Tags.item_condition);

                productModel.image_1 = JSONParser.getString(object, Tags.image_1);
                productModel.image_2 = JSONParser.getString(object, Tags.image_2);
                productModel.image_3 = JSONParser.getString(object, Tags.image_3);
                productModel.image_4 = JSONParser.getString(object, Tags.image_4);

                productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
                productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

                productModel.sold_status = JSONParser.getString(object, Tags.sold_status);
                productModel.status = JSONParser.getString(object, Tags.status);
                productModel.created = JSONParser.getString(object, Tags.created);
                productModel.modified = JSONParser.getString(object, Tags.modified);
                productModel.total_product_views = JSONParser.getString(object, Tags.total_product_views);
                productModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

                if (object.has(Tags.product_category) && object.optJSONObject(Tags.product_category) != null) {
                    JSONObject productObject = object.getJSONObject(Tags.product_category);
                    productModel.pc_id = productObject.getString(Tags.id);
                    productModel.pc_title = productObject.getString(Tags.cat_name);
                    productModel.pc_status = productObject.getString(Tags.status);
                }

                productModel.latitude = JSONParser.getString(object, Tags.latitude);
                productModel.longitude = JSONParser.getString(object, Tags.longitude);

                productModel.user_id = dataModel.userId;
                productModel.user_first_name = dataModel.first_name;
                productModel.user_last_name = dataModel.last_name;
                productModel.user_email = dataModel.email;
                productModel.user_role = dataModel.role;
                productModel.user_image = dataModel.image;
                productModel.user_social_id = dataModel.social_id;
                productModel.user_gcm_token = dataModel.gcm_token;

                productModel.user_institution_state_id = institutionModel.institution_state_id;
                productModel.user_institution_id = institutionModel.institution_name_id;
                productModel.user_institute_name = institutionModel.institution_name;
                productModel.user_department_name = institutionModel.department_name;
                addProductToList(productModel);

                if (img_c_check.isSelected()) {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                    openFacebook(productModel);
                } else {
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                    AppDelegate.showFragmentAnimationOppose(getActivity().getSupportFragmentManager(), new HomeFragment());
                }

            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server Error");
        }
    }

    //------------image selector-----------------//

    //    private DisplayImageOptions options;
    public static Uri imageURI = null;
    public static String str_file_path = "";
    public static Uri imageUri;
    public static Uri cropimageUri;
    private Bitmap OriginalPhoto;

    Dialog dialog;

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
//                        startActivity(new Intent(getActivity(), VideoRecordingPreLollipopActivity.class));
                        new OpenCamera().execute();
                        break;
                    case 1:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 2:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    public String writeImageFile() {
        FileOutputStream fOut = null;
        String filePath = "";
        try {
            switch (selected_image) {
                case 0:
                    if (capturedFile_0 == null)
                        capturedFile_0 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_0);
                    filePath = capturedFile_0.getAbsolutePath();
                    break;
                case 1:
                    if (capturedFile_1 == null)
                        capturedFile_1 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_1);
                    filePath = capturedFile_1.getAbsolutePath();
                    break;
                case 2:
                    if (capturedFile_2 == null)
                        capturedFile_2 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_2);
                    filePath = capturedFile_2.getAbsolutePath();
                    break;
                case 3:
                    if (capturedFile_3 == null)
                        capturedFile_3 = new File(getNewFile());
                    fOut = new FileOutputStream(capturedFile_3);
                    filePath = capturedFile_3.getAbsolutePath();
                    break;
            }
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        return filePath;
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        getActivity().startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri);
                OriginalPhoto = rotateImageIfRequired(OriginalPhoto, picUri);
//                OriginalPhoto = AppDelegate.getResizedBitmap(OriginalPhoto, 300);
                getBitmapAndWriteFile(getActivity(), writeImageFile());
                switch (selected_image) {
                    case 0:
                        bitmap_pic_0 = OriginalPhoto;
                        img_c_pic_0.setImageBitmap(bitmap_pic_0);
                        break;
                    case 1:
                        bitmap_pic_1 = OriginalPhoto;
                        img_c_pic_1.setImageBitmap(bitmap_pic_1);
                        break;
                    case 2:
                        bitmap_pic_2 = OriginalPhoto;
                        img_c_pic_2.setImageBitmap(bitmap_pic_2);
                        break;
                    case 3:
                        bitmap_pic_3 = OriginalPhoto;
                        img_c_pic_3.setImageBitmap(bitmap_pic_3);
                        break;
                }
            } catch (OutOfMemoryError e) {
                AppDelegate.LogE(e);
                AppDelegate.showToast(getActivity(), "Device is too slow or get failed while capturing image, please try again later.");
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    public static Bitmap getBitmapAndWriteFile(Context mContext, String path) {
        Uri uri = Uri.fromFile(new File(path));
        InputStream in = null;
        try {
            final int IMAGE_MAX_SIZE = 150000; // 0.15MP
            in = mContext.getContentResolver().openInputStream(uri);

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(in, null, o);
            in.close();
            int scale = 1;
            while ((o.outWidth * o.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }
            Bitmap b = null;
            in = mContext.getContentResolver().openInputStream(uri);
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                o = new BitmapFactory.Options();
                o.inSampleSize = scale;
                b = BitmapFactory.decodeStream(in, null, o);
                // resize to desired dimensions
                int height = b.getHeight();
                int width = b.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(b, (int) x, (int) y, true);
                b.recycle();
                b = scaledBitmap;

                System.gc();
            } else {
                b = BitmapFactory.decodeStream(in);
            }
            in.close();
            Log.d("test", "getBitmapAndWriteFile => File saved successfully : ");

            storeImage(path, b);
            return b;
        } catch (IOException e) {
            AppDelegate.LogE(e);
            return null;
        }
    }

    public static void storeImage(String filePath, Bitmap image) {
        File pictureFile = new File(filePath);
        if (pictureFile == null) {
            Log.d("test", "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            Log.d("test", "storeImage => File saved successfully : " + filePath);
        } catch (FileNotFoundException e) {
            Log.d("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("test", "Error accessing file: " + e.getMessage());
        }
    }

    private static Bitmap rotateImageIfRequired(Bitmap img, Uri selectedImage) {
        try {
            ExifInterface ei = null;
            try {
                ei = new ExifInterface(selectedImage.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    return rotateImage(img, 90);
                case ExifInterface.ORIENTATION_ROTATE_180:
                    return rotateImage(img, 180);
                case ExifInterface.ORIENTATION_ROTATE_270:
                    return rotateImage(img, 270);
                default:
                    return img;
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return img;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onPictureResult = null;
    }

    boolean findAddressCalled = false;


    @Override
    public void onLocationChanged(Location location) {
        if (location != null)
            mCurrentLocation = location;
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        if (!findAddressCalled) {
            setLatLngAndFindAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 100);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        AppDelegate.LogT("onConnected called");
        requestForLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        AppDelegate.LogT("onConnectionSuspended called");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        AppDelegate.LogT("onConnectionFailed called");
    }

    public void setLatLngAndFindAddress(final LatLng latLng, long countDownTime) {
        AppDelegate.LogT("setLatLngAndFindAddress called");
        findAddressCalled = true;
        countDownTimer = new CountDownTimer(countDownTime, countDownTime) {

            @Override
            public void onTick(long millisUntilFinished) {
                AppDelegate.LogT("timer = " + millisUntilFinished);
            }

            @Override
            public void onFinish() {
                LatLng arg0 = AppDelegate.getRoundedLatLng(latLng);
                mHandler.sendEmptyMessage(10);
                LocationAddress.getAddressFromLocation(
                        arg0.latitude, arg0.longitude,
                        getActivity(), mHandler);
            }
        };
        countDownTimer.start();
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//            String str_file_path = getNewFile();
//            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
//                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
//                return null;
//            }
//            switch (selected_image) {
//                case 0:
//                    imageURI = Uri.fromFile(capturedFile_0);
//                    break;
//
//                case 1:
//                    imageURI = Uri.fromFile(capturedFile_1);
//                    break;
//
//                case 2:
//                    imageURI = Uri.fromFile(capturedFile_2);
//                    break;
//
//                case 3:
//                    imageURI = Uri.fromFile(capturedFile_3);
//                    break;
//            }
//
//            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            getActivity().startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            try {
                switch (selected_image) {
                    case 0:
                        capturedFile_0 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_0.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_0.getAbsolutePath());
                            return capturedFile_0.getAbsolutePath();
                        }
                        break;
                    case 1:
                        capturedFile_1 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_1.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_1.getAbsolutePath());
                            return capturedFile_1.getAbsolutePath();
                        }
                        break;
                    case 2:
                        capturedFile_2 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_2.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_2.getAbsolutePath());
                            return capturedFile_2.getAbsolutePath();
                        }
                        break;
                    case 3:
                        capturedFile_3 = new File(directoryFile, "Image_" + System.currentTimeMillis()
                                + ".png");
                        if (capturedFile_3.createNewFile()) {
                            AppDelegate.LogT("File created = " + capturedFile_3.getAbsolutePath());
                            return capturedFile_3.getAbsolutePath();
                        }
                        break;
                }
            } catch (Exception e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }
}
