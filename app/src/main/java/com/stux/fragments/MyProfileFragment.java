package com.stux.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.GCMClientManager;
import com.stux.Models.InstitutionModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.Utils.SpacesItemDecoration;
import com.stux.activities.LargeImageActivity;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnListItemClickListenerWithHeight;
import com.stux.interfaces.OnPictureResult;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.LinearLayout;
import carbon.widget.RelativeLayout;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.ProductRecyclerViewAdapter;


/**
 * Created by Bharat on 07/06/2016.
 */
public class MyProfileFragment extends Fragment implements View.OnClickListener, OnListItemClickListener, OnReciveServerResponse, OnListItemClickListenerWithHeight, OnPictureResult {

    public static File capturedFile;
    public static Uri imageURI = null;
    public static OnPictureResult onPictureResult;

    private Prefs prefs;
    private UserDataModel dataModel;
    private InstitutionModel institutionModel;
    private CircleImageView cimg_user;
    private TextView txt_c_listings, txt_c_followers, txt_c_following, txt_c_university_name, txt_c_time;
    private LinearLayout ll_c_following;
    private ImageView img_c_chat;
    private android.widget.ImageView img_loading;
    private RelativeLayout rl_c_sold_listings, rl_c_reviews, rl_c_following;
//    private ProgressBar progressbar;

    //  private CampusListAdapter mCampusListAdapter;
//  private StaggeredGridView stgv;
    private RecyclerView recyclerView;
    private ProductRecyclerViewAdapter rcAdapter;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;

    public static int campusCounter = 1, campusTotalPage = -1;
    //    private TextView txt_c_no_list;
    private Handler mHandler;
    public static ArrayList<ProductModel> productArray = new ArrayList<>();
    private boolean campusAsyncExcecuting = false;
    private Bitmap OriginalPhoto;

    private int visibleThreshold = 10;
    private int lastVisibleItem, totalItemCount;

    private SwipyRefreshLayout swipyrefreshlayout;

    private boolean isRefreshOnce = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        institutionModel = prefs.getInstitutionModel();
        onPictureResult = this;
        initView(view);
        setValues();
        setHandler();
        if (!isRefreshOnce) {
            isRefreshOnce = true;
            updateUserData(dataModel.userId);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
//                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 13) {
//                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("gridViewCampusList notified ");
//                    txt_c_no_list.setVisibility(productArray.size() > 0 ? View.GONE : View.VISIBLE);
//                    txt_c_no_list.setText("No product available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                } else if (msg.what == 3) {
                    ((MainActivity) getActivity()).updateUserDetail();
                    setValues();
                }
            }
        };
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (productArray.size() == 0) {
            campusCounter = 1;
            callMyProductListAsync();
        } else {
            mHandler.sendEmptyMessage(2);
        }
    }

    private void updateUserData(String userId) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.profile_id, userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_USER,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setValues() {
        dataModel = prefs.getUserdata();
        img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        if (AppDelegate.isValidString(dataModel.image))
            Picasso.with(getActivity()).load(dataModel.image).into(cimg_user, new Callback() {
                @Override
                public void onSuccess() {
                    img_loading.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    img_loading.setVisibility(View.GONE);
                }
            });
        txt_c_university_name.setText(institutionModel.institution_name + " - " + institutionModel.department_name);
        if (AppDelegate.isValidString(dataModel.created)) {
            try {
                txt_c_time.setText("From " + new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("yyyy-MM-dd").parse(dataModel.created)));
            } catch (Exception e) {
                AppDelegate.LogE(e);
                txt_c_time.setText("From " + dataModel.created);
            }
        } else txt_c_time.setVisibility(View.GONE);

        txt_c_listings.setText(dataModel.total_product + "");
        txt_c_followers.setText(dataModel.followers_count + "");
        txt_c_following.setText(dataModel.following_count + "");
    }

    private void initView(View view) {
        if (dataModel != null && AppDelegate.isValidString(dataModel.first_name))
            ((TextView) view.findViewById(R.id.txt_c_header)).setText(dataModel.first_name + " " + dataModel.last_name);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        ((ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        ((ImageView) view.findViewById(R.id.img_c_left)).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_right).setOnClickListener(this);
        ((ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.edit);
        ((ImageView) view.findViewById(R.id.img_c_right)).setVisibility(View.VISIBLE);

        view.findViewById(R.id.img_check).setOnClickListener(this);
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);

        txt_c_listings = (TextView) view.findViewById(R.id.txt_c_listings);
        txt_c_followers = (TextView) view.findViewById(R.id.txt_c_followers);
        txt_c_following = (TextView) view.findViewById(R.id.txt_c_following);
        txt_c_university_name = (TextView) view.findViewById(R.id.txt_c_university_name);
        txt_c_time = (TextView) view.findViewById(R.id.txt_c_time);

        img_c_chat = (ImageView) view.findViewById(R.id.img_c_chat);
        img_c_chat.setOnClickListener(this);

        img_loading = (android.widget.ImageView) view.findViewById(R.id.img_loading);

        ll_c_following = (LinearLayout) view.findViewById(R.id.ll_c_following);
        ll_c_following.setOnClickListener(this);

        rl_c_sold_listings = (RelativeLayout) view.findViewById(R.id.rl_c_sold_listings);
        rl_c_sold_listings.setOnClickListener(this);
        rl_c_reviews = (RelativeLayout) view.findViewById(R.id.rl_c_reviews);
        rl_c_reviews.setOnClickListener(this);
        rl_c_following = (RelativeLayout) view.findViewById(R.id.rl_c_following);
        rl_c_following.setOnClickListener(this);
        rl_c_following.setVisibility(View.GONE);

        view.findViewById(R.id.rl_user_img).setOnClickListener(this);
        view.findViewById(R.id.ll_c_listing_count).setOnClickListener(this);
        view.findViewById(R.id.ll_c_followers_count).setOnClickListener(this);
        view.findViewById(R.id.ll_c_following_count).setOnClickListener(this);
//        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
//        txt_c_no_list.setVisibility(View.GONE);
//        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
//
//        mCampusListAdapter = new CampusListAdapter(getActivity(), R.id.txt_line1, productArray, this, this);
//        stgv = (StaggeredGridView) view.findViewById(R.id.stgv);
//        stgv.setItemMargin(20);
//        stgv.setPadding(22, 0, 22, 0);
//        stgv.setAdapter(mCampusListAdapter);
//
//        stgv.setOnLoadmoreListener(new StaggeredGridView.OnLoadmoreListener() {
//            @Override
//            public void onLoadmore() {
//                if (campusTotalPage != 0 && !campusAsyncExcecuting) {
//                    mHandler.sendEmptyMessage(2);
//                    callMyProductListAsync();
//                    campusAsyncExcecuting = true;
//                }
//            }
//        });
//

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setHasFixedSize(true);

        gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        gaggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(gaggeredGridLayoutManager);
        recyclerView.addItemDecoration(new SpacesItemDecoration(AppDelegate.dpToPix(getActivity(), 5)));
        rcAdapter = new ProductRecyclerViewAdapter(getActivity(), productArray, this);
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (campusTotalPage != 0 && !campusAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 campusAsyncExcecuting = true;
                                 callMyProductListAsync();
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    public static void clearArrayData(int value) {
        AppDelegate.LogCh("clearArrayData called at MyProfileFragment => " + value);
        switch (value) {
            default:
                campusCounter = 1;
                campusTotalPage = -1;
                productArray.clear();
                break;
        }
    }

    private void callMyProductListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, campusCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_MY_PRODUCT,
                    mPostArrayList, null);
            if (!campusAsyncExcecuting)
                mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private Bundle bundle;
    private Fragment fragment;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;

            case R.id.img_c_right:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new EditProfileFragment());
                break;

            case R.id.img_check:
                showImageSelectorList();
                break;

            case R.id.ll_c_following:
                break;

            case R.id.rl_user_img:
                Intent intent = new Intent(getActivity(), LargeImageActivity.class);
                bundle = new Bundle();
                bundle.putString(Tags.image_1, dataModel.image);
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            case R.id.ll_c_listing_count:
                bundle = new Bundle();
                bundle.putParcelable(Tags.user, dataModel);
                fragment = new MyProductListFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new MyProductListFragment());
                break;

            case R.id.ll_c_followers_count:
                bundle = new Bundle();
                bundle.putParcelable(Tags.user, dataModel);
                fragment = new FollowersListFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new FollowersListFragment());
                break;

            case R.id.ll_c_following_count:
                bundle = new Bundle();
                bundle.putParcelable(Tags.user, dataModel);
                fragment = new FollowingListFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
//                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new FollowingListFragment());
                break;

            case R.id.img_c_chat:
                break;

            case R.id.rl_c_sold_listings:
                bundle = new Bundle();
                bundle.putParcelable(Tags.user, dataModel);
                fragment = new SoldListingFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;

            case R.id.rl_c_reviews:
                Bundle bundle1 = new Bundle();
                bundle1.putParcelable(Tags.user, dataModel);
                Fragment fragment1 = new ReviewsFragment();
                fragment1.setArguments(bundle1);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment1);
                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_MY_PRODUCT)) {
            mHandler.sendEmptyMessage(11);
            if (campusCounter > 1) {
                campusAsyncExcecuting = false;
            }
            swipyrefreshlayout.setRefreshing(false);
            parseCampusListResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.EDIT_PROFILE)) {
            mHandler.sendEmptyMessage(11);
            parseUpdateProfileResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_USER)) {
            mHandler.sendEmptyMessage(11);
            parseUserData(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.PRODUCT_LIKES)) {
            mHandler.sendEmptyMessage(11);
            parseLikesResponse(result);
        }
    }

    private void parseLikesResponse(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                productArray.get(selected_pos).product_like_status = productArray.get(selected_pos).product_like_status == 0 ? 1 : 0;
                SellItemFragment.updateProduct(productArray.get(selected_pos));
            }
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    private void parseUserData(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
//                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            } else {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                if (object.has(Tags.image) && AppDelegate.isValidString(object.optString(Tags.image)))
                    userDataModel.image = object.getString(Tags.image);
                if (!AppDelegate.isValidString(userDataModel.image) && prefs.getTempFacebookData() != null && AppDelegate.isValidString(prefs.getTempFacebookData().image)) {
                    userDataModel.image = prefs.getTempFacebookData().image;
                }
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.follow_status = object.getInt(Tags.follow_status);
                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                userDataModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    userDataModel.institution_id = studentObject.getString(Tags.institution_state_id);
                    userDataModel.institute_name = studentObject.getString(Tags.institution_name);
                } else {
                    userDataModel.institute_name = studentObject.getString(Tags.other_ins_name);
                }
                userDataModel.department_name = studentObject.getString(Tags.department_name);
                dataModel = userDataModel;
                prefs.setUserData(dataModel);

                mHandler.sendEmptyMessage(3);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }

    }


    private void parseUpdateProfileResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONObject object = jsonObject.getJSONObject(Tags.response);
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.role = object.getString(Tags.role);
                userDataModel.status = object.getString(Tags.status);
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.role = object.getString(Tags.role);
                userDataModel.image = object.getString(Tags.image);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.gcm_token = object.getString(Tags.gcm_token);
                userDataModel.facebook_id = object.getString(Tags.social_id);
                userDataModel.login_type = object.getString(Tags.login_type);
                userDataModel.is_verified = object.getString(Tags.is_verified);

                userDataModel.following_count = object.getInt(Tags.following_count);
                userDataModel.followers_count = object.getInt(Tags.followers_count);
                userDataModel.total_product = object.getInt(Tags.total_product);

                JSONObject studentObject = object.getJSONObject(Tags.student_detail);

                userDataModel.str_Gender = studentObject.getString(Tags.gender);
                userDataModel.mobile_number = studentObject.getString(Tags.phone_no);
                userDataModel.userId = studentObject.getString(Tags.user_id);
                userDataModel.dob = studentObject.getString(Tags.dob);
                userDataModel.fav_cat_id = studentObject.getString(Tags.fav_cat_id);

                InstitutionModel institutionModel = new InstitutionModel();
                institutionModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                    institutionModel.institution_name_id = studentObject.getString(Tags.institution_state_id);
                    institutionModel.institution_name = studentObject.getString(Tags.institution_name);
                } else {
                    institutionModel.institution_name = studentObject.getString(Tags.other_ins_name);
                }
                institutionModel.department_name = studentObject.getString(Tags.department_name);

                prefs.setUserData(userDataModel);
                prefs.setInstitutionModel(institutionModel);
                mHandler.sendEmptyMessage(3);

            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }


    //------------image selector-----------------//

    private void parseCampusListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    if (jsonArray.length() != 0) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object = jsonArray.getJSONObject(i);
                            ProductModel productModel = new ProductModel();
                            productModel.id = object.getString(Tags.id);
                            productModel.cat_id = object.getString(Tags.cat_id);
                            productModel.title = object.getString(Tags.title);
                            productModel.description = object.getString(Tags.description);
                            productModel.price = object.getString(Tags.price);
                            productModel.item_condition = object.getString(Tags.item_condition);

                            productModel.image_1 = JSONParser.getString(object, Tags.image_1);
                            productModel.image_2 = JSONParser.getString(object, Tags.image_2);
                            productModel.image_3 = JSONParser.getString(object, Tags.image_3);
                            productModel.image_4 = JSONParser.getString(object, Tags.image_4);

                            productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
                            productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
                            productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
                            productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

                            productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
                            productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
                            productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

                            float floatValue = Float.parseFloat(object.getString(Tags.rating));
                            AppDelegate.LogT("floatValue = " + floatValue);
                            productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
                            AppDelegate.LogT("productModel.rating = " + productModel.rating);

                            productModel.sold_status = object.getString(Tags.sold_status);
                            productModel.status = object.getString(Tags.status);
                            productModel.created = object.getString(Tags.created);
                            productModel.modified = object.getString(Tags.modified);
                            productModel.total_product_views = object.getString(Tags.total_product_views);
                            productModel.logged_user_view_status = object.getString(Tags.logged_user_view_status);

                            if (object.has(Tags.product_category) && object.optJSONObject(Tags.product_category) != null) {
                                JSONObject productObject = object.getJSONObject(Tags.product_category);
                                productModel.pc_id = productObject.getString(Tags.id);
                                productModel.pc_title = productObject.getString(Tags.cat_name);
                                productModel.pc_status = productObject.getString(Tags.status);
                            }

                            productModel.latitude = object.getString(Tags.latitude);
                            productModel.longitude = object.getString(Tags.longitude);

                            JSONObject userObject = object.getJSONObject(Tags.user);
                            productModel.user_id = userObject.getString(Tags.id);
                            productModel.user_first_name = userObject.getString(Tags.first_name);
                            productModel.user_last_name = userObject.getString(Tags.last_name);
                            productModel.user_email = userObject.getString(Tags.email);
                            productModel.user_role = userObject.getString(Tags.role);
                            productModel.user_image = userObject.getString(Tags.image);
                            productModel.user_social_id = userObject.getString(Tags.social_id);
                            productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

                            JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                                productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                                productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                                JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                                if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                                    productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                                }
                                if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                                } else {
                                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                                }
                            } else {
                                productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                                productModel.user_department_name = studentObject.getString(Tags.department_name);
                            }

                            productArray.add(productModel);
                        }
                    }
                } else {
                    campusTotalPage = 0;
                    AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
                }
            } else {
                campusTotalPage = 0;
                AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            }
            campusCounter++;
            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
//            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    int selected_pos = 0;

    @Override
    public void setOnListItemClickListener(String name, int position) {
        selected_pos = position;
        if (name.equalsIgnoreCase(Tags.product)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.product, productArray.get(position));
            bundle.putInt(Tags.FROM, AppDelegate.PRODUCT_MY_PROFILE);
            Fragment fragment = new ProductDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        } else if (name.equalsIgnoreCase(Tags.LIKES)) {
            callProductLikesAsync(productArray.get(position).id, productArray.get(position).product_like_status == 0 ? "1" : "0");
        }
    }

    private void callProductLikesAsync(String product_id, String status) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.PRODUCT_LIKES,
                    mPostArrayList, this);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position, int height) {
        if (name.equalsIgnoreCase(Tags.HEIGHT)) {
            productArray.get(position).height = height;
        }
    }

    private void execute_updateProfileApi() {
        if (OriginalPhoto == null) {
            AppDelegate.showAlert(getActivity(), "Please select image.");
        } else if (AppDelegate.haveNetworkConnection(getActivity())) {
            writeImageFile();
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.first_name, dataModel.first_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.last_name, dataModel.last_name);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.email, dataModel.email);
//            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.password, dataModel.password);
            if (AppDelegate.isValidString(dataModel.dob))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.dob, dataModel.dob);
            if (AppDelegate.isValidString(dataModel.str_Gender))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gender, dataModel.str_Gender);
            if (AppDelegate.isValidString(dataModel.city_name))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.city_name, "");
            if (AppDelegate.isValidString(dataModel.fav_cat_id))
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.fav_cat_id, dataModel.fav_cat_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.image, capturedFile.getAbsolutePath(), ServerRequestConstants.Key_PostFileValue);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.gcm_token, GCMClientManager.getRegistrationId(getActivity()));

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    MyProfileFragment.this, ServerRequestConstants.EDIT_PROFILE,
                    mPostArrayList, MyProfileFragment.this);
            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        }
    }

    public void showImageSelectorList() {
        AppDelegate.hideKeyBoard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ListView modeList = new ListView(getActivity());
        String[] stringArray = new String[]{"  Avatar", "  Camera", "  Gallery", "  Cancel"};
        ArrayAdapter<String> modeAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_simple_list_item, stringArray);
        modeList.setAdapter(modeAdapter);
        builder.setView(modeList);
        final Dialog dialog = builder.create();
        modeList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i) {
                    case 0:
                        dialog.dismiss();
                        showAvatarAlert();
                        break;
                    case 1:
                        dialog.dismiss();
                        new OpenCamera().execute();
                        break;
                    case 2:
                        dialog.dismiss();
                        openGallery();
                        break;
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
        dialog.show();
    }

    private void showAvatarAlert() {
        try {
            final AlertDialog.Builder mAlert = new AlertDialog.Builder(getActivity());
            mAlert.setCancelable(true);
            mAlert.setTitle("Use image");
            LinearLayout ll_c_img_01, ll_c_img_02;
            ImageView img_01, img_02;
            View view = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.dialog_default_user_image, null, false);

            img_01 = (ImageView) view.findViewById(R.id.img_01);
            img_02 = (ImageView) view.findViewById(R.id.img_02);
            if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                img_01.setImageResource(R.drawable.user_male_1);
                img_02.setImageResource(R.drawable.user_male_2);
            } else {
                img_01.setImageResource(R.drawable.user_female_1);
                img_02.setImageResource(R.drawable.user_female_2);
            }

            ll_c_img_01 = (LinearLayout) view.findViewById(R.id.ll_c_img_01);
            ll_c_img_02 = (LinearLayout) view.findViewById(R.id.ll_c_img_02);
            mAlert.setView(view);
            mAlert.setNegativeButton(
                    Tags.CANCEL,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            final AlertDialog dialog = mAlert.show();

            ll_c_img_01.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_1);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_1);
                    }

//                    cimg_user.setImageBitmap(OriginalPhoto);
                    execute_updateProfileApi();
                }
            });
            ll_c_img_02.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (dataModel.str_Gender.equalsIgnoreCase(Tags.MALE)) {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_male_2);
                    } else {
                        OriginalPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.user_female_2);
                    }

//                    cimg_user.setImageBitmap(OriginalPhoto);
                    execute_updateProfileApi();
                }
            });
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public void writeImageFile() {
        if (capturedFile == null)
            capturedFile = new File(getNewFile());
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream(capturedFile);
        } catch (FileNotFoundException e) {
            AppDelegate.LogE(e);
        }
        OriginalPhoto.compress(Bitmap.CompressFormat.PNG, 85, fOut);
        try {
            fOut.flush();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
        try {
            fOut.close();
        } catch (IOException e) {
            AppDelegate.LogE(e);
        }
    }

    public void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        getActivity().startActivityForResult(Intent.createChooser(intent, getString(R.string.label_select_picture)), AppDelegate.SELECT_PICTURE);
    }

    @Override
    public void setOnReceivePictureResult(String apiName, Uri picUri) {
        if (apiName.equalsIgnoreCase(Tags.PICTURE)) {
            try {
                OriginalPhoto = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), picUri);
                OriginalPhoto = Bitmap.createScaledBitmap(OriginalPhoto, 240, 240, true);
                execute_updateProfileApi();
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        onPictureResult = null;
    }

    public String getNewFile() {
        File directoryFile;
        if (AppDelegate.isSDcardAvailable()) {
            directoryFile = new File(Environment.getExternalStorageDirectory()
                    + "/" + getString(R.string.app_name));
        } else {
            directoryFile = getActivity().getDir(getString(R.string.app_name), Context.MODE_PRIVATE);
        }
        if (directoryFile.exists() && directoryFile.isDirectory()
                || directoryFile.mkdirs()) {
            capturedFile = new File(directoryFile, "Image_" + System.currentTimeMillis()
                    + ".png");
            try {
                if (capturedFile.createNewFile()) {
                    AppDelegate.LogT("File created = " + capturedFile.getAbsolutePath());
                    return capturedFile.getAbsolutePath();
                }
            } catch (IOException e) {
                AppDelegate.LogE(e);
            }
        }
        AppDelegate.LogE("no file created.");
        return null;
    }

    class OpenCamera extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mHandler.sendEmptyMessage(10);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            String str_file_path = getNewFile();
            if (str_file_path == null ? true : str_file_path.length() > 0 ? false : true) {
                AppDelegate.showToast(getActivity(), "File not created, please try agin later.");
                return null;
            }
            imageURI = Uri.fromFile(capturedFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageURI);
            getActivity().startActivityForResult(intent, AppDelegate.CAPTURE_IMAGE_FULLSIZE_ACTIVITY_REQUEST_CODE);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mHandler.sendEmptyMessage(11);
        }
    }
}
