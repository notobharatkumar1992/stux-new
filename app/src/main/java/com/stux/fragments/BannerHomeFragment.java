package com.stux.fragments;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.SliderModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.BannerDetailActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.ImageView;
import carbon.widget.TextView;

/**
 * Created by Bharat on 07/19/2016.
 */
public class BannerHomeFragment extends Fragment implements View.OnClickListener, OnReciveServerResponse {

    private Prefs prefs;
    private UserDataModel dataModel;

    private RelativeLayout rl_tag;
    private ImageView img_c_large, img_c_loading;
    private TextView txt_c_title, txt_c_description, txt_c_open;
    private SliderModel sliderModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_banner_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        sliderModel = getArguments().getParcelable(Tags.slider_id);
        initView(view);
        setValues();
    }

    private void setValues() {
        img_c_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading.getDrawable();
        frameAnimation.setCallback(img_c_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        Picasso.with(getActivity()).load(sliderModel.banner_thumb_image).into(img_c_large, new Callback() {
            @Override
            public void onSuccess() {
                img_c_loading.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
        txt_c_title.setText(sliderModel.title);
        txt_c_description.setText(sliderModel.descriptions);
    }

    private void initView(View view) {
        img_c_large = (ImageView) view.findViewById(R.id.img_c_large);
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) img_c_large.getLayoutParams();
        layoutParams.height = (AppDelegate.getDeviceWith(getActivity()) / 2) - 15;
        AppDelegate.LogT("pix width => " + AppDelegate.getDeviceWith(getActivity()) + ", height = " + layoutParams.height);
        AppDelegate.LogT("dp width => " + AppDelegate.pixToDP(getActivity(), AppDelegate.getDeviceWith(getActivity())) + ", height = " + AppDelegate.pixToDP(getActivity(), layoutParams.height));

        img_c_large.setLayoutParams(layoutParams);

        img_c_loading = (ImageView) view.findViewById(R.id.img_c_loading);
        txt_c_title = (TextView) view.findViewById(R.id.txt_c_title);
        txt_c_description = (TextView) view.findViewById(R.id.txt_c_description);
        txt_c_open = (TextView) view.findViewById(R.id.txt_c_open);
        txt_c_open.setOnClickListener(this);

        rl_tag = (RelativeLayout) view.findViewById(R.id.rl_tag);

        rl_tag.setVisibility(View.VISIBLE);
        if (sliderModel.banner_type == 1) {
            txt_c_open.setText(R.string.Learn_More);
        } else if (sliderModel.banner_type == 2) {
            txt_c_open.setText(R.string.Open);
        } else if (sliderModel.banner_type == 3) {
            txt_c_open.setText(R.string.Shop);
        } else if (sliderModel.banner_type == 4) {
            txt_c_open.setText(R.string.Watch_Trailer);
        } else if (sliderModel.banner_type == 5) {
            rl_tag.setVisibility(View.GONE);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_open:
                if (AppDelegate.haveNetworkConnection(getActivity())) {
                    if (sliderModel.thump_clicked == 0) {
                        callClicksAsync(sliderModel, 1);
                    }
                    if (sliderModel.banner_type == 4) {
                        Intent intent = new Intent(getActivity(), BannerDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.image, sliderModel.banner_image);
                        bundle.putParcelable(Tags.slider_id, sliderModel);
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else AppDelegate.openURL(getActivity(), sliderModel.url);

                }
                break;
        }
    }

    private void callClicksAsync(SliderModel sliderModel, int type) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, dataModel.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.slider_id, sliderModel.id);
            if (type == 0)
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.click_status, "1");
            else
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.thumb_status, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.device_id, AppDelegate.getUUID(getActivity()));
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.SLIDERS_CLICKS,
                    mPostArrayList, this);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        if (apiName.equalsIgnoreCase(ServerRequestConstants.SLIDERS_CLICKS)) {
            parseSlidersClick(result);
        }
    }

    private void parseSlidersClick(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (!jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                sliderModel.single_clicked = 1;
            } else {
                sliderModel.single_clicked = 1;
            }
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }
}
