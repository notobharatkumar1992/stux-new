package com.stux.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.stux.R;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;

/**
 * Created by Bharat on 07/26/2016.
 */
public class WebViewDetailFragment extends Fragment implements View.OnClickListener {

    private ProgressBar progressbar;
    private int value = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.web_view_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        value = getArguments().getInt(Tags.FROM);
        initView(view);

    }

    private void initView(View view) {
//        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
//        ((TextView) view.findViewById(R.id.txt_c_header)).setText(fromPage + "");
//        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
//        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
//        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getFragmentManager().popBackStack();
//            }
//        });
//        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.edit);
//        view.findViewById(R.id.img_c_right).setVisibility(View.GONE);

        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        progressbar.setVisibility(View.GONE);

        WebView webview = (WebView) view.findViewById(R.id.webview);

        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);

        progressbar.setVisibility(View.VISIBLE);
        webview.setWebViewClient(new WebViewClient() {

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressbar.setVisibility(View.VISIBLE);
            }

            public void onPageFinished(WebView view, String url) {
                if (progressbar.getVisibility() == View.VISIBLE) {
                    progressbar.setVisibility(View.GONE);
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                if (progressbar.getVisibility() == View.VISIBLE) {
                    progressbar.setVisibility(View.GONE);
                }
            }
        });

        switch (value) {
            case 0:
                webview.loadUrl(ServerRequestConstants.HELP_TERMS_AND_CONDITIONS);
                break;
            case 1:
                webview.loadUrl(ServerRequestConstants.HELP_PRIVACY_POLICY);
                break;
            case 2:
                webview.loadUrl(ServerRequestConstants.HELP_SAFETY_GUIDELINE);
                break;
            case 3:
                webview.loadUrl(ServerRequestConstants.HELP_STUX_RULES);
                break;
            case 4:
                webview.loadUrl(ServerRequestConstants.HELP_FAQ);
                break;
        }

    }

    @Override
    public void onClick(View v) {

    }
}
