package com.stux.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.ItemFoundModel;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.DividerItemDecoration;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.parser.JSONParser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.LostFoundItemRecyclerViewAdapter;

/**
 * Created by Bharat on 07/08/2016.
 */
public class LostFoundItemFragment extends Fragment implements OnReciveServerResponse, OnListItemClickListener {

    private ArrayList<ItemFoundModel> itemArray = new ArrayList<>();

    private Handler mHandler;
    private Prefs prefs;
    private UserDataModel userData;

    private ProgressBar progressbar;

    private TextView txt_c_no_list;
    private int itemCounter = 1, itemTotalPage = -1;
    private boolean itemAsyncExcecuting = false;
    private int preLast;

    private SwipyRefreshLayout swipyrefreshlayout;
    private RecyclerView recyclerView;
    private LostFoundItemRecyclerViewAdapter rcAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_event_list_view, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        itemArray.clear();
        if (itemArray.size() == 0) {
            itemCounter = 1;
            itemTotalPage = -1;
            itemAsyncExcecuting = true;
            callFoundItemListAsync();
        } else
            mHandler.sendEmptyMessage(2);
    }

    private void callFoundItemListAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.user_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, itemCounter + "");
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_FOUND_LIST,
                    mPostArrayList, null);
            if (itemCounter == 1)
                mHandler.sendEmptyMessage(12);
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHandler();
        initView(view);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    progressbar.setVisibility(View.VISIBLE);
                    txt_c_no_list.setVisibility(View.GONE);
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 2) {
                    AppDelegate.LogT("mEventAdapter notified = " + itemArray.size());
                    txt_c_no_list.setVisibility(itemArray.size() > 0 ? View.GONE : View.VISIBLE);
                    txt_c_no_list.setText("No item available");
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                }
            }
        };
    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Lost / Found");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_right)).setImageResource(R.drawable.add);
        view.findViewById(R.id.img_c_right).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_right).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                } else
                    AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new AddItemFoundFragment());
            }
        });


        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        rcAdapter = new LostFoundItemRecyclerViewAdapter(getActivity(), itemArray, this, Tags.LOST_FOUND);
        recyclerView.setBackgroundColor(Color.WHITE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(
                new DividerItemDecoration(getActivity(), R.drawable.bg_divider));
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setPadding(AppDelegate.dpToPix(getActivity(), 15), 0, AppDelegate.dpToPix(getActivity(), 15), 0);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                     @Override
                     public void onRefresh(SwipyRefreshLayoutDirection direction) {
                         if (direction == SwipyRefreshLayoutDirection.BOTTOM) {
                             if (itemTotalPage != 0 && !itemAsyncExcecuting) {
                                 mHandler.sendEmptyMessage(2);
                                 callFoundItemListAsync();
                                 itemAsyncExcecuting = true;
                             } else {
                                 swipyrefreshlayout.setRefreshing(false);
                                 AppDelegate.LogT("selected_tab = 0, " + itemTotalPage + ", " + itemAsyncExcecuting);
                             }
                         } else {
                             swipyrefreshlayout.setRefreshing(false);
                         }
                     }
                 }
                );
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(13);
        if (!AppDelegate.isValidString(result)) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            return;
        }
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_FOUND_LIST)) {
            itemAsyncExcecuting = false;
            swipyrefreshlayout.setRefreshing(false);
            parseEventListResult(result);
        }
    }

    private void parseEventListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response) && jsonObject.optJSONArray(Tags.response) != null) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        ItemFoundModel itemModel = new ItemFoundModel();
                        itemModel.id = JSONParser.getString(object, Tags.id);
                        itemModel.user_id = JSONParser.getString(object, Tags.user_id);
                        itemModel.item_name = object.getString(Tags.item_name);
                        itemModel.item_description = object.getString(Tags.item_description);
                        itemModel.location = JSONParser.getString(object, Tags.location);
                        itemModel.campus_name = JSONParser.getString(object, Tags.campus_name);
                        itemModel.contact_no = JSONParser.getString(object, Tags.contact_no);
                        itemModel.image_1 = JSONParser.getString(object, Tags.image_1);
                        itemModel.image_thumb_1 = JSONParser.getString(object, Tags.image_1_thumb);
                        itemModel.image_2 = JSONParser.getString(object, Tags.image_2);
                        itemModel.image_thumb_2 = JSONParser.getString(object, Tags.image_2_thumb);
                        itemModel.image_3 = JSONParser.getString(object, Tags.image_3);
                        itemModel.image_thumb_3 = JSONParser.getString(object, Tags.image_3_thumb);
                        itemModel.image_4 = JSONParser.getString(object, Tags.image_4);
                        itemModel.image_thumb_4 = JSONParser.getString(object, Tags.image_4_thumb);

                        itemModel.status = JSONParser.getString(object, Tags.status);
                        itemModel.is_view = JSONParser.getString(object, Tags.is_view);
                        itemModel.created = JSONParser.getString(object, Tags.created);
                        itemModel.contact_no = JSONParser.getString(object, Tags.contact_no);

                        itemModel.lat = JSONParser.getString(object, Tags.latitude);
                        itemModel.lng = JSONParser.getString(object, Tags.longitude);

                        itemModel.userModel = new UserDataModel();
                        JSONObject userObject = object.getJSONObject(Tags.user);
                        itemModel.userModel.first_name = userObject.getString(Tags.first_name);
                        itemModel.userModel.last_name = userObject.getString(Tags.last_name);
                        itemModel.userModel.image = userObject.getString(Tags.image);

                        JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
                        if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                            itemModel.userModel.institution_state_id = studentObject.getString(Tags.institution_state_id);
                            itemModel.userModel.institution_id = studentObject.getString(Tags.institution_id);
                            if (studentObject.has(Tags.institute_name) && AppDelegate.isValidString(studentObject.optString(Tags.institute_name))) {
                                itemModel.userModel.institute_name = studentObject.getString(Tags.institution_name);
                            }
                            if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                                itemModel.userModel.department_name = studentObject.getString(Tags.department_name);
                            } else {
                                itemModel.userModel.department_name = studentObject.getString(Tags.department_name);
                            }
                        } else {
                            itemModel.userModel.institute_name = studentObject.getString(Tags.other_ins_name);
                            itemModel.userModel.department_name = studentObject.getString(Tags.department_name);
                        }
                        itemArray.add(itemModel);
                    }
                } else {
                    itemTotalPage = 0;
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                itemTotalPage = 0;
                AppDelegate.showToast(getActivity(), "No record found.");
            }
            itemCounter++;

            mHandler.sendEmptyMessage(2);
        } catch (Exception e) {
            if (isAdded())
                AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.LOST_FOUND)) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.FOUND, itemArray.get(position));
            Fragment fragment = new LostFoundDetailFragment();
            fragment.setArguments(bundle);
            AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
        }
    }

}
