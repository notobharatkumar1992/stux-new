package com.stux.fragments;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Models.JobsModel;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.constants.Tags;

import carbon.widget.TextView;

/**
 * Created by Bharat on 06/02/2016.
 */
public class JobDetailFragment extends Fragment implements View.OnClickListener {

    private ImageView img_banner, img_loading;
    private CircleImageView cimg_user;
    private TextView txt_c_name, txt_c_inc, txt_c_address, txt_c_posted_by, txt_c_company_desc;

    private JobsModel jobsModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.jobs_detail_page, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        jobsModel = getArguments().getParcelable(Tags.jobs);
        initView(view);
        setValues();
    }

    private void setValues() {
        img_loading.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
        frameAnimation.setCallback(img_loading);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();

        AppDelegate.LogT("jobsModel.banner_image => " + jobsModel.banner_image);
        Picasso.with(getActivity()).load(jobsModel.banner_image).into(img_banner);
        Picasso.with(getActivity()).load(jobsModel.logo_image).into(cimg_user, new Callback() {
            @Override
            public void onSuccess() {
                img_loading.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
        txt_c_name.setText(jobsModel.title);
        txt_c_inc.setText(jobsModel.company_name);
        txt_c_address.setText(jobsModel.company_address);
        txt_c_posted_by.setText(Html.fromHtml("Posted by <b>" + jobsModel.posted_by + "<b/> (" + jobsModel.designation + ")"));
        txt_c_company_desc.setText(jobsModel.description);
    }

    private void initView(View view) {
        img_banner = (ImageView) view.findViewById(R.id.img_banner);
        img_loading = (ImageView) view.findViewById(R.id.img_loading);
        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);

        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);
        txt_c_inc = (TextView) view.findViewById(R.id.txt_c_inc);
        txt_c_address = (TextView) view.findViewById(R.id.txt_c_address);
        txt_c_posted_by = (TextView) view.findViewById(R.id.txt_c_posted_by);
        txt_c_company_desc = (TextView) view.findViewById(R.id.txt_c_company_desc);

        view.findViewById(R.id.txt_c_share).setOnClickListener(this);
        view.findViewById(R.id.txt_c_apply).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_c_share:
                break;
            case R.id.txt_c_apply:
                break;

        }
    }
}
