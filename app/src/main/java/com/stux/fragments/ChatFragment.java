package com.stux.fragments;

import android.app.Service;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductModel;
import com.stux.Models.UserDataModel;
import com.stux.PushNotificationService;
import com.stux.R;
import com.stux.Utils.CircleImageView;
import com.stux.Utils.Prefs;
import com.stux.Utils.SoftKeyboard;
import com.stux.Utils.SpacesChatDecoration;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;
import com.stux.interfaces.OnReciveSocketMessage;
import com.stux.parser.JSONParser;
import com.stux.service.ChatService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import carbon.widget.ImageView;
import carbon.widget.TextView;
import inducesmile.com.androidstaggeredgridlayoutmanager.ChatListViewAdapter;

/**
 * Created by Bharat on 06/06/2016.
 */
public class ChatFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnListItemClickListener, EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener, OnReciveSocketMessage {

    public SoftKeyboard softKeyboard;
    public static EmojiconEditText editEmojicon;
    private FrameLayout emojicons;
    private RelativeLayout rl_message;
    private ViewGroup rootLayout;
    private Handler mHandler;

    private ArrayList<com.stux.Models.Message> messageArray = new ArrayList<>();
    private Prefs prefs;
    private UserDataModel userData;
    private ProgressBar progressbar;

    // Campus list
    private int campusCounter = 1, campusTotalPage = -1;
    private TextView txt_c_no_list;

    private boolean campusAsyncExcecuting = false;

    private SwipyRefreshLayout swipyrefreshlayout;
    private LinearLayoutManager linearLayoutManager;
    private SpacesChatDecoration dealItemDecoration;
    private RecyclerView recyclerView;
    private ChatListViewAdapter rcAdapter;

    private Bundle bundle = null;
    private Fragment fragment = null;

    public ProductModel productModel;
    public com.stux.Models.Message messageModel;
    public int from = 0;
    public static final int FROM_CHAT = 1, FROM_PRODUCT = 2;

    // Product detail
    private ImageView img_c_loading_user, img_c_loading_product, img_c_product;
    private CircleImageView cimg_user;
    private TextView txt_c_name, txt_c_product_name;

    public static OnReciveSocketMessage onReciveSocketMessage;

    public String user_first_name, user_last_name, sender_id, receiver_id, product_id, user_image, product_name, product_image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        userData = prefs.getUserdata();
        onReciveSocketMessage = this;
        AppDelegate.LogT("chatFragment onViewCreated called");
        from = getArguments().getInt(Tags.PAGE);

        sender_id = userData.userId;

        if (from == FROM_PRODUCT) {
            productModel = getArguments().getParcelable(Tags.product);
            receiver_id = productModel.user_id;
            product_id = productModel.id;

            user_first_name = productModel.user_first_name;
            user_last_name = productModel.user_last_name;
            user_image = productModel.user_image;

            product_image = productModel.image_1;
            product_name = productModel.title;

            messageModel = new com.stux.Models.Message();
        } else {
            productModel = new ProductModel();
            messageModel = getArguments().getParcelable(Tags.message);
            productModel.id = messageModel.product_id;
            productModel.title = messageModel.product_name;
            productModel.image_1 = messageModel.product_image;

            receiver_id = messageModel.sender_id;
            product_id = messageModel.product_id;

            user_first_name = messageModel.dataModel.first_name;
            user_last_name = messageModel.dataModel.last_name;
            user_image = messageModel.dataModel.image;

            product_image = messageModel.product_image;
            product_name = messageModel.product_name;

            AppDelegate.LogT("image => " + messageModel.dataModel.image);
            if (messageModel != null && messageModel.productModel != null && AppDelegate.isValidString(messageModel.productModel.sold_status)) {
                if (messageModel.productModel.sold_status.equalsIgnoreCase("1")) {
                    view.findViewById(R.id.img_c_send).setEnabled(false);
                    view.findViewById(R.id.rl_message).setVisibility(View.GONE);
                    AppDelegate.showToast(getActivity(), "This product is already sold out now you will not be able to chat more in this room!");
                }
            }
        }

        initView(view);
        attachKeyboardListeners();
        setHandler();
        try {
            setValues();
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        if (messageArray.size() == 0) {
            callChatAsync();
        } else {
            mHandler.sendEmptyMessage(1);
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    if (isAdded())
                        onResume();
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
            }
        }, 2000);
    }

    @Override
    public void onResume() {
        super.onResume();
        AppDelegate.LogT("chatFragment onResume called");
        onReciveSocketMessage = this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppDelegate.LogT("chatFragment onDestroy called");
        onReciveSocketMessage = null;
    }

    private void setValues() {
        txt_c_name.setText(user_first_name + " " + user_last_name);
        txt_c_product_name.setText(product_name);

        img_c_loading_user.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation = (AnimationDrawable) img_c_loading_user.getDrawable();
        frameAnimation.setCallback(img_c_loading_user);
        frameAnimation.setVisible(true, true);
        frameAnimation.start();
        if (AppDelegate.isValidString(product_image)) {
            Picasso.with(getActivity()).load(product_image).into(img_c_product, new Callback() {
                @Override
                public void onSuccess() {
                    img_c_loading_user.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                }
            });
        } else {
            img_c_loading_user.setVisibility(View.GONE);
        }

        img_c_loading_product.setVisibility(View.VISIBLE);
        AnimationDrawable frameAnimation1 = (AnimationDrawable) img_c_loading_product.getDrawable();
        frameAnimation1.setCallback(img_c_loading_product);
        frameAnimation1.setVisible(true, true);
        frameAnimation1.start();
        Picasso.with(getActivity()).load(user_image).into(cimg_user, new Callback() {
            @Override
            public void onSuccess() {
                img_c_loading_product.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });

    }

    private void initView(View view) {
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Chat");
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.menu);
        view.findViewById(R.id.img_c_left).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlider();
            }
        });
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        txt_c_no_list = (TextView) view.findViewById(R.id.txt_c_no_list);
        txt_c_no_list.setVisibility(View.GONE);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        rl_message = (RelativeLayout) view.findViewById(R.id.rl_message);
        editEmojicon = (EmojiconEditText) view.findViewById(R.id.editEmojicon);
        rootLayout = (ViewGroup) view.findViewById(R.id.rootLayout);

        emojicons = (FrameLayout) view.findViewById(R.id.emojicons);
        view.findViewById(R.id.img_c_emoji).setOnClickListener(this);
        view.findViewById(R.id.img_c_send).setOnClickListener(this);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setPadding(AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5), AppDelegate.dpToPix(getActivity(), 5));
        recyclerView.setHasFixedSize(true);

        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        dealItemDecoration = new SpacesChatDecoration(AppDelegate.dpToPix(getActivity(), 5), true);
        recyclerView.addItemDecoration(dealItemDecoration);
        rcAdapter = new ChatListViewAdapter(getActivity(), messageArray, this);
        recyclerView.setAdapter(rcAdapter);

        swipyrefreshlayout = (SwipyRefreshLayout) view.findViewById(R.id.swipyrefreshlayout);
        swipyrefreshlayout.setRefreshing(false);
        swipyrefreshlayout.setEnabled(false);
        swipyrefreshlayout.setOnRefreshListener
                (new SwipyRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh(SwipyRefreshLayoutDirection direction) {
                        if (direction == SwipyRefreshLayoutDirection.TOP) {
//                            if (campusTotalPage != 0 && !campusAsyncExcecuting) {
//                                mHandler.sendEmptyMessage(2);
//                                callCampusListAsync();
//                                campusAsyncExcecuting = true;
//                            } else {
//                                swipyrefreshlayout.setRefreshing(false);
//                                AppDelegate.LogT("selected_tab = 0, " + campusTotalPage + ", " + campusAsyncExcecuting);
//                            }
                            swipyrefreshlayout.setRefreshing(false);
                        } else {
                            swipyrefreshlayout.setRefreshing(false);
                        }
                    }
                });

        cimg_user = (CircleImageView) view.findViewById(R.id.cimg_user);
        cimg_user.setOnClickListener(this);

        img_c_loading_user = (ImageView) view.findViewById(R.id.img_c_loading_user);
        img_c_loading_product = (ImageView) view.findViewById(R.id.img_c_loading_product);
        img_c_product = (ImageView) view.findViewById(R.id.img_c_product);

        txt_c_name = (TextView) view.findViewById(R.id.txt_c_name);
        txt_c_product_name = (TextView) view.findViewById(R.id.txt_c_product_name);

        view.findViewById(R.id.rl_user).setOnClickListener(this);
        view.findViewById(R.id.rl_product).setOnClickListener(this);

    }

    private void callChatAsync() {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.sender_id, userData.userId);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.receiver_id, receiver_id);
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_id, product_id);
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.CHAT_LIST,
                    mPostArrayList, null);
            if (!campusAsyncExcecuting)
                mHandler.sendEmptyMessage(10);
            campusAsyncExcecuting = true;
            mPostAsyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 12) {
                    shoftKeyboardShown();
                } else if (msg.what == 13) {
                    progressbar.setVisibility(View.VISIBLE);
                } else if (msg.what == 14) {
                    progressbar.setVisibility(View.GONE);
                } else if (msg.what == 1) {
                    rcAdapter.notifyDataSetChanged();
                    recyclerView.invalidate();
                    recyclerView.scrollToPosition(messageArray.size() - 1);
                }
            }
        };
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.CHAT_LIST)) {
            parseChatListResult(result);
        }
    }

    private void parseChatListResult(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("1")) {
                if (AppDelegate.isValidString(JSONParser.getString(jsonObject, Tags.response))) {
                    JSONArray jsonArray = jsonObject.getJSONObject(Tags.response).getJSONArray(Tags.chat_data);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject object = jsonArray.getJSONObject(i);
                        com.stux.Models.Message message = new com.stux.Models.Message();
                        message.id = JSONParser.getString(object, Tags.ID);
                        message.sender_id = JSONParser.getString(object, Tags.sender_id);
                        message.receiver_id = JSONParser.getString(object, Tags.receiver_id);
                        message.product_id = JSONParser.getString(object, Tags.product_id);
                        message.mMessage = JSONParser.getString(object, Tags.message);
                        message.status = JSONParser.getString(object, Tags.status);
                        message.created = JSONParser.getString(object, Tags.created);
                        message.mType = com.stux.Models.Message.TYPE_MESSAGE;
                        if (message.sender_id.equalsIgnoreCase(userData.userId)) {
                            message.user_type = com.stux.Models.Message.USER_SENDER;

                            message.dataModel = new UserDataModel();
                            message.dataModel.userId = object.getJSONObject("sender_data").getString("id");
                            message.dataModel.first_name = object.getJSONObject("sender_data").getString("first_name");
                            message.dataModel.last_name = object.getJSONObject("sender_data").getString("last_name");
                            message.dataModel.image = object.getJSONObject("sender_data").getString("image");

                        } else {
                            message.user_type = com.stux.Models.Message.USER_RECEIVER;

                            message.dataModel = new UserDataModel();
                            message.dataModel.userId = object.getJSONObject("reciver_data").getString("id");
                            message.dataModel.first_name = object.getJSONObject("reciver_data").getString("first_name");
                            message.dataModel.last_name = object.getJSONObject("reciver_data").getString("last_name");
                            message.dataModel.image = object.getJSONObject("reciver_data").getString("image");

                            if (object.has(Tags.is_view) && !JSONParser.getString(object, Tags.is_view).equalsIgnoreCase("1")) {
                                ChatService.sendCallbackToServer(message);
                            } else {
                                ChatService.sendCallbackToServer(message);
                            }
                        }
                        messageArray.add(message);
                    }

                    JSONObject object = jsonObject.getJSONObject(Tags.response).getJSONArray(Tags.product_data).getJSONObject(0);
                    ProductModel productModel = getProductModelFromJSONObject(object.toString());
                    for (int i = 0; i < messageArray.size(); i++) {
                        messageArray.get(i).productModel = productModel;
                    }

                    mHandler.sendEmptyMessage(1);
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
    }

    public static ProductModel getProductModelFromJSONObject(String jsonObject) {
        JSONObject object = null;
        ProductModel productModel = new ProductModel();
        try {
            object = new JSONObject(jsonObject);
            productModel.id = JSONParser.getString(object, Tags.id);
            productModel.cat_id = JSONParser.getString(object, Tags.cat_id);
            productModel.title = JSONParser.getString(object, Tags.title);
            productModel.description = JSONParser.getString(object, Tags.description);
            productModel.price = JSONParser.getString(object, Tags.price);
            productModel.item_condition = JSONParser.getString(object, Tags.item_condition);

            productModel.image_1 = JSONParser.getString(object, Tags.image_1);
            productModel.image_2 = JSONParser.getString(object, Tags.image_2);
            productModel.image_3 = JSONParser.getString(object, Tags.image_3);
            productModel.image_4 = JSONParser.getString(object, Tags.image_4);

            productModel.image_1_thumb = JSONParser.getString(object, Tags.image_1_thumb);
            productModel.image_2_thumb = JSONParser.getString(object, Tags.image_2_thumb);
            productModel.image_3_thumb = JSONParser.getString(object, Tags.image_3_thumb);
            productModel.image_4_thumb = JSONParser.getString(object, Tags.image_4_thumb);

            productModel.total_product_likes = JSONParser.getInt(object, Tags.total_product_likes);
            productModel.product_like_status = JSONParser.getInt(object, Tags.product_like_status);
            productModel.total_comments = JSONParser.getInt(object, Tags.total_comments);

            float floatValue = Float.parseFloat(JSONParser.getString(object, Tags.rating));
//                    AppDelegate.LogT("floatValue = " + floatValue);
            productModel.rating = (int) floatValue + (floatValue % 1 > 0.50f ? 1 : 0);
//                    AppDelegate.LogT("productModel.rating = " + productModel.rating);

            productModel.sold_status = JSONParser.getString(object, Tags.sold_status);
            productModel.status = JSONParser.getString(object, Tags.status);
            productModel.created = JSONParser.getString(object, Tags.created);
            productModel.modified = JSONParser.getString(object, Tags.modified);
            productModel.total_product_views = JSONParser.getString(object, Tags.total_product_views);
            productModel.logged_user_view_status = JSONParser.getString(object, Tags.logged_user_view_status);

            if (object.has(Tags.product_category) && AppDelegate.isValidString(object.optJSONObject(Tags.product_category) + "")) {
                JSONObject productObject = object.getJSONObject(Tags.product_category);
                productModel.pc_id = productObject.getString(Tags.id);
                productModel.pc_title = productObject.getString(Tags.cat_name);
                productModel.pc_status = productObject.getString(Tags.status);
            }

            productModel.latitude = JSONParser.getString(object, Tags.latitude);
            productModel.longitude = JSONParser.getString(object, Tags.longitude);

            JSONObject userObject = object.getJSONObject(Tags.user);
            productModel.user_id = userObject.getString(Tags.id);
            productModel.user_first_name = userObject.getString(Tags.first_name);
            productModel.user_last_name = userObject.getString(Tags.last_name);
            productModel.user_email = userObject.getString(Tags.email);
            productModel.user_role = userObject.getString(Tags.role);
            productModel.user_image = userObject.getString(Tags.image);
            productModel.user_social_id = userObject.getString(Tags.social_id);
            productModel.user_gcm_token = userObject.getString(Tags.gcm_token);

            productModel.user_following_count = userObject.getInt(Tags.following_count);
            productModel.user_followers_count = userObject.getInt(Tags.followers_count);
            productModel.user_total_product = userObject.getInt(Tags.total_product);
            productModel.user_follow_status = JSONParser.getInt(userObject, Tags.follow_status);

            JSONObject studentObject = userObject.getJSONObject(Tags.student_detail);
            if (studentObject.has(Tags.institution_id) && !studentObject.optString(Tags.institution_id).equalsIgnoreCase("0")) {
                productModel.user_institution_state_id = studentObject.getString(Tags.institution_state_id);
                productModel.user_institution_id = studentObject.getString(Tags.institution_id);
                JSONObject instituteObject = studentObject.getJSONObject(Tags.institute);
                if (instituteObject.has(Tags.institute_name) && AppDelegate.isValidString(instituteObject.optString(Tags.institute_name))) {
                    productModel.user_institute_name = instituteObject.getString(Tags.institute_name);
                }
                if (studentObject.has(Tags.department_name) && !studentObject.optString(Tags.department_name).equalsIgnoreCase("other")) {
                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                } else {
                    productModel.user_department_name = studentObject.getString(Tags.department_name);
                }
            } else {
                productModel.user_institute_name = studentObject.getString(Tags.other_ins_name);
                productModel.user_department_name = studentObject.getString(Tags.department_name);
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
        }
        return productModel;
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_emoji:
                AppDelegate.hideKeyBoard(getActivity());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                AppDelegate.LogT("emoji clicked = " + (emojicons.getVisibility() != View.VISIBLE));
                if (emojicons.getVisibility() != View.VISIBLE) {
                    emojicons.setVisibility(View.VISIBLE);
                    layoutParams.addRule(RelativeLayout.ABOVE, R.id.emojicons);
                    getActivity().getSupportFragmentManager()
                            .beginTransaction()
                            .replace(R.id.emojicons, EmojiconsFragment.newInstance(false))
                            .commit();
                } else {
                    emojicons.setVisibility(View.GONE);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                }
                rl_message.setLayoutParams(layoutParams);
                rl_message.requestLayout();
                break;

            case R.id.img_c_send:
//                ChatService.mSocket.emit("receive", editEmojicon.getText().toString() + ", prodcut id => " + productModel.id);
                if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
                    AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
                } else if (editEmojicon.length() == 0) {
                    AppDelegate.showToast(getActivity(), "Please enter message before sending.");
                } else {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("sender_id", sender_id);
                        jsonObject.put("receiver_id", receiver_id);
                        jsonObject.put("product_id", product_id);
                        jsonObject.put("message", editEmojicon.getText().toString());
                        jsonObject.put("status", "1");
                    } catch (JSONException e) {
                        AppDelegate.LogE(e);
                    }
                    ChatService.mSocket.emit("send message", jsonObject.toString());
                    AppDelegate.LogC("send => " + jsonObject.toString());
                    addMessageToList();
                }
                break;

            case R.id.rl_user:
                break;

            case R.id.cimg_user:
                UserDataModel sellersDataModel = new UserDataModel();

                sellersDataModel.userId = receiver_id;
                sellersDataModel.first_name = user_first_name;
                sellersDataModel.last_name = user_last_name;
                sellersDataModel.image = product_image;

                bundle = new Bundle();
                bundle.putParcelable(Tags.user, sellersDataModel);
                fragment = new SellersProfileFragment();
                if (sellersDataModel.userId.equalsIgnoreCase(userData.userId)) {
                    fragment = new MyProfileFragment();
                } else {
                    fragment = new SellersProfileFragment();
                }
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;

            case R.id.rl_product:
                bundle = new Bundle();
                bundle.putParcelable(Tags.product, productModel);
                bundle.putInt(Tags.FROM, AppDelegate.PRODUCT_CHAT);
                fragment = new ProductDetailFragment();
                fragment.setArguments(bundle);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                break;
        }
    }

    private void addMessageToList() {
        com.stux.Models.Message message = new com.stux.Models.Message();
        message.user_type = com.stux.Models.Message.USER_SENDER;
//        message.created = Calendar.getInstance().getTimeInMillis() + "";
        if (userData == null)
            userData = new UserDataModel();
        message.dataModel = userData;
        if (productModel == null)
            productModel = new ProductModel();
        message.productModel = productModel;
        message.product_id = productModel.id;
        message.product_name = productModel.title;

//        2016-08-12T10:07:51.000Z
        message.created = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(Calendar.getInstance().getTime());
        message.mType = com.stux.Models.Message.TYPE_MESSAGE;
        message.mUsername = userData.first_name + " " + userData.last_name;
        message.mMessage = editEmojicon.getText().toString();
        messageArray.add(message);
        mHandler.sendEmptyMessage(1);
        editEmojicon.setText("");
    }

    private void addMessageToList(com.stux.Models.Message message) {
        messageArray.add(message);
        mHandler.sendEmptyMessage(1);
    }

    protected void attachKeyboardListeners() {
        InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        softKeyboard = new SoftKeyboard(rootLayout, im);
        softKeyboard.setSoftKeyboardCallback(new SoftKeyboard.SoftKeyboardChanged() {

            @Override
            public void onSoftKeyboardHide() {
                AppDelegate.LogT("onSoftKeyboardHide called");
            }

            @Override
            public void onSoftKeyboardShow() {
                AppDelegate.LogT("onSoftKeyboardShow called");
                mHandler.sendEmptyMessage(12);
            }
        });
    }

    private void shoftKeyboardShown() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        emojicons.setVisibility(View.GONE);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        rl_message.setLayoutParams(layoutParams);
        rl_message.requestLayout();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (softKeyboard != null)
            softKeyboard.unRegisterSoftKeyboardCallback();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(editEmojicon, emojicon);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(editEmojicon);
    }

    @Override
    public void setOnReciveSocketMessage(String apiName, com.stux.Models.Message messageModel) {
        if (apiName.equalsIgnoreCase(Tags.CHAT)) {
            AppDelegate.LogT("product_id => " + messageModel.product_id + " = " + product_id);
            if (messageModel.product_id.equalsIgnoreCase(product_id)) {
                addMessageToList(messageModel);
            } else {
                PushNotificationService.showUserChatNotification(getActivity(), messageModel);
            }
        }
    }
}
