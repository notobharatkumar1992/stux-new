package com.stux.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.stux.Adapters.ChooseCategoryListAdapter;
import com.stux.AppDelegate;
import com.stux.Async.PostAsync;
import com.stux.Models.PostAysnc_Model;
import com.stux.Models.ProductCategoryModel;
import com.stux.Models.RefineSearchModel;
import com.stux.Models.UserDataModel;
import com.stux.R;
import com.stux.Utils.Prefs;
import com.stux.activities.MainActivity;
import com.stux.constants.ServerRequestConstants;
import com.stux.constants.Tags;
import com.stux.interfaces.OnListItemClickListener;
import com.stux.interfaces.OnReciveServerResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import carbon.widget.TextView;

/**
 * Created by Bharat on 08/29/2016.
 */
public class ChooseCategoryFragment extends Fragment implements OnReciveServerResponse, View.OnClickListener, OnListItemClickListener {

    private Prefs prefs;
    private UserDataModel dataModel;

    private Handler mHandler;

    private RefineSearchModel refineSearchModel;

    private ListView list;
    private ChooseCategoryListAdapter categoryListAdapter;
    public ArrayList<ProductCategoryModel> arrayProductCategory = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.choose_category, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        dataModel = prefs.getUserdata();
        initView(view);
        setHandler();
        callGetProductTypeAsync();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    categoryListAdapter.notifyDataSetChanged();
                    list.invalidate();
                }
            }
        };
    }

    private void callGetProductTypeAsync() {
        if (!AppDelegate.haveNetworkConnection(getActivity(), false)) {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        } else {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            PostAsync mPostAsyncObj = new PostAsync(getActivity(),
                    this, ServerRequestConstants.GET_PRODUCT_CATEGORY,
                    mPostArrayList, null);
            mHandler.sendEmptyMessage(10);
            mPostAsyncObj.execute();
        }
    }


    private void initView(View view) {
        ((TextView) view.findViewById(R.id.txt_c_header)).setText("Choose Category");
        view.findViewById(R.id.txt_c_header).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_header).setVisibility(View.GONE);
        ((carbon.widget.ImageView) view.findViewById(R.id.img_c_left)).setImageResource(R.drawable.back);
        view.findViewById(R.id.img_c_left).setOnClickListener(this);
        view.findViewById(R.id.ll_c_notification_dot).setVisibility(((MainActivity) getActivity()).newNotificationStatus ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.img_c_notification).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_notification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSlidingLayer();
            }
        });
        view.findViewById(R.id.img_c_search).setVisibility(View.VISIBLE);
        view.findViewById(R.id.img_c_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).toggleSearchLayout();
            }
        });

        categoryListAdapter = new ChooseCategoryListAdapter(getActivity(), arrayProductCategory, this);
        list = (ListView) view.findViewById(R.id.list);
        list.setAdapter(categoryListAdapter);

    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        mHandler.sendEmptyMessage(11);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.GET_PRODUCT_CATEGORY)) {
            parseProductCategoryResult(result);
        } else if (apiName.equalsIgnoreCase(ServerRequestConstants.REFINE_PRODUCTS)) {
            parseRefineProduct(result);
        }
    }

    private void parseRefineProduct(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.STATUS).equalsIgnoreCase("1")) {
                if (jsonObject.has(Tags.response)) {
                    JSONArray jsonArray = jsonObject.getJSONArray(Tags.response);
                    if (jsonArray.length() > 0) {
                        Bundle bundle = new Bundle();
                        bundle.putString(Tags.product, result);
                        bundle.putParcelable(Tags.refine, refineSearchModel);
                        Fragment fragment = new RefineProductFragment();
                        fragment.setArguments(bundle);
                        AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), fragment);
                    } else {
                        AppDelegate.showToast(getActivity(), "No record found.");
                    }
                } else {
                    AppDelegate.showToast(getActivity(), "No record found.");
                }
            } else {
                AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.MESSAGE));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server Error");
        }
    }

    private void parseProductCategoryResult(String result) {
        try {
            JSONObject object = new JSONObject(result);
            if (object.getString(Tags.status).equalsIgnoreCase("1")) {
                JSONArray jsonArray = object.getJSONArray(Tags.response);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ProductCategoryModel productCategoryModel = new ProductCategoryModel();
                    productCategoryModel.id = jsonObject.getString(Tags.id);
                    productCategoryModel.cat_name = jsonObject.getString(Tags.cat_name);
                    productCategoryModel.status = jsonObject.getString(Tags.id);
                    arrayProductCategory.add(productCategoryModel);
                }
                mHandler.sendEmptyMessage(1);
            } else {
                AppDelegate.showAlert(getActivity(), object.getString(Tags.message));
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.showToast(getActivity(), "Server Error");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_c_left:
                getFragmentManager().popBackStack();
                break;
        }
    }

    @Override
    public void setOnListItemClickListener(String name, int position) {
        if (name.equalsIgnoreCase(Tags.category)) {
            execute_searchProduct(arrayProductCategory.get(position).id + "");
        }
    }

    private void execute_searchProduct(String category_id) {
        if (AppDelegate.haveNetworkConnection(getActivity(), false)) {
            ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.logged_in_User_id, dataModel.userId + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.record, "10");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.page, "1");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.product_name, "");
            try {
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.campus_id, "");
            } catch (Exception e) {
                AppDelegate.LogE(e);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.campus_id, "");
            }
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.item_condition, "0");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.cat_id, category_id + "");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.price, "asc");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.upload, "asc");
            AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.range, "500");

            refineSearchModel = new RefineSearchModel();
            refineSearchModel.logged_in_User_id = dataModel.userId;
            refineSearchModel.record = "10";
            refineSearchModel.page = 1;
            refineSearchModel.product_name = "";
            refineSearchModel.campus_id = "";
            refineSearchModel.item_condition = "0";
            refineSearchModel.cat_id = "" + category_id;
            refineSearchModel.price = "asc";
            refineSearchModel.upload = "asc";
            refineSearchModel.rangeProgress = "500";

            PostAsync mPostasyncObj = new PostAsync(getActivity(),
                    ChooseCategoryFragment.this, ServerRequestConstants.REFINE_PRODUCTS,
                    mPostArrayList, ChooseCategoryFragment.this);

            mHandler.sendEmptyMessage(10);
            mPostasyncObj.execute();
        } else {
            AppDelegate.addFragment(getActivity().getSupportFragmentManager(), new NoInternetConnectionFragment(), 1);
        }
    }
}
