package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 06/29/2016.
 */
public class DealModel implements Parcelable {

    public String id, user_id, deal_catid, deal_area, institute_id, banner_image, title, details, venue, price, discount, discount_price, emailid, expiry_date, coupon_code,
            status, created, modified, product_category, view_count = "0", image_1, image_2, image_3, image_4, image_1_thumb, image_2_thumb, image_3_thumb, image_4_thumb, total_deal_views, logged_user_view_status, is_grabbed;

    public DealModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        deal_catid = in.readString();
        deal_area = in.readString();
        institute_id = in.readString();
        banner_image = in.readString();
        title = in.readString();
        details = in.readString();
        venue = in.readString();
        price = in.readString();
        discount = in.readString();
        discount_price = in.readString();
        emailid = in.readString();
        expiry_date = in.readString();
        coupon_code = in.readString();
        status = in.readString();
        created = in.readString();
        modified = in.readString();
        product_category = in.readString();
        view_count = in.readString();
        image_1 = in.readString();
        image_2 = in.readString();
        image_3 = in.readString();
        image_4 = in.readString();
        image_1_thumb = in.readString();
        image_2_thumb = in.readString();
        image_3_thumb = in.readString();
        image_4_thumb = in.readString();
        total_deal_views = in.readString();
        logged_user_view_status = in.readString();
        is_grabbed = in.readString();
    }

    public static final Creator<DealModel> CREATOR = new Creator<DealModel>() {
        @Override
        public DealModel createFromParcel(Parcel in) {
            return new DealModel(in);
        }

        @Override
        public DealModel[] newArray(int size) {
            return new DealModel[size];
        }
    };

    public DealModel() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(deal_catid);
        dest.writeString(deal_area);
        dest.writeString(institute_id);
        dest.writeString(banner_image);
        dest.writeString(title);
        dest.writeString(details);
        dest.writeString(venue);
        dest.writeString(price);
        dest.writeString(discount);
        dest.writeString(discount_price);
        dest.writeString(emailid);
        dest.writeString(expiry_date);
        dest.writeString(coupon_code);
        dest.writeString(status);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(product_category);
        dest.writeString(view_count);
        dest.writeString(image_1);
        dest.writeString(image_2);
        dest.writeString(image_3);
        dest.writeString(image_4);
        dest.writeString(image_1_thumb);
        dest.writeString(image_2_thumb);
        dest.writeString(image_3_thumb);
        dest.writeString(image_4_thumb);
        dest.writeString(total_deal_views);
        dest.writeString(logged_user_view_status);
        dest.writeString(is_grabbed);
    }
}
