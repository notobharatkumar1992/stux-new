package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 08/01/2016.
 */
public class ItemFoundModel implements Parcelable {

    public String id, user_id, item_name, item_description, location, campus_name, contact_no, image_1, image_thumb_1, image_2, image_thumb_2, image_3, image_thumb_3, image_4, image_thumb_4, status, is_view, created, lat, lng;

    public UserDataModel userModel;

    public ItemFoundModel(Parcel in) {
        id = in.readString();
        user_id = in.readString();
        item_name = in.readString();
        item_description = in.readString();
        location = in.readString();
        campus_name = in.readString();
        contact_no = in.readString();
        image_1 = in.readString();
        image_thumb_1 = in.readString();
        image_2 = in.readString();
        image_thumb_2 = in.readString();
        image_3 = in.readString();
        image_thumb_3 = in.readString();
        image_4 = in.readString();
        image_thumb_4 = in.readString();
        status = in.readString();
        is_view = in.readString();
        created = in.readString();
        lat = in.readString();
        lng = in.readString();

        userModel = in.readParcelable(UserDataModel.class.getClassLoader());
    }

    public static final Creator<ItemFoundModel> CREATOR = new Creator<ItemFoundModel>() {
        @Override
        public ItemFoundModel createFromParcel(Parcel in) {
            return new ItemFoundModel(in);
        }

        @Override
        public ItemFoundModel[] newArray(int size) {
            return new ItemFoundModel[size];
        }
    };

    public ItemFoundModel() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(user_id);
        dest.writeString(item_name);
        dest.writeString(item_description);
        dest.writeString(location);
        dest.writeString(campus_name);
        dest.writeString(contact_no);
        dest.writeString(image_1);
        dest.writeString(image_thumb_1);
        dest.writeString(image_2);
        dest.writeString(image_thumb_2);
        dest.writeString(image_3);
        dest.writeString(image_thumb_3);
        dest.writeString(image_4);
        dest.writeString(image_thumb_4);
        dest.writeString(status);
        dest.writeString(is_view);
        dest.writeString(created);
        dest.writeString(lat);
        dest.writeString(lng);

        dest.writeParcelable(userModel, flags);
    }
}
