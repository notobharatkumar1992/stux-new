package com.stux.Models;

/**
 * Created by Bharat on 07/29/2016.
 */
public class ReasonModel {

    public String id, name;
    public int icon;

    public ReasonModel(String id, String name, int icon) {
        this.id = id;
        this.name = name;
        this.icon = icon;
    }
}
