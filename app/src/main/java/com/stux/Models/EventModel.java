package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 06/02/2016.
 */
public class EventModel implements Parcelable {

    public String event_location, event_vanue, email_address, from_date, to_date, from_time, to_time, description,
            location_type, event_type, event_name, banner_image, theme, details, venue, location, tickets, gate_fees, contact_no, event_start_time, event_end_time,
            created, modified, banner_image_thumb, event_category_id, event_category_cat_name, event_category_status, event_category_created, event_category_modified, total_event_likes, event_like_status;

    public String id;
    public int event_id, type_of_event, gate_fees_type, status;

    public String event_category_name, user_id, user_first_name, user_last_name, user_image, user_image_thumb, user_institution_state_id, user_institution_id, user_institution_name, user_other_ins_name, user_department_name, latitude, longitude, total_event_views = "0", logged_user_view_status = "0";

    public EventModel() {
    }

    protected EventModel(Parcel in) {
        event_location = in.readString();
        event_vanue = in.readString();
        email_address = in.readString();
        from_date = in.readString();
        to_date = in.readString();
        from_time = in.readString();
        to_time = in.readString();
        description = in.readString();
        location_type = in.readString();
        event_type = in.readString();
        event_name = in.readString();
        banner_image = in.readString();
        theme = in.readString();
        details = in.readString();
        venue = in.readString();
        location = in.readString();
        tickets = in.readString();
        gate_fees = in.readString();
        contact_no = in.readString();
        event_start_time = in.readString();
        event_end_time = in.readString();
        created = in.readString();
        modified = in.readString();
        banner_image_thumb = in.readString();
        event_category_id = in.readString();
        event_category_cat_name = in.readString();
        event_category_status = in.readString();
        event_category_created = in.readString();
        event_category_modified = in.readString();
        total_event_likes = in.readString();
        event_like_status = in.readString();


        event_id = in.readInt();
        type_of_event = in.readInt();
        gate_fees_type = in.readInt();
        status = in.readInt();


        event_category_name = in.readString();
        user_id = in.readString();
        user_first_name = in.readString();
        user_last_name = in.readString();
        user_image = in.readString();
        user_image_thumb = in.readString();
        user_institution_state_id = in.readString();
        user_institution_id = in.readString();
        user_institution_name = in.readString();
        user_other_ins_name = in.readString();
        user_department_name = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        total_event_views = in.readString();
        logged_user_view_status = in.readString();

    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(event_location);
        dest.writeString(event_vanue);
        dest.writeString(email_address);
        dest.writeString(from_date);
        dest.writeString(to_date);
        dest.writeString(from_time);
        dest.writeString(to_time);
        dest.writeString(description);
        dest.writeString(location_type);
        dest.writeString(event_type);
        dest.writeString(event_name);
        dest.writeString(banner_image);
        dest.writeString(theme);
        dest.writeString(details);
        dest.writeString(venue);
        dest.writeString(location);
        dest.writeString(tickets);
        dest.writeString(gate_fees);
        dest.writeString(contact_no);
        dest.writeString(event_start_time);
        dest.writeString(event_end_time);
        dest.writeString(created);
        dest.writeString(modified);
        dest.writeString(banner_image_thumb);
        dest.writeString(event_category_id);
        dest.writeString(event_category_cat_name);
        dest.writeString(event_category_status);
        dest.writeString(event_category_created);
        dest.writeString(event_category_modified);
        dest.writeString(total_event_likes);
        dest.writeString(event_like_status);

        dest.writeInt(event_id);
        dest.writeInt(type_of_event);
        dest.writeInt(gate_fees_type);
        dest.writeInt(status);

        dest.writeString(event_category_name);
        dest.writeString(user_id);
        dest.writeString(user_first_name);
        dest.writeString(user_last_name);
        dest.writeString(user_image);
        dest.writeString(user_image_thumb);
        dest.writeString(user_institution_state_id);
        dest.writeString(user_institution_id);
        dest.writeString(user_institution_name);
        dest.writeString(user_other_ins_name);
        dest.writeString(user_department_name);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(total_event_views);
        dest.writeString(logged_user_view_status);
    }
}
