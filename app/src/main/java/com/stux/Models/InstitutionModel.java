package com.stux.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Bharat on 06/07/2016.
 */
public class InstitutionModel implements Parcelable {

    public String institution_state_name, institution_state_id, institution_name, institution_name_id, department_name, department_name_id;
    public int selected_institute_state = -1, selected_institute_name = -1, selected_department = -1;

    public InstitutionDetailModel detailModel;

    public InstitutionModel() {

    }

    protected InstitutionModel(Parcel in) {
        institution_state_name = in.readString();
        institution_state_id = in.readString();
        institution_name = in.readString();
        institution_name_id = in.readString();
        department_name = in.readString();
        department_name_id = in.readString();
        selected_institute_state = in.readInt();
        selected_institute_name = in.readInt();
        selected_department = in.readInt();
        detailModel = in.readParcelable(InstitutionDetailModel.class.getClassLoader());
    }

    public static final Creator<InstitutionModel> CREATOR = new Creator<InstitutionModel>() {
        @Override
        public InstitutionModel createFromParcel(Parcel in) {
            return new InstitutionModel(in);
        }

        @Override
        public InstitutionModel[] newArray(int size) {
            return new InstitutionModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(institution_state_name);
        dest.writeString(institution_state_id);
        dest.writeString(institution_name);
        dest.writeString(institution_name_id);
        dest.writeString(department_name);
        dest.writeString(department_name_id);
        dest.writeInt(selected_institute_state);
        dest.writeInt(selected_institute_name);
        dest.writeInt(selected_department);
        dest.writeParcelable(detailModel, flags);
    }
}
